<?php
/**
 * Page - Register new ward.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

$base_path = basename(__DIR__);

require_once 'includes/common.php';

// Check if the logged in user has access to the page.
$has_access = check_access(get_roles_with_access('is manager'), $user);

$site_loader = $container->getSiteLoader();
$sites = $site_loader->getAllHospitalSites();

// Get the current user's site id and code.
$user_sid = $user->getSid();
$user_site_code = $site_loader->getSiteBySid($user_sid)->getSiteCode();

// Error setting for if Javascript is not enabled.
$error_hidden = true;
$error_message = '';
require_once 'includes/register-ward.php';

// Value filling for if Javascript is not enabled.
$site_code_value = isset($_POST['site_code']) ? $_POST['site_code'] : null;
$ward_name_value = isset($_POST['ward_name']) ? $_POST['ward_name'] : null;
$ward_code_value = isset($_POST['ward_code']) ? $_POST['ward_code'] : null;
?>

<!DOCTYPE html>
<html>

    <head>
        <?php require_once 'includes/incl-head.html'; ?>
        <title>DC-Tracker - Register Ward</title>
        <script src="vendor/angular.min.js"></script>
        <script>
          var app = angular.module('formWard', []);

          var user_site_code = <?php echo json_encode($user_site_code); ?>;

          app.controller('formController', function($scope) {
            $scope.site_code = user_site_code;
          });
        </script>
    </head>

    <body>
        <!-- Navigation Bar -->
        <?php require_once 'templates/nav.php'; ?>

        <!-- Main Content -->
        <div class="container">

<?php if (!$has_access) : ?>
            <!-- Forbidden -->
            <?php include 'templates/forbidden.html'; ?>
<?php elseif (empty($sites)) : ?>
            <!-- Page Title -->
            <div class="row">
              <section class="col-xs-12">
                <h3>No sites registered!</h3>
              </section>
            </div>

            <!-- Content -->
            <div class="alert alert-warning">
                <p>There are currently no sites registered, so you cannot register any wards.</p>
            </div>
<?php else : ?>
            <!-- Page Title -->
            <div class="row">
                <section class="col-xs-12">
                    <h3>Register Ward</h3>
                </section>
            </div>

            <!-- Content -->
            <div class="row">
                <!-- Register Form -->
                <form id="form_register" method="post" action="" class="form-horizontal">

                    <!-- Fields -->
                    <section class="col-sm-6 col-xs-12" ng-app="formWard" ng-controller="formController">
                        <?php include 'templates/form-fields-ward.php'; ?>
                    </section>

                    <!-- Error -->
                    <?php include 'templates/alert-error.php'; ?>

                    <!-- Submit Button: Register -->
                    <section class="col-sm-6 col-xs-12">
                        <input type="submit" name="btn_register" class="btn btn-success col-xs-12" value="Register">
                    </section>

                </form>
            </div><!-- /.row -->
<?php endif; ?>
        </div><!-- /.container -->

        <!-- Footer -->
        <?php require_once 'templates/footer.php'; ?>

        <!-- Scripts -->
        <?php require_once 'includes/incl-js.html'; ?>
        <script>
          $(document).ready(function(){

            // Show Username field only if JS is enabled.
            $('#field_username').removeClass('hidden');

            $('#form_register').submit(function(e){
              //Stop the form from submitting itself to the server.
              e.preventDefault();
              // Assign input values to variables.
              var site_code = $('#site_code').val();
              var ward_name = $('#ward_name').val();
              var ward_code = $('#ward_code').val();

              var data = {
                site_code: site_code,
                ward_name: ward_name,
                ward_code: ward_code
              };

              // Pass data to ajax form.
              $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: 'includes/register-ward.php',
                data: {
                  json_data: JSON.stringify(data)
                },
                success: function(result){
                  // No errors.
                  if (true == result.success) {
                    $(location).attr('href', 'wards.php');
                  }
                  // Error occurred.
                  else {
                    errors = result.errors;
                    messages = '';
                    // Add breaks between each message.
                    for (var key in errors.messages) {
                      messages += errors.messages[key] + '<br>';
                    }
                    // Adds/removes .has-error from fields.
                    for (var key in errors.fields) {
                      if (errors.fields[key]) {
                        $('#' + key).parent().addClass('has-error');
                      }
                      else {
                        $('#' + key).parent().removeClass('has-error');
                      }
                    }
                    // Display messages.
                    $('#error').removeClass('hidden');
                    $('#error_message').html(messages);
                  }
                }
              });
            });
          });
        </script>

    </body>
</html>
