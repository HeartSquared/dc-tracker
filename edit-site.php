<?php
/**
 * Page - Edit hospital site details.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

$base_path = basename(__DIR__);

require_once 'includes/common.php';

// Check if there is an entry to edit.
if (empty($_SESSION['editing_sid'])) {
    header('Location: sites.php');
}

// Check if the logged in user has access to the page.
$has_access = check_access(get_roles_with_access('is admin'), $user);

$site_editor = $container->getSiteEditor();

// Get site ID.
$sid = $_SESSION['editing_sid'];

$site = $site_editor->getSiteBySid($sid);
$has_wards = $site_editor->checkHasWards($sid);
$has_users = $site_editor->checkHasUsers($sid);

// Error setting for if Javascript is not enabled.
$password_reset_success = false;
$new_password = null;
require_once 'includes/pass-admin-reset.php';
$error_hidden = true;
$error_message = '';
require_once 'includes/edit-site.php';

// Value filling for if Javascript is not enabled.
$hosp_name_value = isset($_POST['hosp_name']) ? $_POST['hosp_name'] : $site->getSiteName();
$suburb_value = isset($_POST['suburb']) ? $_POST['suburb'] : $site->getSuburb();
$site_code_value = isset($_POST['site_code']) ? $_POST['site_code'] : $site->getSiteCode();
?>

<!DOCTYPE html>
<html>

    <head>
        <?php require_once 'includes/incl-head.html'; ?>
        <title>DC-Tracker - Edit Site</title>
        <script src="vendor/angular.min.js"></script>
        <script>
          var app = angular.module('formSite', []);

          var site_code = <?php echo json_encode($site->getSiteCode()); ?>;

          app.controller('formController', function($scope) {
            $scope.site_code = site_code;
          });
        </script>
    </head>

    <body>
        <!-- Navigation Bar -->
        <?php require_once 'templates/nav.php'; ?>

        <!-- Main Content -->
        <div class="container">

<?php if (!$has_access) : ?>
            <!-- Forbidden -->
            <?php include 'templates/forbidden.html'; ?>

<?php else : ?>
            <!-- Page Title -->
            <div class="row">
              <section class="col-xs-12">
                <h3>Edit Site</h3>
              </section>
            </div>

            <?php include 'templates/alert-success--pass-reset.php'; ?>

    <?php if ($has_wards) : ?>
            <!-- Warning -->
            <div class="row">
              <section class="col-xs-12">
                <div class="alert alert-warning col-xs-12">
                  <p><strong>Note!</strong> This site has wards registered - Site Code cannot be changed and site cannot be deleted.</p>
                </div>
              </section>
            </div>
    <?php elseif ($has_users) : ?>
            <!-- Warning -->
            <div class="row">
              <section class="col-xs-12">
                <div class="alert alert-warning col-xs-12">
                  <p><strong>Note!</strong> This site has users registered and cannot be deleted.</p>
                </div>
              </section>
            </div>
    <?php endif; ?>

            <!-- Content -->
            <div class="row">
                <!-- Edit Form -->
                <form id="form_edit" method="post" action="" class="form-horizontal">
                    <section class="col-sm-6 col-xs-12" ng-app="formSite" ng-controller="formController">
                        <!-- Fields -->
                        <?php include 'templates/form-fields-site.php'; ?>
                    </section>

                    <!-- Error -->
                    <?php include 'templates/alert-error.php'; ?>

                    <!-- Submit Buttons -->
                    <section id="buttons" class="col-sm-6 col-xs-12">
                        <div class="btn-group btn-group-justified" role="group">
                            <div class="btn-group" role="group">
                                <button type="submit" id="btn_save" name="btn_save" class="btn btn-success">Save</button>
                            </div>
                        <?php if (!$has_wards && !$has_users) : ?>
                            <div class="btn-group" role="group">
                                <button type="submit" id="btn_delete" name="btn_delete" class="btn btn-danger">Delete</button>
                            </div>
                        <?php endif; ?>
                            <div class="btn-group" role="group">
                                <button type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-default">Cancel</button>
                            </div>
                        </div>
                    </section>
                </form>
            </div><!-- /.row -->

            <div class="row">
                <!-- Reset Password Form -->
                <form id="form_reset_password" method="post" action="" class="form-horizontal">
                    <section class="col-xs-12">
                        <!-- Fields -->
                        <?php include 'templates/form-fields-reset-password.php'; ?>
                    </section>
                </form>
            </div><!-- /.row -->
<?php endif; ?>
        </div><!-- /.container -->

        <!-- Footer -->
        <?php require_once 'templates/footer.php'; ?>

        <!-- Scripts -->
        <?php
        require_once 'includes/incl-js.html';
        require_once 'js/pass-admin-reset.html';
        ?>
        <script>
          $(document).ready(function(){

            // Show Username field only if JS is enabled.
            $('#field_username').removeClass('hidden');

            $('#btn_save, #btn_delete').click(function(e) {
              //Stop the form from submitting itself to the server.
              e.preventDefault();

              // Check which button was clicked and assign request type.
              if (this.id == 'btn_save') {
                var request_type = 'update';
              }
              else if (this.id == 'btn_delete') {
                var request_type = 'delete';
              }

              // Assign input values to variables.
              var sid = "<?php echo $sid; ?>";
              var hosp_name = $('#hosp_name').val();
              var suburb = $('#suburb').val();
              var site_code = $('#site_code').val();

              var data = {
                request_type: request_type,
                sid: sid,
                hosp_name: hosp_name,
                suburb: suburb,
                site_code: site_code
              };

              // Pass data to ajax form.
              $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: 'includes/edit-site.php',
                data: {
                  json_data: JSON.stringify(data)
                },
                success: function(result) {
                  // No errors.
                  if (true == result.success) {
                    $(location).attr('href', 'sites.php');
                  }
                  // Error occurred.
                  else {
                    errors = result.errors;
                    messages = '';
                    // Add breaks between each message.
                    for (var key in errors.messages) {
                      messages += errors.messages[key] + '<br>';
                    }
                    // Adds/removes .has-error from fields.
                    for (var key in errors.fields) {
                      if (errors.fields[key]) {
                        $('#' + key).parent().addClass('has-error');
                      }
                      else {
                        $('#' + key).parent().removeClass('has-error');
                      }
                    }
                    // Display messages.
                    $('#error').removeClass('hidden');
                    $('#error_message').html(messages);
                  }
                }
              });
            });

            $('#btn_cancel').click(function(e) {
              // Return to Sites without saving.
              $(location).attr('href', 'sites.php');
            });
          });
        </script>

    </body>
</html>

