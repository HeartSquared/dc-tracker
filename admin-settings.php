<?php
/**
 * Page - Admin settings.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

$base_path = basename(__DIR__);

require_once 'includes/common.php';

// Check if the logged in user has access to the page.
$has_access = check_access(get_roles_with_access('is admin'), $user);

$setting_loader = $container->getSettingLoader();
$pharmacy_domain_setting = $setting_loader->getSetting('pharmacy_domain', 'default');
$static_password_setting = $setting_loader->getSetting('static_password', 'default');
$ur_char_length_setting = $setting_loader->getSetting('ur_num', 'char_length');
$ur_leading_zero_setting = $setting_loader->getSetting('ur_num', 'leading_zero');
$ur_alphanumeric_setting = $setting_loader->getSetting('ur_num', 'alphanumeric');
$pharmacy_domain_id = $pharmacy_domain_setting->getId();
$static_password_id = $static_password_setting->getId();
$ur_char_length_id = $ur_char_length_setting->getId();
$ur_leading_zero_id = $ur_leading_zero_setting->getId();
$ur_alphanumeric_id = $ur_alphanumeric_setting->getId();

// Error setting for if Javascript is not enabled.
$success_hidden = true;
$error_hidden = true;
$error_message = '';
require_once 'includes/edit-settings.php';

$pharmacy_domain_value = isset($_POST[$pharmacy_domain_id]) ? $_POST[$pharmacy_domain_id] : $pharmacy_domain_setting->getValue();
$static_password_value = isset($_POST[$static_password_id]) ? $_POST[$static_password_id] : $static_password_setting->getValue();
$ur_char_length_value = isset($_POST[$ur_char_length_id]) ? $_POST[$ur_char_length_id] : $ur_char_length_setting->getValue();
$ur_leading_zero_value = isset($_POST[$ur_leading_zero_id]) ? $_POST[$ur_leading_zero_id] : $ur_leading_zero_setting->getValue();
$ur_alphanumeric_value = isset($_POST[$ur_alphanumeric_id]) ? $_POST[$ur_alphanumeric_id] : $ur_alphanumeric_setting->getValue();
?>

<!DOCTYPE html>
<html>

    <head>
        <?php
        require 'includes/incl-head.html';
        require 'includes/incl-form-style.html';
        ?>
        <title>DC-Tracker - Settings</title>
    </head>

    <body>
        <!-- Navigation Bar -->
        <?php require_once 'templates/nav.php'; ?>

        <!-- Main Content -->
        <div class="container">

<?php if (!$has_access) : ?>
            <!-- Forbidden -->
            <?php include 'templates/forbidden.html'; ?>

<?php else : ?>
            <!-- Page Title -->
            <div class="row">
                <section class="col-xs-12">
                    <h3>Settings</h3>
                </section>
            </div>

            <!-- Content -->
            <div class="row">
                <!-- Settings Form -->
                <form id="form_settings" method="post" action="" class="form-horizontal">

                    <section class="col-sm-6 col-xs-12">
                        <!-- Fields -->
                        <?php include 'templates/form-fields-settings.php'; ?>
                    </section>

                    <!-- Success -->
                    <section id="success" class="col-sm-6 col-xs-12
    <?php if ($success_hidden) : ?>
                        hidden
    <?php endif; ?>
                    ">
                        <div class="alert alert-success col-xs-12">
                            <p id="success_message">Your changes have been saved.</p>
                        </div>
                    </section>

                    <!-- Error -->
                    <?php include 'templates/alert-error.php'; ?>

                    <!-- Submit Button: Save -->
                    <section class="col-sm-6 col-xs-12">
                        <input type="submit" name="btn_save" class="btn btn-success col-xs-12" value="Save">
                    </section>

                </form>
            </div><!-- /.row -->

<?php endif; ?>
        </div><!-- /.container -->

        <!-- Footer -->
        <?php require_once 'templates/footer.php'; ?>

        <!-- Scripts -->
        <?php
        require_once 'includes/incl-js.html';
        require_once 'includes/incl-form-js.html';
        ?>
        <script>
          $(document).ready(function(){
            $('#form_settings').submit(function(e){
              //Stop the form from submitting itself to the server.
              e.preventDefault();
              // Assign input values to variables.
              var pharmacy_domain = $('#<?php echo $pharmacy_domain_id; ?>').val();
              var static_password = $('#<?php echo $static_password_id; ?>').val();
              var ur_num_char_length = $('#<?php echo $ur_char_length_id; ?>').val();
              var ur_num_leading_zero = $('#<?php echo $ur_leading_zero_id; ?>').is(':checked');
              var ur_num_alphanumeric = $('#<?php echo $ur_alphanumeric_id; ?>').is(':checked');

              var data = {
                <?php echo $pharmacy_domain_id; ?>: pharmacy_domain,
                <?php echo $static_password_id; ?>: static_password,
                <?php echo $ur_char_length_id; ?>: ur_num_char_length,
                <?php echo $ur_leading_zero_id; ?>: ur_num_leading_zero,
                <?php echo $ur_alphanumeric_id; ?>: ur_num_alphanumeric
              };

              // Pass data to ajax form.
              $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: 'includes/edit-settings.php',
                data: {
                  json_data: JSON.stringify(data)
                },
                success: function(result) {
                  // No errors.
                  if (true == result.success) {
                    fields = {
                      <?php echo $ur_char_length_id; ?>,
                      <?php echo $ur_leading_zero_id; ?>,
                      <?php echo $ur_alphanumeric_id; ?>
                    };
                    // Display success message.
                    $('#success').removeClass('hidden');
                    // Hide error message.
                    $('#error').addClass('hidden');

                    // Remove error class and values from fields.
                    for (var field in fields) {
                      $('#' + field).parent().removeClass('has-error');
                    }
                  }
                  // Error occurred.
                  else {
                    errors = result.errors;
                    messages = '';
                    // Add breaks between each message.
                    for (var key in errors.messages) {
                      messages += errors.messages[key] + '<br>';
                    }
                    // Adds/removes .has-error from fields.
                    for (var key in errors.fields) {
                      if (errors.fields[key]) {
                        $('#' + key).parent().addClass('has-error');
                      }
                      else {
                        $('#' + key).parent().removeClass('has-error');
                      }
                    }
                    // Display error message.
                    $('#error').removeClass('hidden');
                    $('#error_message').html(messages);
                    // Hide success message.
                    $('#success').addClass('hidden');
                  }
                }
              });
            });
          });
        </script>

    </body>
</html>
