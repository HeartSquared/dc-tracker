<?php
/**
 * Page - Display list of hospital sites.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

$base_path = basename(__DIR__);

require_once 'includes/common.php';

// Check if the logged in user has access to the page.
$has_access = check_access(get_roles_with_access('is admin'), $user);

$table_manager = $container->getTableManager();
$table = $container->getTableSites($user);

$collection_type = 'sites';
$sort_default = isset($_GET['sort_option']) ? $_GET['sort_option'] : 'site_name';
$sort_order = isset($_GET['sort_order']) && $_SESSION['prev_sort_option'] == $sort_default ? $_GET['sort_order'] : 'asc';
$_SESSION['prev_sort_option'] = $sort_default;

// Newly registered site details.
$new_site_username = isset($_SESSION['new_site_username']) ? $_SESSION['new_site_username'] : null;
$new_site_pass = isset($_SESSION['new_site_pass']) ? $_SESSION['new_site_pass'] : null;
unset($_SESSION['new_site_username']);
unset($_SESSION['new_site_pass']);
?>

<!DOCTYPE html>
<html>

    <head>
        <?php require_once 'includes/incl-head.html'; ?>
        <title>DC-Tracker - Sites</title>
    </head>

    <body>
        <!-- Navigation Bar -->
        <?php require_once 'templates/nav.php'; ?>

        <!-- Main Content -->
        <div class="container">

<?php if (!$has_access) : ?>
            <!-- Forbidden -->
            <?php include 'templates/forbidden.html'; ?>

<?php else : ?>
            <!-- Page Title -->
            <div class="row">
                <section class="col-xs-12">
                    <h3>Sites</h3>
                </section>
            </div>

    <?php if (!empty($new_site_username) && !empty($new_site_pass)) : ?>
            <!-- Newly registered site details -->
            <div class="row">
                <section class="col-xs-12">
                    <div class="alert alert-success">
                        <p><strong>New site successfully registered.</strong></p>
                        <p>
                            Username: <em><?php echo $new_site_username; ?></em>
                            | Password: <em><?php echo $new_site_pass; ?></em>
                        </p>
                    </div>
                </section>
            </div>
    <?php endif; ?>

            <!-- Table -->
            <div class="row">
                <section class="col-xs-12">
                    <?php $table_manager->displayTable($table, $collection_type, $sort_default, $sort_order); ?>
                </section>
            </div>

<?php endif; ?>
        </div><!-- /.container -->

        <!-- Footer -->
        <?php require_once 'templates/footer.php'; ?>

        <!-- Scripts -->
        <?php
        require_once 'includes/incl-js.html';
        require_once 'js/common-table.html';
        ?>

    </body>
</html>
