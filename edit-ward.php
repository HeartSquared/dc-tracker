<?php
/**
 * Page - Edit hospital ward details.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

$base_path = basename(__DIR__);

require_once 'includes/common.php';

use Service\UserLoader;

// Check if there is an entry to edit.
if (empty($_SESSION['editing_wid'])) {
    header('Location: wards.php');
}

// Check if the logged in user has access to the page.
$has_access = check_access(get_roles_with_access('is manager'), $user);

$ward_editor = $container->getWardEditor();
$site_loader = $container->getSiteLoader();

// Get the current logged in user's site id and code.
$user_sid = $user->getSid();
$user_site_code = $site_loader->getSiteBySid($user_sid)->getSiteCode();

// Get ward ID.
$wid = $_SESSION['editing_wid'];

$ward = $ward_editor->getWardByWid($wid);
$has_entries = $ward_editor->checkHasEntries($wid);
$sid = $ward->getSid();
$site_code = $site_loader->getSiteBySid($sid)->getSiteCode();

// If logged in user has a manager role, check ward to edit is from their site.
$user_is_manager = UserLoader::ROLE_MANAGER == $user->getRole();
$user_manages_account = $user_is_manager && ($user->getSid() == $sid);

// Error setting for if Javascript is not enabled.
$password_reset_success = false;
$new_password = null;
require_once 'includes/pass-admin-reset.php';
$error_hidden = true;
$error_message = '';
require_once 'includes/edit-ward.php';

// Value filling for if Javascript is not enabled.
$site_code_value = isset($_POST['site_code']) ? $_POST['site_code'] : $site_code;
$ward_name_value = isset($_POST['ward_name']) ? $_POST['ward_name'] : $ward->getWardName();
$ward_code_value = isset($_POST['ward_code']) ? $_POST['ward_code'] : $ward->getWardCode();
?>

<!DOCTYPE html>
<html>

    <head>
        <?php require_once 'includes/incl-head.html'; ?>
        <title>DC-Tracker - Edit Ward</title>
        <script src="vendor/angular.min.js"></script>
        <script>
          var app = angular.module('formWard', []);

          var site_code = <?php echo json_encode($site_code); ?>;
          var ward_code = <?php echo json_encode($ward->getWardCode()); ?>;

          app.controller('formController', function($scope) {
            $scope.site_code = site_code;
            $scope.ward_code = ward_code;
          });
        </script>
    </head>

    <body>
        <!-- Navigation Bar -->
        <?php require_once 'templates/nav.php'; ?>

        <!-- Main Content -->
        <div class="container">
<?php
// Check if the logged user does not have access, or if they do not
// manage the selected user.
if (!$has_access || ($user_is_manager && !$user_manages_account)) : ?>
            <!-- Forbidden -->
            <?php include 'templates/forbidden.html'; ?>

<?php else : ?>
            <!-- Page Title -->
            <div class="row">
                <section class="col-xs-12">
                    <h3>Edit Ward</h3>
                </section>
            </div>

            <?php include 'templates/alert-success--pass-reset.php'; ?>

    <?php if ($has_entries) : ?>
            <!-- Warning -->
            <div class="row">
                <section class="col-xs-12">
                    <div class="alert alert-warning col-xs-12">
                        <p><strong>Note!</strong> This ward has entries registered - Hospital Site and Ward Code cannot be changed.</p>
                    </div>
                </section>
            </div>
    <?php endif; ?>

            <!-- Content -->
            <div class="row">
                <!-- Edit Form -->
                <form id="form_edit" method="post" action="" class="form-horizontal">
                    <section class="col-sm-6 col-xs-12" ng-app="formWard" ng-controller="formController">
                        <!-- Fields -->
                        <?php include 'templates/form-fields-ward.php'; ?>
                    </section>

                    <!-- Error -->
                    <?php include 'templates/alert-error.php'; ?>

                    <!-- Submit Buttons -->
                    <section id="buttons" class="col-sm-6 col-xs-12">
                        <div class="btn-group btn-group-justified" role="group">
                            <div class="btn-group" role="group">
                                <button type="submit" id="btn_save" name="btn_save" class="btn btn-success">Save</button>
                            </div>
    <?php if (!$has_entries) : ?>
                            <div class="btn-group" role="group">
                                <button type="submit" id="btn_delete" name="btn_delete" class="btn btn-danger">Delete</button>
                            </div>
    <?php endif; ?>
                            <div class="btn-group" role="group">
                                <button type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-default">Cancel</button>
                            </div>
                        </div>
                    </section>
                </form>
            </div><!-- /.row -->

            <div class="row">
                <!-- Reset Password Form -->
                <form id="form_reset_password" method="post" action="" class="form-horizontal">
                    <section class="col-xs-12">
                        <!-- Fields -->
                        <?php include 'templates/form-fields-reset-password.php'; ?>
                    </section>
                </form>
            </div><!-- /.row -->
<?php endif; ?>
        </div><!-- /.container -->

        <!-- Footer -->
        <?php require_once 'templates/footer.php'; ?>

        <!-- Scripts -->
        <?php
        require_once 'includes/incl-js.html';
        require_once 'js/pass-admin-reset.html';
        ?>
        <script>
          $(document).ready(function(){

            // Show Username field only if JS is enabled.
            $('#field_username').removeClass('hidden');

            $('#btn_save, #btn_delete').click(function(e){
              //Stop the form from submitting itself to the server.
              e.preventDefault();

              // Check which button was clicked and assign request type.
              if (this.id == 'btn_save') {
                var request_type = 'update';
              }
              else if (this.id == 'btn_delete') {
                var request_type = 'delete';
              }

              // Assign input values to variables.
              var wid = "<?php echo $wid; ?>";
              var site_code = $('#site_code').val();
              var ward_name = $('#ward_name').val();
              var ward_code = $('#ward_code').val();
              var username = $('#username').text();

              var data = {
                request_type: request_type,
                wid: wid,
                site_code: site_code,
                ward_name: ward_name,
                ward_code: ward_code,
                username: username
              };

              // Pass data to ajax form.
              $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: 'includes/edit-ward.php',
                data: {
                  json_data: JSON.stringify(data)
                },
                success: function(result) {
                  // No errors.
                  if (true == result.success) {
                    $(location).attr('href', 'wards.php');
                  }
                  // Error occurred.
                  else {
                    errors = result.errors;
                    messages = '';
                    // Add breaks between each message.
                    for (var key in errors.messages) {
                      messages += errors.messages[key] + '<br>';
                    }
                    // Adds/removes .has-error from fields.
                    for (var key in errors.fields) {
                      if (errors.fields[key]) {
                        $('#' + key).parent().addClass('has-error');
                      }
                      else {
                        $('#' + key).parent().removeClass('has-error');
                      }
                    }
                    // Display messages.
                    $('#error').removeClass('hidden');
                    $('#error_message').html(messages);
                  }
                }
              });
            });

            $('#btn_cancel').click(function(e){
              // Return to Wards without saving.
              $(location).attr('href', 'wards.php');
            });
          });
        </script>

    </body>
</html>

