<?php
/**
 * Page - Edit discharge request entry.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

$base_path = basename(__DIR__);

require_once 'includes/common.php';

use Service\EntryLoader;

// Check if there is an entry to edit.
if (empty($_SESSION['editing_eid'])) {
    header('Location: index.php');
}

// Check if the logged in user has access to the page.
$has_access = check_access(get_roles_with_access('is pharmacy'), $user);

// Get character limit for UR Number.
$setting_loader = $container->getSettingLoader();
$ur_char_length_setting = $setting_loader->getSetting('ur_num', 'char_length');
$ur_char_limit = $ur_char_length_setting->getValue();

// Get user ID.
$eid = $_SESSION['editing_eid'];

$entry_editor = $container->getEntryEditor();

$entry = $entry_editor->getEntryById($eid);
$time_completed = $entry->getTimeCompleted();
$time_collected = $entry->getTimeCollected();
$collected_by = $entry->getCollectedBy();

// Error setting for if Javascript is not enabled.
$error_hidden = true;
$error_message = '';
require_once 'includes/edit-entry.php';

// Value filling for if Javascript is not enabled.
$ward_value = isset($_POST['ward']) ? $_POST['ward'] : $entry->getWid();
$firstname_value = isset($_POST['firstname']) ? $_POST['firstname'] : $entry->getFirstName();
$lastname_value = isset($_POST['lastname']) ? $_POST['lastname'] : $entry->getLastName();
$ur_num_value = isset($_POST['ur_num']) ? $_POST['ur_num'] : $entry->getUR();
$medprof_required_checked = isset($_POST['medprof_required']) ? $_POST['medprof_required'] : $entry->getRequireMedprof();
$discharge_date_ts = $entry->getDischargeDate();
$discharge_date_value = isset($_POST['discharge_date']) ? $_POST['discharge_date'] : date('d/m/Y', $discharge_date_ts);
$comments_value = isset($_POST['comments']) ? $_POST['comments'] : htmlspecialchars($entry->getComments(), ENT_QUOTES);
?>

<!DOCTYPE html>
<html>

    <head>
        <?php
        require_once 'includes/incl-head.html';
        require_once 'includes/incl-form-style.html';
        ?>
        <title>DC-Tracker - Edit Discharge Entry</title>
    </head>

    <body>
        <!-- Navigation Bar -->
        <?php require_once 'templates/nav.php'; ?>

        <!-- Main Content -->
        <div class="container">

<?php if (!$has_access) : ?>
            <!-- Forbidden -->
            <?php include 'templates/forbidden.html'; ?>

<?php else : ?>
            <!-- Page Title -->
            <div class="row">
                <section class="col-xs-12">
                    <h3>Edit Entry</h3>
                </section>
            </div>

            <!-- Content -->
            <div class="row">
                <!-- Edit Form -->
                <form id="form_edit" method="post" action="" class="form-horizontal">

                    <section class="col-sm-6 col-xs-12">
                        <!-- Fields -->
                        <?php include 'templates/form-fields-entry.php'; ?>
                    </section>

                    <section class="col-sm-6 col-xs-12">
                        <fieldset>
                            <legend>Status Details</legend>

                            <!-- Select: Status -->
                            <div class="form-group">
                                <label for="status" class="control-label col-xs-12">Status:</label>
                                <div class="col-sm-8 col-xs-12">
                                    <select id="status" name="status" class="form-control">
    <?php
    // Get all sites.
    $status_options = $entry_editor->getStatusOptions();
    if (!$entry_editor->checkIsCompleted($eid)) {
        $status_options = array_diff($status_options, [EntryLoader::STATUS_COMPLETED]);
    }
    if (!$entry_editor->checkIsCollected($eid)) {
        $status_options = array_diff($status_options, [EntryLoader::STATUS_COLLECTED]);
    }
    ?>

    <?php foreach ($status_options as $status) : ?>
        <?php if (EntryLoader::STATUS_CANCELLED == $status) : ?>
                                        <option disabled>─────</option>
        <?php endif; ?>

                                        <option value="<?php echo $status; ?>"
        <?php if ($status == $entry->getStatus()) : ?>
                                            selected
        <?php endif; ?>
                                        >
                                            <?php echo $status; ?>
                                        </option>
    <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

    <?php if ($entry_editor->checkIsCollected($eid)) : ?>
                            <!-- Collected By -->
                            <div class="form-group">
                                <label for="collected_by" class="control-label col-xs-12">Collected By:</label>
                                <div class="col-sm-6 col-xs-12">
                                    <select id="collected_by" name="collected_by" class="form-control">
        <?php
        // Get all sites.
        $collected_by_options = $entry_editor->getCollectedByOptions();
        ?>

        <?php foreach ($collected_by_options as $option) : ?>
                                        <option value="<?php echo $option; ?>"
            <?php if ($option == $collected_by) : ?>
                                            selected
            <?php endif; ?>
                                        >
                                            <?php echo $option; ?>
                                        </option>
        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="collected_by_details" class="control-label col-xs-12">Collected By Details:</label>
                                <div class="col-sm-10 col-xs-12">
                                    <input type="text" id="collected_by_details" name="collected_by_details" class="form-control" placeholder="Role/Name" value="<?php echo $entry->getCollectedByDetails(); ?>">
                                </div>
                            </div>
    <?php endif; ?>
                        </fieldset>
                    </section>

                    <!-- Error -->
                    <?php include 'templates/alert-error.php'; ?>

                    <!-- Submit Buttons -->
                    <section id="buttons" class="col-sm-6 col-xs-12 pull-right">
                        <div class="btn-group btn-group-justified" role="group">
                            <div class="btn-group" role="group">
                                <button type="submit" id="btn_save" name="btn_save" class="btn btn-success">Save</button>
                            </div>
                            <div class="btn-group" role="group">
                                <button type="submit" id="btn_delete" name="btn_delete" class="btn btn-danger">Delete</button>
                            </div>
                            <div class="btn-group" role="group">
                                <button type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-default">Cancel</button>
                            </div>
                        </div>
                    </section>

                </form>
            </div><!-- /.row -->

<?php endif; ?>
        </div><!-- /.container -->

        <!-- Footer -->
        <?php require_once 'templates/footer.php'; ?>

        <!-- Scripts -->
        <?php
        require_once 'includes/incl-js.html';
        require_once 'includes/incl-form-js.html';
        ?>
        <script>
          $(document).ready(function() {

            // Change the shown email field if JS is enabled.
            $('#field_discharge_date_no_js').html('');
            $('#field_discharge_date_js').removeClass('hidden');

            // Get stored date for entry and convert to compatible format.
            discharge_ts = <?php echo $discharge_date_ts; ?>;
            discharge_date_orig = new Date(discharge_ts * 1000);
            // Datepicker settings.
            $('#datepicker').datepicker({
              format: 'dd/mm/yyyy',
              weekStart: 1,
              todayBtn: 'linked',
              todayHighlight: true
            });
            $('#datepicker').datepicker('setDate', discharge_date_orig);
            $('#datepicker').datepicker('update');

            $('#btn_save').click(function(e){
              //Stop the form from submitting itself to the server.
              e.preventDefault();

              var request_type = 'update';

              // Assign input values to variables.
              var eid = "<?php echo $eid; ?>";
              var ward = $('#ward').val();
              var firstname = $('#firstname').val();
              var lastname = $('#lastname').val();
              var ur_num = $('#ur_num').val();
              var medprof_required = $('#medprof_required').is(':checked');
              var comments = $('#comments').val();
              var discharge_date = $('#discharge_date').val();
              var status = $('#status').val();
              var time_completed = "<?php echo $time_completed; ?>";
              var time_collected = "<?php echo $time_collected; ?>";
              var collected_by = $('#collected_by').val();
              var collected_by_details = $('#collected_by_details').val();

              var data = {
                request_type: request_type,
                eid: eid,
                ward: ward,
                firstname: firstname,
                lastname: lastname,
                ur_num: ur_num,
                medprof_required: medprof_required,
                comments: comments,
                discharge_date: discharge_date,
                status: status,
                time_completed: time_completed,
                time_collected: time_collected,
                collected_by: collected_by,
                collected_by_details: collected_by_details
              };

              // Pass data to ajax form.
              $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: 'includes/edit-entry.php',
                data: {
                  json_data: JSON.stringify(data)
                },
                success: function(result) {
                  // No errors.
                  if (true == result.success) {
                    $(location).attr('href', 'index.php');
                  }
                  // Error occurred.
                  else {
                    errors = result.errors;
                    messages = '';
                    // Add breaks between each message.
                    for (var key in errors.messages) {
                      messages += errors.messages[key] + '<br>';
                    }
                    // Adds/removes .has-error from fields.
                    for (var key in errors.fields) {
                      if (errors.fields[key]) {
                        $('#' + key).parent().addClass('has-error');
                      }
                      else {
                        $('#' + key).parent().removeClass('has-error');
                      }
                    }
                    // Display messages.
                    $('#error').removeClass('hidden');
                    $('#error_message').html(messages);
                  }
                }
              });
            });

            $('#btn_cancel').click(function(e){
              // Return to Discharges without saving.
              $(location).attr('href', 'index.php');
            });

            $('#btn_delete, #btn_dc_cancel').click(function(e){
              //Stop the form from submitting itself to the server.
              e.preventDefault();

              // Check which button was clicked and assign request type.
              if (this.id == 'btn_delete') {
                var request_type = 'delete';
              }
              else if (this.id == 'btn_dc_cancel') {
                var request_type = 'dc_cancel';
              }

              var eid = "<?php echo $eid; ?>";
              var data = {
                request_type: request_type,
                eid: eid
              };
              // Pass data to ajax form.
              $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: 'includes/edit-entry.php',
                data: {
                  json_data: JSON.stringify(data)
                },
                success: function(result) {
                  // No errors.
                  if (true == result.success) {
                    $(location).attr('href', 'index.php');
                  }
                }
              });
            });

          });
        </script>

    </body>
</html>

