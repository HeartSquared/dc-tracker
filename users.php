<?php
/**
 * Page - Display list of registered users.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

$base_path = basename(__DIR__);

require_once 'includes/common.php';

// Check if the logged in user has access to the page.
$has_access = check_access(get_roles_with_access('is manager'), $user);

$site_loader = $container->getSiteLoader();
$head_office = $site_loader->getHeadOffice();

$table_manager = $container->getTableManager();
$table = $container->getTableUsers($user);

$collection_type = 'users';

// Set table sorting default.
if (isset($_GET['sort_option'])) {
    $sort_default = $_GET['sort_option'];
} else {
    if ($head_office->getSid() == $user->getSid()) {
        $sort_default = 'site_name';
    } else {
        $sort_default = 'role';
    }
}

$sort_order = isset($_GET['sort_order']) && $_SESSION['prev_sort_option'] == $sort_default ? $_GET['sort_order'] : 'asc';
$_SESSION['prev_sort_option'] = $sort_default;

// Newly registered user details.
$new_user_username = isset($_SESSION['new_user_username']) ? $_SESSION['new_user_username'] : null;
$new_user_pass = isset($_SESSION['new_user_pass']) ? $_SESSION['new_user_pass'] : null;
unset($_SESSION['new_user_username']);
unset($_SESSION['new_user_pass']);
?>

<!DOCTYPE html>
<html>

    <head>
        <?php require_once 'includes/incl-head.html'; ?>
        <title>DC-Tracker - Users</title>
    </head>

    <body>
        <!-- Navigation Bar -->
        <?php require_once 'templates/nav.php'; ?>

        <!-- Main Content -->
        <div class="container">

<?php if (!$has_access): ?>
            <!-- Forbidden -->
            <?php include 'templates/forbidden.html'; ?>

<?php else : ?>
            <!-- Page Title -->
            <div class="row">
                <section class="col-xs-12">
                    <h3>
                        Users
    <?php if ($head_office->getSid() != $user->getSid()) : ?>
                        (
        <?php
        $site = $site_loader->getSiteBySid($user->getSid());
                            echo ucwords($site->getSiteName());
        ?>
                        )
    <?php endif; ?>
                    </h3>
                </section>
            </div>

    <?php if (!empty($new_user_username) && !empty($new_user_pass)) : ?>
            <!-- Newly registered user details -->
            <div class="row">
                <section class="col-xs-12">
                    <div class="alert alert-success">
                        <p><strong>New user successfully registered.</strong></p>
                        <p>
                            Username: <em><?php echo $new_user_username; ?></em>
                            | Password: <em><?php echo $new_user_pass; ?></em>
                        </p>
                    </div>
                </section>
            </div>
    <?php endif; ?>

          <!-- Table -->
          <div class="row">
              <section class="col-xs-12">
                  <?php $table_manager->displayTable($table, $collection_type, $sort_default, $sort_order); ?>
              </section>
          </div>

<?php endif; ?>
        </div><!-- /.container -->

        <!-- Footer -->
        <?php require_once 'templates/footer.php'; ?>

        <!-- Scripts -->
        <?php
        require_once 'includes/incl-js.html';
        require_once 'js/common-table.html';
        ?>

    </body>
</html>
