<?php
/**
 * Page - Display discharge request entries.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

$no_redirect = true;
$base_path = basename(__DIR__);

if (!file_exists('includes/config/config.php')) {
    header('Location: install.php');
}

require_once 'includes/common.php';

if (!$logged_in) {
    header('Location: login.php');
}

use Service\UserLoader;

$table_manager = $container->getTableManager();

$site_loader = $container->getSiteLoader();
$head_office = $site_loader->getHeadOffice();
$user_from_head_office = $head_office->getSid() == $user->getSid();

$user_roles = UserLoader::getUserRoles();
$user_role = method_exists($user, 'getRole') ? $user->getRole() : null;

$today = strtotime('today midnight');
$display_date = isset($_GET['select_date']) ? date_create_from_format('d/m/Y H:i:s', $_GET['select_date'] . ' 00:00:00') : date_create_from_format('U', $today);
$table = $container->getTableEntries($user, $display_date);

$collection_type = 'entries';
$sort_default = isset($_GET['sort_option']) ? $_GET['sort_option'] : 'lastname';
$sort_order = isset($_GET['sort_order']) && $_SESSION['prev_sort_option'] == $sort_default ? $_GET['sort_order'] : 'asc';
$_SESSION['prev_sort_option'] = $sort_default;

require_once 'includes/table-entries-modify.php';

// Value filling for if Javascript is not enabled.
$select_date_value = isset($_GET['select_date']) ? $_GET['select_date'] : date('d/m/Y', $today);
?>

<!DOCTYPE html>
<html>

    <head>
        <?php
        require_once 'includes/incl-head.html';
        require_once 'includes/incl-form-style.html';
        ?>
        <title>DC-Tracker - Discharges</title>
    </head>

    <body>
        <!-- Navigation Bar -->
        <?php require_once 'templates/nav.php'; ?>

        <!-- Main Content -->
        <div class="container">

            <!-- Page Title -->
            <div class="row">
                <section class="col-xs-12">
                    <h3>
                        Discharges ( <span id="displaying_date"><?php echo $select_date_value; ?></span> )
<?php if (!$user_from_head_office) : ?>
                        (
    <?php
    $site = $site_loader->getSiteBySid($user->getSid());
                            echo ucwords($site->getSiteName());

    if ('ward' == $user_credential->getUserType()) {
        $ward_loader = $container->getWardLoader();
        $ward = $ward_loader->getWardByCid($user->getCid());
                            echo ' - ' . ucwords($ward->getWardName());
    }
    ?>
                        )
<?php endif; ?>
                    </h3>
                </section>
            </div>

            <!-- Input calendar -->
            <div class="row">
                <section class="col-sm-4">
                    <form name="calendar" method="get" action="">
                        <div class="row form-group">
                            <div class="col-xs-10">
                                <!-- Javascript disabled -->
                                <div id="field_discharge_date_no_js">
                                    <input name="select_date" class="form-control" type="text" value="<?php echo $select_date_value; ?>">
                                </div>
                                <!-- Javascript enabled -->
                                <div id="field_discharge_date_js" class="input-group hidden">
                                    <div id="discharge_date" class="input-group date">
                                        <input id="select_date" class="form-control" type="text" value="<?php echo $select_date_value; ?>">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <input id="btn_filter" type="submit" class="btn btn-default" value="Filter">
                            </div>
                        </div>
                    </form>
                </section>
            </div>

            <!-- Content -->
            <div class="row">
<?php if ($user_from_head_office && $user_roles['sysadmin'] != $user->getRole()) : ?>
                <!-- Head office summary tables -->
                <section id="tables">
                    <?php $table_manager->displayHeadOfficeEntriesTable($table); ?>
                </section>

<?php else : ?>
                <!-- Table -->
                <section class="col-xs-12">
                    <?php $table_manager->displayTable($table, $collection_type, $sort_default, $sort_order); ?>
                </section>
<?php endif; ?>
            </div><!-- /.row -->

        </div><!-- /.container -->

        <!-- Footer -->
        <?php require_once 'templates/footer.php'; ?>

        <!-- Scripts -->
        <?php
        require_once 'includes/incl-js.html';
        require_once 'includes/incl-form-js.html';
        require_once 'js/common-table.html';
        ?>
        <script>
          $(document).ready(function(){
            // Change the shown email field if JS is enabled.
            $('#field_discharge_date_no_js').addClass('hidden');
            $('#field_discharge_date_js').removeClass('hidden');

            // Datepicker settings.
            $('#discharge_date').datepicker({
              format: 'dd/mm/yyyy',
              weekStart: 1,
              todayBtn: 'linked',
              todayHighlight: true
            });
            $('#discharge_date').datepicker('setDate', 'now');

            function submitAjaxCall(e, retrieve_method, request_type, eid = undefined, collected_by = undefined, collected_by_details = undefined) {
              // Stop the form from submitting itself to the server.
              e.preventDefault();

              // Assign input values to variables.
              var select_date = $('#select_date').val();
              var collection_type = "<?php echo $collection_type; ?>";
              var sort_id = document.querySelector('.sort-select').id;
              var split_sort_id = sort_id.split('-');
              var sort_option = split_sort_id[1];
              var sort_order = split_sort_id[2];

              var data = {
                request_type: request_type,
                eid: eid,
                collected_by: collected_by,
                collected_by_details: collected_by_details,
                select_date: select_date,
                collection_type: collection_type,
                sort_option: sort_option,
                sort_order: sort_order
              };

              // Pass data to ajax form.
              var ajaxCall = $.ajax({
                type: retrieve_method,
                url: 'includes/table-entries-modify.php',
                data: {
                  json_data: JSON.stringify(data)
                }
              });

              $.when(ajaxCall).done(function(result) {
                // No errors.
                $('#displaying_date').html(select_date);
                $('#table').html(result);
              });
            }

            function refreshHeadOfficeDisplay(e) {
              // Stop the form from submitting itself to the server.
              e.preventDefault();

              // Assign input values to variables.
              var select_date = $('#select_date').val();

              var data = {
                request_type: 'refresh_head_office_display',
                select_date: select_date,
              };

              // Pass data to ajax form.
              var ajaxCall = $.ajax({
                type: 'GET',
                url: 'includes/table-entries-modify.php',
                data: {
                  json_data: JSON.stringify(data)
                }
              });

              $.when(ajaxCall).done(function(result) {
                // No errors.
                $('#tables').html(result);
              });
            }

            $('#btn_filter').click(
              function(e) {
                var user_from_head_office = "<?php echo $user_from_head_office; ?>";
                var role_sysadmin = "<?php echo $user_roles['sysadmin']; ?>";
                var user_role = "<?php echo $user_role; ?>";

                if (user_from_head_office && (role_sysadmin != user_role)) {
                  refreshHeadOfficeDisplay(e);
                }
                else {
                  submitAjaxCall(e, 'GET', 'filter');
                }
              }
            );

            $('#table').on('submit', '[id*="form_complete"]',
              function(e) {
                eid = this.id.split('_')[2];
                submitAjaxCall(e, 'POST', 'complete', eid);
              }
            );

            $('#table').on('submit', '[id*="form_collect"]',
              function(e) {
                var eid = this.id.split('_')[2];
                var collected_by = $('#collected_by_' + eid).val();
                if (('Nurse' == collected_by) || ('Other' == collected_by)) {
                  if ('Nurse' == collected_by) {
                    var details = window.prompt('Please enter the nurse\'s name (if known).');
                  }
                  else {
                    var details = window.prompt('Please enter any details (eg. Role/Name)');
                  }
                  if (details) {
                    submitAjaxCall(e, 'POST', 'collect', eid, collected_by, details);
                  }
                  else {
                    submitAjaxCall(e, 'POST', 'collect', eid, collected_by);
                  }
                }
                else {
                  submitAjaxCall(e, 'POST', 'collect', eid, collected_by);
                }
              }
            );
          });
        </script>

    </body>
</html>
