<?php
/**
 * Page - Edit user profile.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

$base_path = basename(__DIR__);

require_once 'includes/common.php';

$credential_editor = $container->getCredentialEditor();
$user_editor = $container->getUserEditor();

// Check if the logged in user has access to the page.
$has_access = check_access(get_roles_with_access('is type user'), $user);

if ($has_access) {
    $uid = $user->getUid();
    // Get automatically generated email based on user's name.
    $pharmacy_domain_value = get_pharmacy_domain($container);
    $generated_email = "{$user->getFirstName()}.{$user->getLastName()}@{$pharmacy_domain_value}";
    $generated_email = strtolower($generated_email);
}

// Error setting for if Javascript is not enabled.
$error_hidden = true;
$error_message = '';
require_once 'includes/edit-profile.php';

// Value filling for if Javascript is not enabled.
$firstname_value = isset($_POST['firstname']) ? $_POST['firstname'] : $user->getFirstName();
$lastname_value = isset($_POST['lastname']) ? $_POST['lastname'] : $user->getLastName();
$email_value = isset($_POST['email']) ? $_POST['email'] : $user->getEmail();

?>

<!DOCTYPE html>
<html>

    <head>
        <?php require_once 'includes/incl-head.html'; ?>
        <title>DC-Tracker - Edit Profile</title>
<?php if ($has_access) : ?>
        <script src="vendor/angular.min.js"></script>
        <script>
          var app = angular.module('editUser', []);

          var firstname = <?php echo json_encode($user->getFirstName()); ?>;
          var lastname = <?php echo json_encode($user->getLastName()); ?>;
          var domain_name = <?php echo json_encode($pharmacy_domain_value); ?>;

          app.controller('formController', function($scope) {
            $scope.firstname = firstname;
            $scope.lastname = lastname;
            $scope.domain_name = '@' + domain_name;
          });
        </script>
<?php endif; ?>
    </head>

    <body>
        <!-- Navigation Bar -->
        <?php require_once 'templates/nav.php'; ?>

        <!-- Main Content -->
        <div class="container">

<?php if (!$has_access) : ?>
            <!-- Forbidden -->
            <?php include 'templates/forbidden.html'; ?>

<?php else : ?>
            <!-- Page Title -->
            <div class="row">
              <section class="col-xs-12">
                <h3>Edit Profile</h3>
              </section>
            </div>

    <?php if (1 == $uid) : ?>
            <!-- Warning -->
            <div class="row">
                <section class="col-xs-12">
                    <div class="alert alert-warning col-xs-12">
                        <p><strong>Note!</strong> This is User 1 (System Admin).</p>
                    </div>
                </section>
            </div>
    <?php endif; ?>

            <!-- Content -->
            <div class="row">
                <!-- Edit Form -->
                <form id="form_edit" method="post" action="" class="form-horizontal">

                    <section class="col-sm-6 col-xs-12" ng-app="editUser" ng-controller="formController">
                        <!-- Input: First Name -->
                        <div class="form-group">
                            <label for="firstname" class="control-label col-xs-12">First Name:</label>
                            <div class="col-xs-12">
                                <input type="text" id="firstname" name="firstname" class="form-control" ng-model="firstname" ng-value="firstname" placeholder="required" value="<?php echo $firstname_value; ?>" required>
                            </div>
                        </div>

                        <!-- Input: Last Name -->
                        <div class="form-group">
                            <label for="lastname" class="control-label col-xs-12">Last Name:</label>
                            <div class="col-xs-12">
                                <input type="text" id="lastname" name="lastname" class="form-control" ng-model="lastname" ng-value="lastname" placeholder="required" value="<?php echo $lastname_value; ?>" required>
                            </div>
                        </div>

                        <!-- Email -->
                        <div class="form-group">
                            <label for="email" class="control-label col-xs-12">Email:</label>
                            <div class="col-xs-12">
    <?php if ($generated_email != $user->getEmail()) : ?>
                                <input type="text" id="email" name="email" class="form-control" value="<?php echo $email_value; ?>" required>
    <?php else : ?>
                                <div id="field_email_no_js">
                                    <input type="text" name="email" class="form-control" value="<?php echo $email_value; ?>" required>
                                </div>
                                <div id="field_email_js" class="hidden">
                                    <div class="input-group">
                                        <input id="email" type="text" class="form-control" value='{{firstname + "." + lastname + domain_name | lowercase}}' disabled required>
                                        <span id="btn_edit_email" class="input-group-btn">
                                            <button type="button" class="btn btn-default">Edit</button>
                                        </span>
                                    </div>
                                </div>
    <?php endif; ?>
                            </div>
                        </div>
                    </section>

                    <!-- Error -->
                    <?php include 'templates/alert-error.php'; ?>

                    <!-- Submit Buttons -->
                    <section id="buttons" class="col-sm-6 col-xs-12">
                        <div class="btn-group btn-group-justified" role="group">
                            <div class="btn-group" role="group">
                                <button type="submit" id="btn_save" name="btn_save" class="btn btn-success">Save</button>
                            </div>
                            <div class="btn-group" role="group">
                                <button type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-default">Cancel</button>
                            </div>
                        </div>
                    </section>

                </form>
            </div><!-- /.row -->

<?php endif; ?>
        </div><!-- /.container -->

        <!-- Footer -->
        <?php require_once 'templates/footer.php'; ?>

        <!-- Scripts -->
        <?php require_once 'includes/incl-js.html'; ?>
        <script>
          $(document).ready(function() {

            // Change the shown email field if JS is enabled.
            $('#field_email_no_js').html('');
            $('#field_email_js').removeClass('hidden');

            // Enable manual editing of email.
            $('#btn_edit_email').click(function() {
              $('#email').removeAttr('disabled');
              $('#email').parent().removeClass('input-group');
              $('#btn_edit_email').addClass('hidden');
            });

            $('#btn_save').click(function(e) {
              //Stop the form from submitting itself to the server.
              e.preventDefault();

              // Assign input values to variables.
              var uid = "<?php echo $uid; ?>";
              var firstname = $('#firstname').val();
              var lastname = $('#lastname').val();
              var email = $('#email').val();

              var data = {
                uid: uid,
                firstname: firstname,
                lastname: lastname,
                email: email
              };

              // Pass data to ajax form.
              $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: 'includes/edit-profile.php',
                data: {
                  json_data: JSON.stringify(data)
                },
                success: function(result) {
                  // No errors.
                  if (true == result.success) {
                    $(location).attr('href', 'index.php');
                  }
                  // Error occurred.
                  else {
                    errors = result.errors;
                    messages = '';
                    // Add breaks between each message.
                    for (var key in errors.messages) {
                      messages += errors.messages[key] + '<br>';
                    }
                    // Adds/removes .has-error from fields.
                    for (var key in errors.fields) {
                      if (errors.fields[key]) {
                        $('#' + key).parent().addClass('has-error');
                      }
                      else {
                        $('#' + key).parent().removeClass('has-error');
                      }
                    }
                    // Display messages.
                    $('#error').removeClass('hidden');
                    $('#error_message').html(messages);
                  }
                }
              });
            });

            $('#btn_cancel').click(function(e) {
              // Return to Discharges without saving.
              $(location).attr('href', 'index.php');
            });
          });
        </script>

    </body>
</html>

