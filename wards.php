<?php
/**
 * Page - Display list of registered wards.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

$base_path = basename(__DIR__);

require_once 'includes/common.php';

// Check if the logged in user has access to the page.
$has_access = check_access(get_roles_with_access('is manager'), $user);

$site_loader = $container->getSiteLoader();
$head_office = $site_loader->getHeadOffice();

$table_manager = $container->getTableManager();
$table = $container->getTableWards($user);

$collection_type = 'wards';

// Set table sorting default.
if (isset($_GET['sort_option'])) {
    $sort_default = $_GET['sort_option'];
} else {
    if ($head_office->getSid() == $user->getSid()) {
        $sort_default = 'site_name';
    } else {
        $sort_default = 'ward_name';
    }
}

$sort_order = isset($_GET['sort_order']) && $_SESSION['prev_sort_option'] == $sort_default ? $_GET['sort_order'] : 'asc';
$_SESSION['prev_sort_option'] = $sort_default;

// Newly registered ward details.
$new_ward_username = isset($_SESSION['new_ward_username']) ? $_SESSION['new_ward_username'] : null;
$new_ward_pass = isset($_SESSION['new_ward_pass']) ? $_SESSION['new_ward_pass'] : null;
unset($_SESSION['new_ward_username']);
unset($_SESSION['new_ward_pass']);
?>

<!DOCTYPE html>
<html>

    <head>
        <?php require_once 'includes/incl-head.html'; ?>
        <title>DC-Tracker - Wards</title>
    </head>

    <body>
        <!-- Navigation Bar -->
        <?php require_once 'templates/nav.php'; ?>

        <!-- Main Content -->
        <div class="container">

<?php if (!$has_access) : ?>
            <!-- Forbidden -->
            <?php include 'templates/forbidden.html'; ?>

<?php else : ?>
            <!-- Page Title -->
            <div class="row">
                <section class="col-xs-12">
                    <h3>
                        Wards
    <?php if ($head_office->getSid() != $user->getSid()) : ?>
                        (
        <?php
        $site = $site_loader->getSiteBySid($user->getSid());
                            echo ucwords($site->getSiteName());
        ?>
                        )
    <?php endif; ?>
                    </h3>
                </section>
            </div>

    <?php if (!empty($new_ward_username) && !empty($new_ward_pass)) : ?>
            <!-- Newly registered ward details -->
            <div class="row">
                <section class="col-xs-12">
                    <div class="alert alert-success">
                        <p><strong>New ward successfully registered.</strong></p>
                        <p>
                            Username: <em><?php echo $new_ward_username; ?></em>
                            | Password: <em><?php echo $new_ward_pass; ?></em>
                        </p>
                    </div>
                </section>
            </div>
    <?php endif; ?>

          <!-- Table -->
          <div class="row">
              <section class="col-xs-12">
                  <?php $table_manager->displayTable($table, $collection_type, $sort_default, $sort_order); ?>
              </section>
          </div>

<?php endif; ?>
        </div><!-- /.container -->

        <!-- Footer -->
        <?php require_once 'templates/footer.php'; ?>

        <!-- Scripts -->
        <?php
        require_once 'includes/incl-js.html';
        require_once 'js/common-table.html';
        ?>

    </body>
</html>
