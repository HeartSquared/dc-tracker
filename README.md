# DC-Tracker

##### Hospital pharmacy patient discharge order tracker

Keep track of patient discharge orders from the wards.

##### Main features

* Pharmacy and Ward user accounts
* Simple form to discharge order entry
* Table of entries
    * For pharmacy head office staff:
        * Summary of entries across the different registered hospital sites
    * For pharmacy staff based at a hospital:
        * Ability to edit and mark the entry as Complete/Collected
    * For hospital ward staff:
        * See the status of orders (entered by pharmacy) for their ward

**Note:** I will be unlikely to add new features to this project as I wish to rebuild it using a framework. However, you may contact me if you happen across bugs, or if your feature request does not require a lot of work.

Special thanks to David Vien for the initial project idea, and to [Sunny Yiu](https://gitlab.com/sunnz) for assistance during the development phase.

## Requirements

### PHP

* PHP-PDO
* PHP 5.5 or higher
    * PHP 5.6 and 7.1 are used in development


## Hosting

### Note

I recommend you host the app on your local network or a secure hosting service to avoid data breaches. You will also need to give access to hospital ward staff if you wish to allow them to see the status of their order.

### macOS

Please install PHP 5.6 or PHP 7, and PHP-PDO via Homebrew.

For a temporary host, run php server in the root of this project's directory:

    php -S 127.0.0.1:9999

### Database (select one)

* MySQL
    * Create your database prior to installation
    * If using MySQL 8+, you will need to alter your login settings to be compatible (see [here](https://stackoverflow.com/questions/49083573/php-7-2-2-mysql-8-0-pdo-gives-authentication-method-unknown-to-the-client-ca))
* PostgreSQL
    * Create your database prior to installation
* SQLite
    * PDO-SQLite may be required on some systems that don't come with PHP-PDO package
    * **Note:** You should only choose this option when installing if you just wish to try a demo, as it is less secure than the other options

## Installation

Once the above has been set up, go to `/install.php` (you should be redirected from the other pages if needed) and follow the installation form.

If you come across issues during installation or need to re-install, you may need to delete the `/includes/config/` folder and contents (generated on install).

## Timezone

You may get timezone warnings if you are installing PHP for the first time.

The warnings come from the design of PHP, which you can fix by supplying a timezone for ``date.timezone`` in your PHP setting.

On macOS where you have installed PHP 5.6 via Homebrew, create a file in ``/local/etc/php/5.6/conf.d/timezone.ini``.

On CentOS/RHEL 7 and similiar Linux distro, create a file in ``/etc/php.d/timezone.ini``.

Put the following in to ``timezone.ini``: (replace "Australia/Melbourne" with your timezone.)

    [Date]
    ; Defines the default timezone used by the date functions
    ; http://php.net/date.timezone
    date.timezone = Australia/Melbourne

Then restart your PHP server and the warnings should be fixed. If you used the above command (``php -S 127.0.0.1:9999``) to start a local PHP server, just press ``CTRL-C`` to stop your PHP server, then restart by entering the command (``php -S 127.0.0.1:9999``) again.
