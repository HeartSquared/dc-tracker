<?php
/**
 * Credential class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Model;

/**
 * User login credentials.
 */
class Credential
{
    private $cid;
    private $username;
    private $password;
    private $user_type;
    private $require_pass_reset;

    /**
     * Constructor.
     *
     * @param string $username Username.
     */
    public function __construct($username)
    {
        $this->username = $username;
    }

    /**
     * GETTERS.
     */

    /**
     * Get credential id.
     *
     * @return int
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * Get username.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Get stored password (hashed).
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get user type.
     *
     * @return string
     */
    public function getUserType()
    {
        return $this->user_type;
    }

    /**
     * Get stored value of if user needs to reset their password.
     *
     * @return bool
     */
    public function getRequirePassReset()
    {
        return $this->require_pass_reset;
    }

    /**
     * SETTERS.
     */

    /**
     * Set credential id.
     *
     * @param int $cid Credential id.
     *
     * @return void
     */
    public function setCid($cid)
    {
        $this->cid = $cid;
    }

    /**
     * Set password (hashed).
     *
     * @param string $password Password (hashed).
     *
     * @return void
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Set user type.
     *
     * @param string $user_type User type.
     *
     * @return void
     */
    public function setUserType($user_type)
    {
        $this->user_type = $user_type;
    }

    /**
     * Set stored value of if user needs to reset their password.
     *
     * @param bool $require_pass Whether user needs to reset their password.
     *
     * @return void
     */
    public function setRequirePassReset($require_pass = true)
    {
        $this->require_pass_reset = $require_pass;
    }
}
