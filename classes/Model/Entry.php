<?php
/**
 * Entry class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Model;

/**
 * Hospital discharge request entry.
 */
class Entry
{
    private $eid;
    private $wid;
    private $firstname;
    private $lastname;
    private $ur_num;
    private $require_medprof;
    private $comments;
    private $discharge_date;
    private $time_received;
    private $status;
    private $time_completed;
    private $time_collected;
    private $collected_by;
    private $collected_by_details;

    /**
     * Constructor.
     *
     * @param int $eid Entry id.
     */
    public function __construct($eid)
    {
        $this->eid = $eid;
    }

    /**
     * GETTERS.
     */

    /**
     * Get entry id.
     *
     * @return int
     */
    public function getEid()
    {
        return $this->eid;
    }

    /**
     * Get ward id.
     *
     * @return int
     */
    public function getWid()
    {
        return $this->wid;
    }

    /**
     * Get patient's first name.
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstname;
    }

    /**
     * Get patient's last name.
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastname;
    }

    /**
     * Get patient's UR number.
     *
     * @return int
     */
    public function getUR()
    {
        return $this->ur_num;
    }

    /**
     * Get value of if a medication profile is required.
     *
     * @return bool
     */
    public function getRequireMedprof()
    {
        return $this->require_medprof;
    }

    /**
     * Get comments.
     *
     * @return string|null
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Get discharge date.
     *
     * @return string Discharge date timestamp (midnight).
     */
    public function getDischargeDate()
    {
        return $this->discharge_date;
    }

    /**
     * Get time received.
     *
     * @return string Time received timestamp.
     */
    public function getTimeReceived()
    {
        return $this->time_received;
    }

    /**
     * Get entry completion/collection status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get time completed.
     *
     * @return string|null Time completed timestamp.
     */
    public function getTimeCompleted()
    {
        return $this->time_completed;
    }

    /**
     * Get time collected.
     *
     * @return string|null Time collected timestamp.
     */
    public function getTimeCollected()
    {
        return $this->time_collected;
    }

    /**
     * Get title of entity who collected the discharge.
     *
     * @return string|null
     */
    public function getCollectedBy()
    {
        return $this->collected_by;
    }

    /**
     * Get extra details of entity who collected the discharge.
     *
     * @return string|null
     */
    public function getCollectedByDetails()
    {
        return $this->collected_by_details;
    }

    /**
     * SETTERS.
     */

    /**
     * Set ward id.
     *
     * @param int $wid Ward id.
     *
     * @return void
     */
    public function setWid($wid)
    {
        $this->wid = $wid;
    }

    /**
     * Set patient's first name.
     *
     * @param string $firstname Patient's first name.
     *
     * @return void
     */
    public function setFirstName($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * Set patient's last name.
     *
     * @param string $lastname Patient's last name.
     *
     * @return void
     */
    public function setLastName($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * Set patient's UR number.
     *
     * @param int $ur_num Patient's UR number.
     *
     * @return void
     */
    public function setUR($ur_num)
    {
        $this->ur_num = $ur_num;
    }

    /**
     * Set value of if a medication profile is required.
     *
     * @param bool $require_medprof Medication profile is required.
     *
     * @return void
     */
    public function setRequireMedprof($require_medprof)
    {
        $this->require_medprof = $require_medprof;
    }

    /**
     * Set comments.
     *
     * @param string|null $comments Comments.
     *
     * @return void
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }

    /**
     * Set discharge date.
     *
     * @param string $discharge_date Discharge date timestamp (midnight).
     *
     * @return void
     */
    public function setDischargeDate($discharge_date)
    {
        $this->discharge_date = $discharge_date;
    }

    /**
     * Set time received.
     *
     * @param integer $time_received Time received timestamp.
     *
     * @return void
     */
    public function setTimeReceived($time_received)
    {
        $this->time_received = $time_received;
    }

    /**
     * Set entry completion/collection status.
     *
     * @param string $status Entry status.
     *
     * @return void
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Set time completed.
     *
     * @param string|null $time_completed Time completed timestamp.
     *
     * @return void
     */
    public function setTimeCompleted($time_completed)
    {
        $this->time_completed = $time_completed;
    }

    /**
     * Set time collected.
     *
     * @param string|null $time_collected Time collected timestamp.
     *
     * @return void
     */
    public function setTimeCollected($time_collected)
    {
        $this->time_collected = $time_collected;
    }

    /**
     * Set title of entity who collected the discharge.
     *
     * @param string|null $collected_by Entity who collected the discharge.
     *
     * @return void
     */
    public function setCollectedBy($collected_by)
    {
        $this->collected_by = $collected_by;
    }

    /**
     * Set extra details of entity who collected the discharge.
     *
     * @param string|null $collected_by_details Details of who collected discharge.
     *
     * @return void
     */
    public function setCollectedByDetails($collected_by_details)
    {
        $this->collected_by_details = $collected_by_details;
    }
}
