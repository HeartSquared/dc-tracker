<?php
/**
 * TableEntries class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Model;

use Service\CredentialLoader;
use Service\SiteLoader;
use Service\EntryLoader;
use Service\UserLoader;
use Service\WardLoader;
use Service\SettingLoader;

/**
 * Data for table display of discharge request entries.
 */
class TableEntries extends AbstractTable
{
    private $display_date_from;
    private $display_date_to;
    private $entry_loader;
    private $user_roles;
    private $setting_loader;

    /**
     * Constructor.
     *
     * @param \PDO             $pdo          PDO connection.
     * @param AbstractUserType $user         User making the search.
     * @param \DateTime        $display_date Date to filter results.
     */
    public function __construct(\PDO $pdo, AbstractUserType $user, \DateTime $display_date)
    {
        // Inherited properties.
        $this->pdo = $pdo;
        $this->user = $user;
        $credential_loader = new CredentialLoader($this->pdo);
        $this->credential = $credential_loader->getCredentialById($user->getCid());
        $this->site_loader = new SiteLoader($this->pdo);
        $this->head_office = $this->site_loader->getHeadOffice();

        // Private properties.
        $this->display_date_from = date_format($display_date, 'U');
        $this->display_date_to = strtotime('+1 day', $this->display_date_from);
        $this->entry_loader = new EntryLoader($this->pdo);
        $user_loader = new UserLoader($this->pdo);
        $this->user_roles = $user_loader->getUserRoles();
        $this->setting_loader = new SettingLoader($this->pdo);
    }

    /**
     * {@inheritDoc}
     *
     * @return array Database column name (for sorting) => Table header.
     */
    public function getColumns()
    {
        $columns = [];
        $from_head_office = $this->head_office->getSid() == $this->user->getSid();
        $is_sysadmin = $this->user_roles['sysadmin'] == $this->user->getRole();

        // Add site column if user is a sysadmin from head office.
        if ($from_head_office && $is_sysadmin) {
            $columns = [
                'sid' => 'Site',
            ];
        }

        if ($this->displayReadOnly()) {
            $columns = array_merge(
                $columns,
                [
                    'lastname' => 'Patient',
                    'ur_num' => 'UR',
                    'time_received' => 'Received',
                    'discharge_date' => 'Discharge',
                    'require_medprof' => 'Medprof',
                    'status' => 'Status',
                ]
            );
        } else {
            $columns = array_merge(
                $columns,
                [
                    'ward_code' => 'Ward',
                    'lastname' => 'Patient',
                    'ur_num' => 'UR',
                    'time_received' => 'Received',
                    'discharge_date' => 'Discharge',
                    'require_medprof' => 'Medprof',
                    'comments' => 'Comments',
                    'time_completed' => 'Completed',
                    'time_collected' => 'Collected By',
                ]
            );
        }

        return $columns;
    }

    /**
     * Gets table columns to query.
     *
     * @return array
     */
    protected function getTableColumns()
    {
        $columns = [];

        if ($this->head_office->getSid() == $this->user->getSid()) {
            $columns = [
                'sid',
            ];
        }

        if ($this->displayReadOnly()) {
            $columns = array_merge(
                $columns,
                [
                    'firstname',
                    'lastname',
                    'ur_num',
                    'time_received',
                    'discharge_date',
                    'require_medprof',
                    'time_completed',
                    'collected_by',
                    'time_collected',
                ]
            );
        } else {
            $columns = array_merge(
                $columns,
                [
                    'ward_code',
                    'firstname',
                    'lastname',
                    'ur_num',
                    'time_received',
                    'discharge_date',
                    'require_medprof',
                    'comments',
                    'time_completed',
                    'collected_by',
                    'time_collected',
                ]
            );
        }

        return $columns;
    }

    /**
     * {@inheritDoc}
     *
     * @param string $sort_option Column to order by.
     * @param string $sort_order  Sort direction (asc/desc).
     *
     * @return string Query statement.
     */
    protected function getDataQuery($sort_option, $sort_order)
    {
        $columns = $this->getTableColumns();
        $columns = array_flip($columns);
        $keys = $this->getKeys($columns);

        // Assign Entry Id to keys (hidden field for editing).
        $keys .= ', eid';
        $query = "SELECT $keys FROM dc_entries JOIN wards ON dc_entries.wid = wards.wid WHERE ((time_received >= :date_from AND time_received < :date_to) OR (time_received < :date_from AND time_collected IS NULL AND status != :status_cancelled) OR (discharge_date >= :date_from AND discharge_date < :date_to))";
        if ($this->head_office->getSid() != $this->user->getSid()) {
            $query .= " AND sid = :sid";
        }
        if ('ward' == $this->credential->getUserType()) {
            $query .= " AND dc_entries.wid = :wid";
        }
        $query .= " ORDER BY LOWER($sort_option) $sort_order, LOWER(lastname) ASC";

        return $query;
    }

    /**
     * {@inheritDoc}
     *
     * @param string $stmt Query statement.
     *
     * @return void
     */
    protected function applyParams($stmt)
    {
        // Get site id of logged in user.
        if ($this->head_office->getSid() != $this->user->getSid()) {
            $sid = $this->user->getSid();
        }
        if ('ward' == $this->credential->getUserType()) {
            $ward_loader = new WardLoader($this->pdo);
            $ward = $ward_loader->getWardByCid($this->user->getCid());
            $wid  = $ward->getWid();
        }

        // Apply required parameters.
        $stmt->bindValue(':status_cancelled', EntryLoader::STATUS_CANCELLED);
        $stmt->bindParam(':date_from', $this->display_date_from);
        $stmt->bindParam(':date_to', $this->display_date_to);
        if (isset($sid)) {
            $stmt->bindParam(':sid', $sid);
        }
        if (isset($wid)) {
            $stmt->bindParam(':wid', $wid);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @return bool
     */
    public function displayReadOnly()
    {
        $sid = $this->user->getSid();
        $user_type = $this->credential->getUserType();
        $is_sysadmin = $this->user_roles['sysadmin'] == $this->user->getRole();
        $from_head_office = $this->head_office->getSid() == $sid;

        if ('ward' == $user_type || (!$is_sysadmin && $from_head_office)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Modify the table results.
     *
     * @param array $results Generated table results.
     *
     * @return array
     */
    protected function modifyResults(array $results)
    {
        $results_not_collected = [];
        $results_collected = [];
        $collected_by_options = $this->entry_loader->getCollectedByOptions();

        foreach ($results as $result) {
            $eid = $result['eid'];
            $status = 'In progress';

            // Loop through all column values.
            foreach ($result as $key => $value) {
                $entry_is_collected = $this->checkEntryCollected($eid);
                $entry_is_cancelled = $this->checkEntryCancelled($eid);

                // Modify value based on key.
                switch ($key) {
                    case 'sid':
                        $value = $this->site_loader->getSiteBySid($value)->getSiteCode();
                        break;

                    case 'firstname':
                        $firstname = ucwords(strtolower($value));
                        break;

                    case 'lastname':
                        $lastname = strtoupper($value);
                        $value = "$firstname $lastname";
                        break;

                    case 'ur_num':
                        if (null != $value) {
                            $value = $this->formatUrNumber($value);
                        }
                        break;

                    case 'require_medprof':
                        if ($value) {
                            $value = '<span class="glyphicon glyphicon-ok" style="color:#94c898;" aria-hidden="true"></span>';
                        } else {
                            $value = null;
                        }
                        break;

                    case 'comments':
                        $value = htmlspecialchars($value, ENT_QUOTES);
                        break;

                    case 'discharge_date':
                        // Formatted dates for checking matches.
                        $timestamp = $value;
                        $day = date('Ymd', $timestamp);
                        $display_day = date('Ymd', $this->display_date_from);

                        if ($day == $display_day) {
                            $value = 'Today';
                        } else {
                            $value = date('d-M', $timestamp);
                            if (!$entry_is_collected && !$entry_is_cancelled) {
                                $value = '<span style="color:#c55">' . $value . '</span>';
                            }
                        }
                        break;

                    case 'time_received':
                        // Formatted dates for checking matches.
                        $timestamp = $value;
                        $day = date('Ymd', $timestamp);
                        $display_day = date('Ymd', $this->display_date_from);

                        if ($day == $display_day) {
                            $value = date('g:ia', $timestamp);
                        } else {
                            $value = date('d-M', $timestamp);
                        }
                        break;

                    case 'time_completed':
                        $completed = false;
                        if ($this->checkEntryCompleted($eid)) {
                            if ($this->displayReadOnly()) {
                                $status = 'Ready for collection';
                            } else {
                                $completed = true;
                                $ts_completed = $value;
                                $value = date('d-M g:ia', $ts_completed);
                            }
                        } else {
                            $value = '
                            <form id="form_complete_' . $eid . '" method="post" action="">
                              <input name="eid" type="hidden" value="' . $eid . '">
                              <button type="submit" name="btn_complete" class="btn btn-default btn-xs" style="width:100px;">Complete</button>
                            </form>';
                        }
                        break;

                    case 'collected_by':
                        $details = $this->entry_loader->getEntryById($eid)->getCollectedByDetails();
                        if ($details && 'Nurse' == $value) {
                            $collected_by = "Nurse ($details)";
                        } elseif ($details && 'Other' == $value) {
                            $collected_by = $details;
                        } else {
                            $collected_by = $value;
                        }
                        break;

                    case 'time_collected':
                        if ($entry_is_collected) {
                            if ($this->displayReadOnly()) {
                                $status = "Collected by $collected_by";
                            }
                            $ts_collected = $value;
                            $value = $collected_by . ' (' . date('d-M g:ia', $ts_collected) . ')';
                        } elseif ($this->checkEntryCompleted($eid)) {
                            $value = '
                            <form id="form_collect_' . $eid . '" method="post" action="" class="form-inline">
                              <input name="eid" type="hidden" value="' . $eid . '">
                              <select id="collected_by_' . $eid . '" name="collected_by" class="form-control input-xs">';
                            foreach ($collected_by_options as $option) {
                                $value .= '<option value="' . $option . '">' . $option . '</option>';
                            }
                            $value .= '</select>
                              <input type="submit" name="btn_collect" class="btn btn-default btn-xs" value="Select">
                            </form>';
                        }
                        break;
                }

                // Assign result values to arrays (separate if collected).
                if (('firstname' != $key) && ('collected_by' != $key)) {
                    $new_result[$key] = $value;
                }
            }

            if ($this->displayReadOnly()) {
                unset($new_result['time_completed']);
                unset($new_result['time_collected']);
                unset($new_result['eid']);

                if ($entry_is_cancelled) {
                    $new_result['status'] = 'Discharge cancelled';
                } else {
                    $new_result['status'] = $status;
                }

                // Re-assign entry id to end of array.
                // Will be used for performing checks, but not appear in the table.
                $new_result['eid'] = $eid;
            }

            // Split the collected and not collected results.
            // Include cancelled entries with the collected results.
            if ($entry_is_collected || $entry_is_cancelled) {
                $results_collected[] = $new_result;
            } else {
                $results_not_collected[] = $new_result;
            }
        }

        // Assign all results into one array (collected at bottom).
        return array_merge($results_not_collected, $results_collected);
    }

    /**
     * Check if discharge entry has been completed.
     *
     * @param int $eid Entry id.
     *
     * @return bool
     */
    public function checkEntryCompleted($eid)
    {
        return $this->entry_loader->checkIsCompleted($eid);
    }

    /**
     * Check if discharge entry has been collected.
     *
     * @param int $eid Entry id.
     *
     * @return bool
     */
    public function checkEntryCollected($eid)
    {
        return $this->entry_loader->checkIsCollected($eid);
    }

    /**
     * Check if discharge entry has been cancelled.
     *
     * @param int $eid Entry id.
     *
     * @return bool
     */
    public function checkEntryCancelled($eid)
    {
        return $this->entry_loader->checkIsCancelled($eid);
    }

    /**
     * Get summary of discharge entries for each site.
     *
     * @param int $sid Site id.
     *
     * @return array
     */
    public function getSiteEntrySummary($sid)
    {
        $summary = [];
        $status_options = $this->entry_loader->getStatusOptions();

        // Loop through status options and get a row count of each.
        foreach ($status_options as $status) {
            $summary[$status] = $this->getEntryCountByStatus($sid, $status);
        }

        $summary['medprof'] = $this->getEntryCountMedprofRequired($sid);

        return $summary;
    }

    /**
     * Get count of entries for each site by status.
     *
     * @param int    $sid    Site id.
     * @param string $status Status of the discharge entry (incomplete,
     *                       completed, collected, cancelled).
     *
     * @return int
     */
    private function getEntryCountByStatus($sid, $status)
    {
        $query = "SELECT COUNT(eid) FROM dc_entries JOIN wards ON dc_entries.wid = wards.wid WHERE sid = :sid AND status = :status AND time_received >= :date_from AND time_received < :date_to";

        $stmt = $this->pdo->prepare($query);

        // Apply required parameters.
        $stmt->bindParam(':sid', $sid);
        $stmt->bindParam(':status', $status);
        $stmt->bindParam(':date_from', $this->display_date_from);
        $stmt->bindParam(':date_to', $this->display_date_to);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * Get count of entries for each site which required a medication profile.
     *
     * @param int $sid Site id.
     *
     * @return int
     */
    private function getEntryCountMedprofRequired($sid)
    {
        $query = "SELECT COUNT(eid) FROM dc_entries JOIN wards ON dc_entries.wid = wards.wid WHERE sid = :sid AND require_medprof = :require_medprof AND time_received >= :date_from AND time_received < :date_to";

        $stmt = $this->pdo->prepare($query);

        // Apply required parameters.
        $stmt->bindParam(':sid', $sid);
        $stmt->bindValue(':require_medprof', true);
        $stmt->bindParam(':date_from', $this->display_date_from);
        $stmt->bindParam(':date_to', $this->display_date_to);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * Format UR number as per settings.
     *
     * @param int $ur_num Patient's UR number.
     *
     * @return int
     */
    private function formatUrNumber($ur_num)
    {
        // Get character length.
        $ur_char_length_setting = $this->setting_loader->getSetting('ur_num', 'char_length');
        $ur_char_length = $ur_char_length_setting->getValue();

        // Check if leading zeroes are required.
        $ur_leading_zero_setting = $this->setting_loader->getSetting('ur_num', 'leading_zero');
        $has_leading_zero = $ur_leading_zero_setting->getValue();

        if ($has_leading_zero) {
            $ur_num = sprintf("%0{$ur_char_length}s", $ur_num);
        }

        return $ur_num;
    }
}
