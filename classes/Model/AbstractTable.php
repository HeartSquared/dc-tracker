<?php
/**
 * Abstract class for tables.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Model;

use Model\AbstractUserType;
use Service\CredentialLoader;
use Service\SiteLoader;

/**
 * Base for retrieving table data.
 */
abstract class AbstractTable
{
    protected $pdo;
    protected $user;
    protected $credential;
    protected $site_loader;
    protected $head_office;

    /**
     * Constructor.
     *
     * @param \PDO             $pdo  PDO connection.
     * @param AbstractUserType $user Logged in user.
     */
    public function __construct(\PDO $pdo, AbstractUserType $user)
    {
        $this->pdo = $pdo;
        $this->user = $user;
        $credential_loader = new CredentialLoader($pdo);
        $this->credential = $credential_loader->getCredentialById($user->getCid());
        $this->site_loader = new SiteLoader($this->pdo);
        $this->head_office = $this->site_loader->getHeadOffice();
    }

    /**
     * Gets table columns to display.
     *
     * @return array Database column name (for sorting) => Table header.
     */
    abstract public function getColumns();

    /**
     * Get database query.
     *
     * @param string $sort_option Column to order by.
     * @param string $sort_order  Sort direction (asc/desc).
     *
     * @return string Query statement.
     */
    abstract protected function getDataQuery($sort_option, $sort_order);

    /**
     * Apply parameters to query statement.
     *
     * @param string $stmt Query statement.
     *
     * @return void
     */
    abstract protected function applyParams($stmt);

    /**
     * Get database data to display in table.
     *
     * @param string $sort_option Column to order by.
     * @param string $sort_order  Sort direction (asc/desc).
     *
     * @return array
     */
    public function getData($sort_option, $sort_order)
    {
        $query = $this->getDataQuery($sort_option, $sort_order);
        $stmt = $this->pdo->prepare($query);
        $this->applyParams($stmt);
        $stmt->execute();
        $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        if (method_exists($this, 'modifyResults')) {
            $results = $this->modifyResults($results);
        }

        return $results;
    }

    /**
     * Get keys required formatted for query statement.
     *
     * @param array $columns Columns to be displayed in the table.
     *
     * @return string
     */
    protected function getKeys($columns)
    {
        $keys = implode(', ', array_keys($columns));

        return $keys;
    }

    /**
     * Checks if the table should be displayed as read only.
     *
     * @return bool
     */
    public function displayReadOnly()
    {
        return false;
    }
}
