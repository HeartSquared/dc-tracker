<?php
/**
 * User class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Model;

/**
 * User profile.
 */
class User extends AbstractUserType
{
    private $uid;
    private $firstname;
    private $lastname;
    private $role;
    private $email;

    /**
     * GETTERS.
     */

    /**
     * Get user id.
     *
     * @return int
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Get first name.
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstname;
    }

    /**
     * Get last name.
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastname;
    }

    /**
     * Get user's role.
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * SETTERS.
     */

    /**
     * Set user id.
     *
     * @param int $uid User id.
     *
     * @return void
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * Set first name.
     *
     * @param string $firstname First name.
     *
     * @return void
     */
    public function setFirstName($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * Set last name.
     *
     * @param string $lastname Last name.
     *
     * @return void
     */
    public function setLastName($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * Set user's role.
     *
     * @param string $role User role.
     *
     * @return void
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * Set email.
     *
     * @param string $email Email address.
     *
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
}
