<?php
/**
 * Ward class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Model;

/**
 * Hospital ward details.
 */
class Ward extends AbstractUserType
{
    private $wid;
    private $ward_code;
    private $ward_name;

    /**
     * GETTERS.
     */

    /**
     * Get ward id.
     *
     * @return int
     */
    public function getWid()
    {
        return $this->wid;
    }

    /**
     * Get ward code.
     *
     * @return string
     */
    public function getWardCode()
    {
        return $this->ward_code;
    }

    /**
     * Get ward name.
     *
     * @return string
     */
    public function getWardName()
    {
        return $this->ward_name;
    }

    /**
     * Get user role type.
     *
     * @return string
     */
    public function getRole()
    {
        return self::USERTYPE_WARD;
    }

    /**
     * SETTERS.
     */

    /**
     * Set ward id.
     *
     * @param int $wid Ward id.
     *
     * @return void
     */
    public function setWid($wid)
    {
        $this->wid = $wid;
    }

    /**
     * Set ward code.
     *
     * @param string $ward_code Ward code.
     *
     * @return void
     */
    public function setWardCode($ward_code)
    {
        $this->ward_code = $ward_code;
    }

    /**
     * Set ward name.
     *
     * @param string $ward_name Ward name.
     *
     * @return void
     */
    public function setWardName($ward_name)
    {
        $this->ward_name = $ward_name;
    }
}
