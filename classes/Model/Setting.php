<?php
/**
 * Setting class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Model;

/**
 * Admin setttings.
 */
class Setting
{
    private $field;
    private $name;
    private $value;

    /**
     *  Constructor.
     *
     * @param string $field Form field id.
     * @param string $name  Setting name.
     */
    public function __construct($field, $name)
    {
        $this->field = $field;
        $this->name = $name;
    }

    /**
     * GETTERS.
     */

    /**
     * Get form field id.
     *
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Get setting name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get setting value.
     *
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Generate id for setting for its own form field.
     *
     * @return string
     */
    public function getId()
    {
        return "{$this->getField()}__{$this->getName()}";
    }

    /**
     * SETTERS.
     */

    /**
     * Set setting value.
     *
     * @param mixed $value Selected value for the setting.
     *
     * @return void
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}
