<?php
/**
 * Site class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Model;

/**
 * Hospital site details.
 */
class Site extends AbstractUserType
{
    private $site_code;
    private $site_name;
    private $suburb;

    /**
     * GETTERS.
     */

    /**
     * Get site code.
     *
     * @return string
     */
    public function getSiteCode()
    {
        return strtoupper($this->site_code);
    }

    /**
     * Get hospital name.
     *
     * @return string
     */
    public function getSiteName()
    {
        return $this->site_name;
    }

    /**
     * Get suburb hospital is based in.
     *
     * @return string
     */
    public function getSuburb()
    {
        return $this->suburb;
    }

    /**
     * Get user role type.
     *
     * @return string
     */
    public function getRole()
    {
        return self::USERTYPE_SITE;
    }

    /**
     * SETTERS.
     */

    /**
     * Set site code.
     *
     * @param string $site_code Site code.
     *
     * @return void
     */
    public function setSiteCode($site_code)
    {
        $this->site_code = $site_code;
    }

    /**
     * Set hospital name.
     *
     * @param string $site_name Hospital name.
     *
     * @return void
     */
    public function setSiteName($site_name)
    {
        $this->site_name = $site_name;
    }

    /**
     * Set suburb hospital is based in.
     *
     * @param string $suburb Suburb of hospital.
     *
     * @return void
     */
    public function setSuburb($suburb)
    {
        $this->suburb = $suburb;
    }
}
