<?php
/**
 * Abstract class for users.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Model;

/**
 * Types of users.
 */
abstract class AbstractUserType
{
    const USERTYPE_USER = 'user';
    const USERTYPE_SITE = 'site';
    const USERTYPE_WARD = 'ward';
    const USERTYPE_INVALID = 'invalid';

    protected $cid;
    protected $sid;

    /**
     * Constructor.
     *
     * @param int $cid Credential id.
     */
    public function __construct($cid)
    {
        $this->cid = $cid;
    }

    /**
     * GETTERS.
     */

    /**
     * Get credential id.
     *
     * @return int
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * Get user's associated site id.
     *
     * @return int
     */
    public function getSid()
    {
        return $this->sid;
    }

    /**
     * Get user's role type.
     *
     * @return string
     */
    abstract public function getRole();

    /**
     * SETTERS.
     */

    /**
     * Set user's associated site id.
     *
     * @param int $sid Site id.
     *
     * @return void
     */
    public function setSid($sid)
    {
        $this->sid = $sid;
    }
}
