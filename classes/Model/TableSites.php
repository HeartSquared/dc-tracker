<?php
/**
 * TableSites class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Model;

/**
 * Data for table display of sites.
 */
class TableSites extends AbstractTable
{
    /**
     * {@inheritDoc}
     *
     * @return array Database column name (for sorting) => Table header.
     */
    public function getColumns()
    {
        $columns = [
            'site_name' => 'Hospital Name',
            'suburb' => 'Suburb',
            'site_code' => 'Site Code',
            'username' => 'Pharmacy Username',
        ];

        return $columns;
    }

    /**
     * {@inheritDoc}
     *
     * @param string $sort_option Column to order by.
     * @param string $sort_order  Sort direction (asc/desc).
     *
     * @return string Query statement.
     */
    public function getDataQuery($sort_option, $sort_order)
    {
        $columns = $this->getColumns();
        $keys = $this->getKeys($columns);

        // Assign Site Id to keys (hidden field for editing).
        $keys .= ', sid';
        $query = "SELECT $keys FROM sites JOIN credentials ON sites.cid = credentials.cid WHERE sites.sid != {$this->head_office->getSid()} ORDER BY LOWER($sort_option) $sort_order";

        return $query;
    }

    /**
     * {@inheritDoc}
     *
     * @param string $stmt Query statement.
     *
     * @return void
     */
    public function applyParams($stmt)
    {
        return;
    }
}
