<?php
/**
 * TableWards class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Model;

/**
 * Data for table display of wards.
 */
class TableWards extends AbstractTable
{
    /**
     * {@inheritDoc}
     *
     * @return array Database column name (for sorting) => Table header.
     */
    public function getColumns()
    {
        $columns = [];

        if ($this->head_office->getSid() == $this->user->getSid()) {
            $columns = [
                'site_name' => 'Site',
            ];
        }

        $columns = array_merge(
            $columns,
            [
                'ward_name' => 'Ward Name',
                'ward_code' => 'Ward Code',
                'username' => 'Username',
            ]
        );

        return $columns;
    }

    /**
     * {@inheritDoc}
     *
     * @param string $sort_option Column to order by.
     * @param string $sort_order  Sort direction (asc/desc).
     *
     * @return string Query statement.
     */
    public function getDataQuery($sort_option, $sort_order)
    {
        $columns = $this->getColumns();
        $keys = $this->getKeys($columns);

        // Assign Ward Id to keys (hidden field for editing).
        $keys .= ', wid';
        $query = "SELECT $keys FROM wards JOIN credentials ON wards.cid = credentials.cid LEFT JOIN sites ON sites.sid = wards.sid";
        if ($this->head_office->getSid() != $this->user->getSid()) {
            $query .= " WHERE wards.sid = :sid";
        }
        $query .= " ORDER BY LOWER($sort_option) $sort_order, LOWER(ward_name) ASC";

        return $query;
    }

    /**
     * {@inheritDoc}
     *
     * @param string $stmt Query statement.
     *
     * @return void
     */
    public function applyParams($stmt)
    {
        // Get site id of logged in user.
        if ($this->head_office->getSid() != $this->user->getSid()) {
            $sid = $this->user->getSid();
            $stmt->bindParam(':sid', $sid);
        }

        return;
    }
}
