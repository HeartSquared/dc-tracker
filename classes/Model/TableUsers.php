<?php
/**
 * TableUsers class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Model;

/**
 * Data for table display of users.
 */
class TableUsers extends AbstractTable
{
    /**
     * {@inheritDoc}
     *
     * @return array Database column name (for sorting) => Table header.
     */
    public function getColumns()
    {
        $columns = [];

        if ($this->head_office->getSid() == $this->user->getSid()) {
            $columns = [
                'site_name' => 'Site',
            ];
        }

        $columns = array_merge(
            $columns,
            [
                'firstname' => 'First Name',
                'lastname' => 'Last Name',
                'role' => 'User Role',
                'email' => 'Email / Username',
            ]
        );

        return $columns;
    }

    /**
     * {@inheritDoc}
     *
     * @param string $sort_option Column to order by.
     * @param string $sort_order  Sort direction (asc/desc).
     *
     * @return string Query statement.
     */
    public function getDataQuery($sort_option, $sort_order)
    {
        $columns = $this->getColumns();
        $keys = $this->getKeys($columns);

        // Assign User Id to keys (hidden field for editing).
        $keys .= ', users.uid';
        $query = "SELECT $keys FROM users JOIN credentials ON users.cid = credentials.cid LEFT JOIN sites ON sites.sid = users.sid";
        if ($this->head_office->getSid() != $this->user->getSid()) {
            $query .= " WHERE users.sid = :sid";
        }
        $query .= " ORDER BY LOWER($sort_option) $sort_order, LOWER(lastname) ASC";

        return $query;
    }

    /**
     * {@inheritDoc}
     *
     * @param string $stmt Query statement.
     *
     * @return void
     */
    public function applyParams($stmt)
    {
        // Get site id of logged in user.
        if ($this->head_office->getSid() != $this->user->getSid()) {
            $sid = $this->user->getSid();
            $stmt->bindParam(':sid', $sid);
        }

        return;
    }
}
