<?php
/**
 * EntryLoader class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Service;

use Model\Entry;

/**
 * Service to load discharge request entries.
 */
class EntryLoader
{
    // Completion/collection status options.
    const STATUS_INCOMPLETE = 'Incomplete';
    const STATUS_COMPLETED = 'Completed';
    const STATUS_COLLECTED = 'Collected';
    const STATUS_CANCELLED = 'Cancelled';

    // Collection entity options.
    const COLLECTED_BY_PATIENT = 'Patient';
    const COLLECTED_BY_PHARMACIST = 'Pharmacist';
    const COLLECTED_BY_NURSE = 'Nurse';
    const COLLECTED_BY_OTHER = 'Other';

    protected $pdo;

    /**
     * Constructor.
     *
     * @param \PDO $pdo PDO connection.
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Get list of completion/collection status options.
     *
     * @return array
     */
    public static function getStatusOptions()
    {
        return [
            'incomplete' => self::STATUS_INCOMPLETE,
            'completed' => self::STATUS_COMPLETED,
            'collected' => self::STATUS_COLLECTED,
            'cancelled' => self::STATUS_CANCELLED,
        ];
    }

    /**
     * Get list of collection entities.
     *
     * @return array
     */
    public static function getCollectedByOptions()
    {
        return [
            'patient' => self::COLLECTED_BY_PATIENT,
            'pharmacist' => self::COLLECTED_BY_PHARMACIST,
            'nurse' => self::COLLECTED_BY_NURSE,
            'other' => self::COLLECTED_BY_OTHER,
        ];
    }

    /**
     * Get discharge request entry by id.
     *
     * @param int $eid Entry id.
     *
     * @return Entry
     */
    public function getEntryById($eid)
    {
        return $this->getSingleEntry('eid', $eid);
    }

    /**
     * Get a single entry by requested filter key and value.
     *
     * @param string $key   Database table column name.
     * @param string $value Query parameter value.
     *
     * @return Entry
     */
    private function getSingleEntry($key, $value)
    {
        $entry_array = $this->querySingleEntry($key, $value);
        $entry = $this->getEntryFromArray($entry_array);
        return $entry;
    }

    /**
     * Create Entry object from array.
     *
     * @param array $entry_array Query result.
     *
     * @return Entry
     */
    private function getEntryFromArray(array $entry_array)
    {
        $entry = new Entry($entry_array['eid']);
        $entry->setWid($entry_array['wid']);
        $entry->setFirstName($entry_array['firstname']);
        $entry->setLastName($entry_array['lastname']);
        $entry->setUR($entry_array['ur_num']);
        $entry->setRequireMedprof($entry_array['require_medprof']);
        $entry->setComments($entry_array['comments']);
        $entry->setDischargeDate($entry_array['discharge_date']);
        $entry->setTimeReceived($entry_array['time_received']);
        $entry->setStatus($entry_array['status']);
        $entry->setTimeCompleted($entry_array['time_completed']);
        $entry->setTimeCollected($entry_array['time_collected']);
        $entry->setCollectedBy($entry_array['collected_by']);
        $entry->setCollectedByDetails($entry_array['collected_by_details']);
        return $entry;
    }

    /**
     * Query for single entry.
     *
     * @param string $filter_key Database table column name.
     * @param string $value      Query parameter value.
     *
     * @return array
     */
    private function querySingleEntry($filter_key, $value)
    {
        $query = "SELECT * FROM dc_entries WHERE $filter_key = :value";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':value', $value);
        $stmt->execute();
        $entry_array = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $entry_array;
    }

    /**
     * Check if discharge request entry has been completed.
     *
     * @param int $eid Entry id.
     *
     * @return bool
     */
    public function checkIsCompleted($eid)
    {
        $entry = $this->getEntryById($eid);
        $time_completed = $entry->getTimeCompleted();
        if (isset($time_completed) && !empty($time_completed)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if discharge request entry has been collected.
     *
     * @param int $eid Entry id.
     *
     * @return bool
     */
    public function checkIsCollected($eid)
    {
        $entry = $this->getEntryById($eid);
        $time_collected = $entry->getTimeCollected();
        if (isset($time_collected) && !empty($time_collected)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if discharge request entry has been cancelled.
     *
     * @param int $eid Entry id.
     *
     * @return bool
     */
    public function checkIsCancelled($eid)
    {
        $entry = $this->getEntryById($eid);
        $status = $entry->getStatus();
        if (self::STATUS_CANCELLED == $status) {
            return true;
        } else {
            return false;
        }
    }
}
