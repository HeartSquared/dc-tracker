<?php
/**
 * WardEditor class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Service;

/**
 * Service to edit hospital wards.
 */
class WardEditor extends WardLoader
{
    /**
     * Check if ward has discharge request entries associated.
     *
     * @param int $wid Ward id.
     *
     * @return bool
     */
    public function checkHasEntries($wid)
    {
        $query = "SELECT * FROM dc_entries WHERE wid = :wid";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':wid', $wid);
        $stmt->execute();
        $result = $stmt->fetch();

        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Create new ward.
     *
     * @param int    $cid       Credential id.
     * @param int    $sid       Site id.
     * @param string $ward_code Ward code.
     * @param string $ward_name Ward name.
     *
     * @return void
     */
    public function createWard($cid, $sid, $ward_code, $ward_name)
    {
        $query = "INSERT INTO wards (cid, sid, ward_code, ward_name) VALUES (:cid, :sid, :ward_code, :ward_name)";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':cid', $cid);
        $stmt->bindParam(':sid', $sid);
        $stmt->bindParam(':ward_code', $ward_code);
        $stmt->bindParam(':ward_name', $ward_name);
        $stmt->execute();
    }

    /**
     * Update ward details.
     *
     * @param int    $wid       Ward id.
     * @param int    $sid       Site id.
     * @param string $ward_code Ward code.
     * @param string $ward_name Ward name.
     *
     * @return void
     */
    public function updateWard($wid, $sid, $ward_code, $ward_name)
    {
        $query = "UPDATE wards SET sid = :sid, ward_code = :ward_code, ward_name = :ward_name WHERE wid = :wid";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':sid', $sid);
        $stmt->bindParam(':ward_code', $ward_code);
        $stmt->bindParam(':ward_name', $ward_name);
        $stmt->bindParam(':wid', $wid);
        $stmt->execute();
    }

    /**
     * Delete ward.
     *
     * @param int $wid Ward id.
     *
     * @return void
     */
    public function deleteWard($wid)
    {
        $query = "DELETE FROM wards WHERE wid = :wid";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':wid', $wid);
        $stmt->execute();
    }
}
