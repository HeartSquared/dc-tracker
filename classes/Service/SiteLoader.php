<?php
/**
 * SiteLoader class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Service;

use Model\Site;

/**
 * Service to load sites.
 */
class SiteLoader
{
    const SITECODE_HEADOFFICE = 'HEAD_OFFICE';

    protected $pdo;

    /**
     * Constructor.
     *
     * @param \PDO $pdo PDO connection.
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Get Head Office.
     *
     * @return Site
     */
    public function getHeadOffice()
    {
        return $this->getSiteBySiteCode(self::SITECODE_HEADOFFICE);
    }

    /**
     * Get all sites.
     *
     * @return Site[]
     */
    public function getAllHospitalSites()
    {
        $sites_array = $this->queryAllHospitalSites();
        $sites = [];

        foreach ($sites_array as $site_array) {
            $site = $this->getSiteFromArray($site_array);
            $sites[] = $site;
        }

        return $sites;
    }

    /**
     * Get a single site by credential id.
     *
     * @param int $cid Credential id.
     *
     * @return Site
     */
    public function getSiteByCid($cid)
    {
        return $this->getSingleSite('cid', $cid);
    }

    /**
     * Get a single site by site id.
     *
     * @param int $sid Site id.
     *
     * @return Site
     */
    public function getSiteBySid($sid)
    {
        return $this->getSingleSite('sid', $sid);
    }

    /**
     * Get a single site by site code.
     *
     * @param string $site_code Site code.
     *
     * @return Site
     */
    public function getSiteBySiteCode($site_code)
    {
        return $this->getSingleSite('site_code', $site_code);
    }

    /**
     * Get a single site by requested filter key and value.
     *
     * @param string $key   Database table column name.
     * @param string $value Query parameter value.
     *
     * @return Site
     */
    private function getSingleSite($key, $value)
    {
        $site_array = $this->querySingleSite($key, $value);
        $site = $this->getSiteFromArray($site_array);
        return $site;
    }

    /**
     * Create Site object from array.
     *
     * @param array $site_array Query result.
     *
     * @return Site
     */
    private function getSiteFromArray($site_array)
    {
        $site = new Site($site_array['cid']);
        $site->setSid($site_array['sid']);
        $site->setSiteCode($site_array['site_code']);
        $site->setSiteName($site_array['site_name']);
        $site->setSuburb($site_array['suburb']);
        return $site;
    }

    /**
     * Query for all sites.
     *
     * @return array
     */
    private function queryAllHospitalSites()
    {
        $head_office = $this->getSiteBySiteCode(self::SITECODE_HEADOFFICE);
        $query = "SELECT * FROM sites WHERE sid != :sid ORDER BY site_name";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindValue(':sid', $head_office->getSid());
        $stmt->execute();
        $sites_array = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $sites_array;
    }

    /**
     * Query for single site.
     *
     * @param string $filter_key Database table column name.
     * @param string $value      Query parameter value.
     *
     * @return array
     */
    private function querySingleSite($filter_key, $value)
    {
        $query = "SELECT * FROM sites WHERE $filter_key = :value";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':value', $value);
        $stmt->execute();
        $site_array = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $site_array;
    }
}
