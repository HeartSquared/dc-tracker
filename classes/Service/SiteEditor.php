<?php
/**
 * SiteEditor class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Service;

/**
 * Service to edit hospital sites.
 */
class SiteEditor extends SiteLoader
{
    /**
     * Create new site.
     *
     * @param int    $cid       Credential id.
     * @param string $site_code Site code.
     * @param string $site_name Hospital name.
     * @param string $suburb    Suburb of hospital.
     *
     * @return void
     */
    public function createSite($cid, $site_code, $site_name, $suburb)
    {
        $query = "INSERT INTO sites (cid, site_code, site_name, suburb) VALUES (:cid, :site_code, :site_name, :suburb)";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':cid', $cid);
        $stmt->bindParam(':site_code', $site_code);
        $stmt->bindParam(':site_name', $site_name);
        $stmt->bindParam(':suburb', $suburb);
        $stmt->execute();
    }

    /**
     * Update site details.
     *
     * @param int    $sid       Site id.
     * @param string $site_name Hospital name.
     * @param string $suburb    Suburb of hospital.
     * @param string $site_code Site code.
     *
     * @return void
     */
    public function updateSite($sid, $site_name, $suburb, $site_code)
    {
        $query = "UPDATE sites SET site_name = :site_name, suburb = :suburb, site_code = :site_code WHERE sid = :sid";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':site_name', $site_name);
        $stmt->bindParam(':suburb', $suburb);
        $stmt->bindParam(':site_code', $site_code);
        $stmt->bindParam(':sid', $sid);
        $stmt->execute();
    }

    /**
     * Delete site.
     *
     * @param int $sid Site id.
     *
     * @return void
     */
    public function deleteSite($sid)
    {
        $query = "DELETE FROM sites WHERE sid = :sid";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':sid', $sid);
        $stmt->execute();
    }

    /**
     * Check if site code has been used for another ward.
     *
     * @param string   $site_code Site code.
     * @param int|null $sid       Site id.
     *
     * @return bool
     */
    public function checkSiteCodeUsed($site_code, $sid = null)
    {
        $query = "SELECT site_code FROM sites WHERE site_code = :site_code";

        if ($sid) {
            $query .= " AND sid != :sid";
        }

        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':site_code', $site_code);

        if ($sid) {
            $stmt->bindParam(':sid', $sid);
        }

        $stmt->execute();
        $result = $stmt->fetch();

        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if site has wards registered.
     *
     * @param int $sid Site id.
     *
     * @return bool
     */
    public function checkHasWards($sid)
    {
        $query = "SELECT * FROM wards WHERE sid = :sid";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':sid', $sid);
        $stmt->execute();
        $result = $stmt->fetch();

        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if site has users registered.
     *
     * @param int $sid Site id.
     *
     * @return bool
     */
    public function checkHasUsers($sid)
    {
        $query = "SELECT * FROM users WHERE sid = :sid";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':sid', $sid);
        $stmt->execute();
        $result = $stmt->fetch();

        if ($result) {
            return true;
        } else {
            return false;
        }
    }
}
