<?php
/**
 * UserEditor class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Service;

/**
 * Service to edit user profiles.
 */
class UserEditor extends UserLoader
{
    /**
     * Create new user.
     *
     * @param int    $cid       Credential id.
     * @param string $firstname First name.
     * @param string $lastname  Last name.
     * @param string $role      User role.
     * @param string $email     Email address.
     * @param int    $sid       Site id.
     *
     * @return void
     */
    public function createUser($cid, $firstname, $lastname, $role, $email, $sid)
    {
        $query = "INSERT INTO users (cid, firstname, lastname, role, email, sid) VALUES (:cid, :firstname, :lastname, :role, :email, :sid)";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':cid', $cid);
        $stmt->bindParam(':firstname', $firstname);
        $stmt->bindParam(':lastname', $lastname);
        $stmt->bindParam(':role', $role);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':sid', $sid);
        $stmt->execute();
    }

    /**
     * Update user details.
     *
     * @param int    $uid       User id.
     * @param string $firstname First name.
     * @param string $lastname  Last name.
     * @param string $role      User role.
     * @param string $email     Email address.
     * @param int    $sid       Site id.
     *
     * @return void
     */
    public function updateUser($uid, $firstname, $lastname, $role, $email, $sid)
    {
        $query = "UPDATE users SET firstname = :firstname, lastname = :lastname, role = :role, email = :email, sid = :sid WHERE uid = :uid";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':firstname', $firstname);
        $stmt->bindParam(':lastname', $lastname);
        $stmt->bindParam(':role', $role);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':sid', $sid);
        $stmt->bindParam(':uid', $uid);
        $stmt->execute();
    }

    /**
     * Update user profile.
     *
     * @param int    $uid       User id.
     * @param string $firstname First name.
     * @param string $lastname  Last name.
     * @param string $email     Email address.
     *
     * @return void
     */
    public function updateProfile($uid, $firstname, $lastname, $email)
    {
        $query = "UPDATE users SET firstname = :firstname, lastname = :lastname, email = :email WHERE uid = :uid";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':firstname', $firstname);
        $stmt->bindParam(':lastname', $lastname);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':uid', $uid);
        $stmt->execute();
    }

    /**
     * Delete user.
     *
     * @param int $uid User id.
     *
     * @return void
     */
    public function deleteUser($uid)
    {
        $query = "DELETE FROM users WHERE uid = :uid";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':uid', $uid);
        $stmt->execute();
    }
}
