<?php
/**
 * Installer class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Service;

use Model\AbstractUserType;

/**
 * Service to set up the database and configuration required to run.
 */
abstract class AbstractInstaller
{
    protected $pdo;

    /**
     * Constructor.
     *
     * @param \PDO $pdo PDO connection.
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Create database.
     *
     * @return void
     */
    public function createDatabase()
    {
        $this->createCredentialsTable();
        $this->createSitesTable();
        $this->createUsersTable();
        $this->createWardsTable();
        $this->createDischargeEntriesTable();
        $this->createSettingsTable();
    }

    /**
     * Create Head Office site.
     *
     * @return void
     */
    public function createHeadOffice()
    {
        $site_editor = new SiteEditor($this->pdo);
        $site_code = SiteLoader::SITECODE_HEADOFFICE;
        $site_name = 'Head Office';

        // Create Head Office site.
        // Credentials must be created to satisfy foreign key.
        // Account will not be allowed to log in.
        $credential_editor = new CredentialEditor($this->pdo);
        $username = $site_code;
        $user_type = AbstractUserType::USERTYPE_INVALID;
        $password = $site_code;
        $credential_editor->createCredential($username, $user_type, $password);

        $cid = $credential_editor->getCredential($username)->getCid();
        $suburb = $site_name;
        $site_editor->createSite($cid, $site_code, $site_name, $suburb);
    }

    /**
     * Create user 1 - System Admin based at Head Office.
     *
     * @param string $email     Email address.
     * @param string $password  Password.
     * @param string $firstname First name.
     * @param string $lastname  Last name.
     *
     * @return void
     */
    public function createUser1($email, $password, $firstname, $lastname)
    {
        // Create System Admin account.
        // Create login credentials.
        $credential_editor = new CredentialEditor($this->pdo);
        $username = $email;
        $user_type = AbstractUserType::USERTYPE_USER;
        $require_pass_reset = 0;
        $credential_editor->createCredential($username, $user_type, $password, $require_pass_reset);

        // User profile.
        $user_editor = new UserEditor($this->pdo);
        $site_loader = new SiteLoader($this->pdo);
        $cid = $credential_editor->getCredential($username)->getCid();
        $role = UserLoader::ROLE_SYSTEM_ADMIN;
        $sid = $site_loader->getHeadOffice()->getSid();
        $user_editor->createUser($cid, $firstname, $lastname, $role, $email, $sid);
    }

    /**
     * Create CREDENTIALS table.
     * Collection of all user logins (users and wards).
     *
     * * cid = Credential id (match in USERS, SITES and WARDS tables).
     * * username = Username for login (email for Users, site_ward for Wards,
     *   [site_code]_pharmacy for Pharmacy Casuals).
     * * user_type = User type (user, site, ward).
     * * password = Hashed password.
     * * require_pass_reset = User required to reset their password (0 or 1).
     *
     * @return void
     */
    abstract protected function createCredentialsTable();

    /**
     * Create SITES table.
     * Collection of hospital sites.
     *
     * * sid = Site id.
     * * cid = Credential id ([site_code]_pharmacy format).
     * * site_code = Short code for the site (used for ward username).
     * * site_name = Site name.
     * * suburb = Suburb of site.
     *
     * @return void
     */
    abstract protected function createSitesTable();

    /**
     * Create USERS table.
     * Collection of pharmacy staff user profiles.
     *
     * * uid = User id.
     * * cid = Credential id.
     * * sid = Site id.
     * * firstname = User's first name.
     * * lastname = User's last name.
     * * role = User role (Admin, Manager, Pharmacist, Technician).
     * * email = User's email address (will be their username).
     *
     * @return void
     */
    abstract protected function createUsersTable();

    /**
     * Create WARDS table.
     * Collection of wards within a hospital site.
     *
     * * wid = Ward id.
     * * cid = Credential id ([site_code]_[ward_code] format).
     * * sid = Site id of site the ward belongs to.
     * * ward_code = Short code for the ward (used for ward username).
     * * ward_name = Full name of the ward.
     *
     * @return void
     */
    abstract protected function createWardsTable();

    /**
     * Create DC_ENTRIES table.
     * Collection of discharge entries.
     *
     * * eid = Entry id.
     * * wid = Ward id.
     * * firstname = Patient's first name.
     * * lastname = Patient's last name.
     * * ur_num = Patient's UR number.
     * * require_medprof = Medication profile required.
     * * comments = Comments on the discharge.
     * * discharge_date = Date patient will discharge from hospital.
     * * time_received = Time discharge charts/scripts are received.
     * * status = Discharge status (incomplete, complete, collected, cancelled).
     * * time_completed = Time discharge has been completed (ready for pick up).
     * * time_collected = Time discharge is collected from pharmacy.
     * * collected_by = Person who collected the discharge.
     *   (Patient, Ward Pharmacist, Nurse, Other (require comment)).
     * * collected_by_details = Details of Role/Name (for Nurse and Other).
     *
     * @return void
     */
    abstract protected function createDischargeEntriesTable();

    /**
     * Create SETTINGS table.
     * Field validation settings.
     *
     * * field = The field the setting corresponds to.
     * * name = The name of the setting to configure.
     * * value = The value of the setting to configure.
     *
     * @return void
     */
    abstract protected function createSettingsTable();
}
