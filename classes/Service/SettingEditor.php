<?php
/**
 * SettingEditor class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Service;

/**
 * Service to edit admin settings.
 */
class SettingEditor extends SettingLoader
{
    /**
     * Create setting.
     *
     * @param string $field Form field id.
     * @param string $name  Setting name.
     * @param mixed  $value Selected value for setting.
     *
     * @return void
     */
    public function createSetting($field, $name, $value)
    {

        $insert_setting = "INSERT INTO field_settings (field, name, value) VALUES (:field, :name, :value)";
        $stmt = $this->pdo->prepare($insert_setting);
        $stmt->bindParam(':field', $field);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':value', $value);
        $stmt->execute();
        return;
    }

    /**
     * Update setting by setting id.
     *
     * @param int   $id    Setting id.
     * @param mixed $value Selected value for setting.
     *
     * @return void
     */
    public function updateSettingById($id, $value)
    {
        $id_segments = explode('__', $id);
        $field = $id_segments[0];
        $name = $id_segments[1];
        $this->updateSetting($field, $name, $value);
    }

    /**
     * Update setting.
     *
     * @param string $field Form field id.
     * @param string $name  Setting name.
     * @param mixed  $value Selected value for setting.
     *
     * @return void
     */
    public function updateSetting($field, $name, $value)
    {
        $query = "UPDATE field_settings SET value = :value WHERE field = :field AND name = :name";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':value', $value);
        $stmt->bindParam(':field', $field);
        $stmt->bindParam(':name', $name);
        $stmt->execute();
        return;
    }
}
