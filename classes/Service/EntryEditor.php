<?php
/**
 * EntryEditor class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Service;

/**
 * Service to edit discharge request entries.
 */
class EntryEditor extends EntryLoader
{
    /**
     * Create new discharge request entry.
     *
     * @param int         $wid             Ward id.
     * @param string      $firstname       Patient's first name.
     * @param string      $lastname        Patient's last name.
     * @param int         $ur_num          Patient's UR number.
     * @param bool        $require_medprof Medication profile is required.
     * @param string|null $comments        Comments.
     * @param string      $discharge_date  Discharge date timestamp (midnight).
     * @param string      $time_received   Time received timestamp.
     *
     * @return void
     */
    public function createEntry($wid, $firstname, $lastname, $ur_num, $require_medprof, $comments, $discharge_date, $time_received)
    {
        $require_medprof = !empty($require_medprof) ? 1 : 0;

        $query = "INSERT INTO dc_entries (wid, firstname, lastname, ur_num, require_medprof, comments, discharge_date, time_received, status) VALUES (:wid, :firstname, :lastname, :ur_num, :require_medprof, :comments, :discharge_date, :time_received, :status)";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':wid', $wid);
        $stmt->bindParam(':firstname', $firstname);
        $stmt->bindParam(':lastname', $lastname);
        $stmt->bindParam(':ur_num', $ur_num);
        $stmt->bindParam(':require_medprof', $require_medprof);
        $stmt->bindParam(':comments', $comments);
        $stmt->bindParam(':discharge_date', $discharge_date);
        $stmt->bindParam(':time_received', $time_received);
        $stmt->bindValue(':status', self::STATUS_INCOMPLETE);
        $stmt->execute();
    }

    /**
     * Update discharge request entry.
     *
     * @param int         $eid                  Entry id.
     * @param int         $wid                  Ward id.
     * @param string      $firstname            Patient's first name.
     * @param string      $lastname             Patient's last name.
     * @param int         $ur_num               Patient's UR number.
     * @param bool        $require_medprof      Medication profile is required.
     * @param string|null $comments             Comments.
     * @param string      $discharge_date       Discharge date timestamp (midnight).
     * @param string      $status               Completion/collection status.
     * @param string|null $time_completed       Time completed timestamp.
     * @param string|null $time_collected       Time collected timestamp.
     * @param string|null $collected_by         Entity who collected the discharge.
     * @param string|null $collected_by_details Details of who collected discharge.
     *
     * @return void
     */
    public function updateEntry($eid, $wid, $firstname, $lastname, $ur_num, $require_medprof, $comments, $discharge_date, $status, $time_completed, $time_collected, $collected_by, $collected_by_details)
    {
        $require_medprof = !empty($require_medprof) ? 1 : 0;

        $query = "UPDATE dc_entries SET wid = :wid, firstname = :firstname, lastname = :lastname, ur_num = :ur_num, require_medprof = :require_medprof, comments = :comments, discharge_date = :discharge_date, status = :status, time_completed = :time_completed, time_collected = :time_collected, collected_by = :collected_by, collected_by_details = :collected_by_details WHERE eid = :eid";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':eid', $eid);
        $stmt->bindParam(':wid', $wid);
        $stmt->bindParam(':firstname', $firstname);
        $stmt->bindParam(':lastname', $lastname);
        $stmt->bindParam(':ur_num', $ur_num);
        $stmt->bindParam(':require_medprof', $require_medprof);
        $stmt->bindParam(':comments', $comments);
        $stmt->bindParam(':discharge_date', $discharge_date);
        $stmt->bindParam(':status', $status);
        $stmt->bindParam(':time_completed', $time_completed);
        $stmt->bindParam(':time_collected', $time_collected);
        $stmt->bindParam(':collected_by', $collected_by);
        $stmt->bindParam(':collected_by_details', $collected_by_details);
        $stmt->execute();
    }

    /**
     * Update status of discharge request entry to complete.
     *
     * @param int $eid Entry id.
     *
     * @return void
     */
    public function completeEntry($eid)
    {
        $ts_now = strtotime('now');
        $query = "UPDATE dc_entries SET status = :status, time_completed = :ts WHERE eid = :eid";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':eid', $eid);
        $stmt->bindValue(':status', self::STATUS_COMPLETED);
        $stmt->bindParam(':ts', $ts_now);
        $stmt->execute();
    }

    /**
     * Update status of discharge request entry to collected.
     *
     * @param int         $eid                  Entry id.
     * @param string      $collected_by         Entity who collected the discharge.
     * @param string|null $collected_by_details Details of who collected discharge.
     *
     * @return void
     */
    public function collectEntry($eid, $collected_by, $collected_by_details = null)
    {
        $ts_now = strtotime('now');
        $query = "UPDATE dc_entries SET status = :status, time_collected = :ts, collected_by = :collected_by, collected_by_details = :collected_by_details WHERE eid = :eid";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':eid', $eid);
        $stmt->bindValue(':status', self::STATUS_COLLECTED);
        $stmt->bindParam(':ts', $ts_now);
        $stmt->bindParam(':collected_by', $collected_by);
        $stmt->bindParam(':collected_by_details', $collected_by_details);
        $stmt->execute();
    }

    /**
     * Delete discharge request entry.
     *
     * @param int $eid Entry id.
     *
     * @return void
     */
    public function deleteEntry($eid)
    {
        $query = "DELETE FROM dc_entries WHERE eid = :eid";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':eid', $eid);
        $stmt->execute();
    }
}
