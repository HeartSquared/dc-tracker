<?php
/**
 * Installer class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Service;

/**
 * Service to set up the database and configuration required to run.
 */
class InstallerMySQL extends AbstractInstaller
{
    /**
     * {@inheritDoc}
     *
     * @return void
     */
    protected function createCredentialsTable()
    {
        $this->pdo->exec(
            "CREATE TABLE IF NOT EXISTS credentials (
            cid INT AUTO_INCREMENT PRIMARY KEY,
            username VARCHAR(255) NOT NULL UNIQUE,
            password VARCHAR(255) NOT NULL,
            user_type VARCHAR(8) NOT NULL,
            require_pass_reset BOOLEAN NOT NULL)"
        );
    }

    /**
     * {@inheritDoc}
     *
     * @return void
     */
    protected function createSitesTable()
    {
        $this->pdo->exec(
            "CREATE TABLE IF NOT EXISTS sites (
            sid INT AUTO_INCREMENT PRIMARY KEY,
            cid INT NOT NULL UNIQUE,
            site_code VARCHAR(16) NOT NULL UNIQUE,
            site_name VARCHAR(255) NOT NULL,
            suburb VARCHAR(255) NOT NULL,
            FOREIGN KEY (cid) REFERENCES credentials(cid))"
        );
    }

    /**
     * {@inheritDoc}
     *
     * @return void
     */
    protected function createUsersTable()
    {
        $this->pdo->exec(
            "CREATE TABLE IF NOT EXISTS users (
            uid INT AUTO_INCREMENT PRIMARY KEY,
            cid INT NOT NULL UNIQUE,
            sid INT NOT NULL,
            firstname VARCHAR(255) NOT NULL,
            lastname VARCHAR(255) NOT NULL,
            role VARCHAR(16) NOT NULL,
            email VARCHAR(255) NOT NULL UNIQUE,
            FOREIGN KEY (cid) REFERENCES credentials(cid),
            FOREIGN KEY (sid) REFERENCES sites(sid))"
        );
    }

    /**
     * {@inheritDoc}
     *
     * @return void
     */
    protected function createWardsTable()
    {
        $this->pdo->exec(
            "CREATE TABLE IF NOT EXISTS wards (
            wid INT AUTO_INCREMENT PRIMARY KEY,
            cid INT NOT NULL UNIQUE,
            sid INT NOT NULL,
            ward_code VARCHAR(6) NOT NULL,
            ward_name VARCHAR(255) NOT NULL,
            FOREIGN KEY (cid) REFERENCES credentials(cid),
            FOREIGN KEY (sid) REFERENCES sites(sid))"
        );
    }

    /**
     * {@inheritDoc}
     *
     * @return void
     */
    protected function createDischargeEntriesTable()
    {
        $this->pdo->exec(
            "CREATE TABLE IF NOT EXISTS dc_entries (
            eid INT AUTO_INCREMENT PRIMARY KEY,
            wid INT NOT NULL,
            firstname VARCHAR(255) NOT NULL,
            lastname VARCHAR(255) NOT NULL,
            ur_num VARCHAR(16),
            require_medprof BOOLEAN NOT NULL,
            comments VARCHAR(255),
            discharge_date INT NOT NULL,
            time_received INT NOT NULL,
            status VARCHAR(16) NOT NULL,
            time_completed INT,
            time_collected INT,
            collected_by VARCHAR(16),
            collected_by_details VARCHAR(255),
            FOREIGN KEY (wid) REFERENCES wards(wid))"
        );
    }

    /**
     * {@inheritDoc}
     *
     * @return void
     */
    protected function createSettingsTable()
    {
        $this->pdo->exec(
            "CREATE TABLE IF NOT EXISTS field_settings (
            field VARCHAR(255) NOT NULL,
            name VARCHAR(255) NOT NULL,
            value VARCHAR(255),
            PRIMARY KEY (field, name))"
        );
    }
}
