<?php
/**
 * SettingLoader class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Service;

use Model\Setting;

/**
 * Service to load admin settings.
 */
class SettingLoader
{
    protected $pdo;

    /**
     * Constructor.
     *
     * @param \PDO $pdo PDO connection.
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Get setting by requested filter key and value.
     *
     * @param string $field Form field id.
     * @param string $name  Setting name.
     *
     * @return Setting
     */
    public function getSetting($field, $name)
    {
        $setting = null;

        $setting_array = $this->querySingleSetting($field, $name);
        if (!empty($setting_array)) {
            $setting = $this->getSettingFromArray($setting_array);
        }

        return $setting;
    }

    /**
     * Create Setting object from array.
     *
     * @param array $setting_array Query result.
     *
     * @return Setting
     */
    private function getSettingFromArray(array $setting_array)
    {
        $setting = new Setting($setting_array['field'], $setting_array['name']);
        $setting->setValue($setting_array['value']);
        return $setting;
    }

    /**
     * Query for single setting.
     *
     * @param string $field Form field id.
     * @param string $name  Setting name.
     *
     * @return array
     */
    private function querySingleSetting($field, $name)
    {
        $query = "SELECT * FROM field_settings WHERE field = :field AND name = :name";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':field', $field);
        $stmt->bindParam(':name', $name);
        $stmt->execute();
        $setting_array = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $setting_array;
    }
}
