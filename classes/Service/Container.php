<?php
/**
 * Container class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Service;

use Model\TableUsers;
use Model\TableSites;
use Model\TableWards;
use Model\TableEntries;
use Model\AbstractUserType;

/**
 * Container for loading and editing model and service classes.
 */
class Container
{
    private $db_config;
    private $pdo;
    private $installer;
    private $setting_loader;
    private $setting_editor;
    private $credential_loader;
    private $credential_editor;
    private $user_loader;
    private $user_editor;
    private $site_loader;
    private $site_editor;
    private $ward_loader;
    private $ward_editor;
    private $entry_loader;
    private $entry_editor;
    private $table_manager;
    private $table_users;
    private $table_sites;
    private $table_wards;
    private $table_entries;

    /**
     * Constructor.
     *
     * @param array $db_config Database connection configuration.
     */
    public function __construct(array $db_config)
    {
        $this->db_config = $db_config;
    }

    /**
     * Get PDO connection.
     *
     * @return PDO
     */
    public function getPDO()
    {
        if ($this->pdo === null) {
            $this->pdo = new \PDO(
                $this->db_config['dsn'],
                $this->db_config['user'],
                $this->db_config['pass']
            );
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        }
        return $this->pdo;
    }

    /**
     * Get Installer service.
     *
     * @param string $db_type Database type.
     *
     * @return Installer
     */
    public function getInstaller($db_type)
    {
        if ($this->installer === null) {
            switch ($db_type) {
                case 'sqlite':
                    $this->installer = new InstallerSQLite($this->getPDO());
                    break;

                case 'mysql':
                    $this->installer = new InstallerMySQL($this->getPDO());
                    break;

                case 'pgsql':
                    $this->installer = new InstallerPostgreSQL($this->getPDO());
                    break;
            }
        }
        return $this->installer;
    }

    /**
     * Get SettingLoader service.
     *
     * @return SettingLoader
     */
    public function getSettingLoader()
    {
        if ($this->setting_loader === null) {
            $this->setting_loader = new SettingLoader($this->getPDO());
        }
        return $this->setting_loader;
    }

    /**
     * Get SettingEditor service.
     *
     * @return SettingEditor
     */
    public function getSettingEditor()
    {
        if ($this->setting_editor === null) {
            $this->setting_editor = new SettingEditor($this->getPDO());
        }
        return $this->setting_editor;
    }

    /**
     * Get CredentialLoader service.
     *
     * @return CredentialLoader
     */
    public function getCredentialLoader()
    {
        if ($this->credential_loader === null) {
            $this->credential_loader = new CredentialLoader($this->getPDO());
        }
        return $this->credential_loader;
    }

    /**
     * Get CredentialEditor service.
     *
     * @return CredentialEditor
     */
    public function getCredentialEditor()
    {
        if ($this->credential_editor === null) {
            $this->credential_editor = new CredentialEditor($this->getPDO());
        }
        return $this->credential_editor;
    }

    /**
     * Get UserLoader service.
     *
     * @return UserLoader
     */
    public function getUserLoader()
    {
        if ($this->user_loader === null) {
            $this->user_loader = new UserLoader($this->getPDO());
        }
        return $this->user_loader;
    }

    /**
     * Get UserEditor service.
     *
     * @return UserEditor
     */
    public function getUserEditor()
    {
        if ($this->user_editor === null) {
            $this->user_editor = new UserEditor($this->getPDO());
        }
        return $this->user_editor;
    }

    /**
     * Get SiteLoader service.
     *
     * @return SiteLoader
     */
    public function getSiteLoader()
    {
        if ($this->site_loader === null) {
            $this->site_loader = new SiteLoader($this->getPDO());
        }
        return $this->site_loader;
    }

    /**
     * Get SiteEditor service.
     *
     * @return SiteEditor
     */
    public function getSiteEditor()
    {
        if ($this->site_editor === null) {
            $this->site_editor = new SiteEditor($this->getPDO());
        }
        return $this->site_editor;
    }

    /**
     * Get WardLoader service.
     *
     * @return WardLoader
     */
    public function getWardLoader()
    {
        if ($this->ward_loader === null) {
            $this->ward_loader = new WardLoader($this->getPDO());
        }
        return $this->ward_loader;
    }

    /**
     * Get WardEditor service.
     *
     * @return WardEditor
     */
    public function getWardEditor()
    {
        if ($this->ward_editor === null) {
            $this->ward_editor = new WardEditor($this->getPDO());
        }
        return $this->ward_editor;
    }

    /**
     * Get EntryLoader service.
     *
     * @return EntryLoader
     */
    public function getEntryLoader()
    {
        if ($this->entry_loader === null) {
            $this->entry_loader = new EntryLoader($this->getPDO());
        }
        return $this->entry_loader;
    }

    /**
     * Get EntryEditor service.
     *
     * @return EntryEditor
     */
    public function getEntryEditor()
    {
        if ($this->entry_editor === null) {
            $this->entry_editor = new EntryEditor($this->getPDO());
        }
        return $this->entry_editor;
    }

    /**
     * Get TableManager service.
     *
     * @return TableManager
     */
    public function getTableManager()
    {
        if ($this->table_manager === null) {
            $this->table_manager = new TableManager($this->getPDO());
        }
        return $this->table_manager;
    }

    /**
     * Get table of registered users.
     *
     * @param AbstractUserType $user Logged in user.
     *
     * @return TableUsers
     */
    public function getTableUsers(AbstractUserType $user)
    {
        if ($this->table_users === null) {
            $this->table_users = new TableUsers($this->getPDO(), $user);
        }
        return $this->table_users;
    }

    /**
     * Get table of registered sites.
     *
     * @param AbstractUserType $user Logged in user.
     *
     * @return TableSites
     */
    public function getTableSites(AbstractUserType $user)
    {
        if ($this->table_sites === null) {
            $this->table_sites = new TableSites($this->getPDO(), $user);
        }
        return $this->table_sites;
    }

    /**
     * Get table of registered wards.
     *
     * @param AbstractUserType $user Logged in user.
     *
     * @return TableWards
     */
    public function getTableWards(AbstractUserType $user)
    {
        if ($this->table_wards === null) {
            $this->table_wards = new TableWards($this->getPDO(), $user);
        }
        return $this->table_wards;
    }

    /**
     * Get table of entered discharge requests.
     *
     * @param AbstractUserType $user         Logged in user.
     * @param DateTime         $display_date Date to filter results.
     *
     * @return TableEntries
     */
    public function getTableEntries(AbstractUserType $user, \DateTime $display_date)
    {
        if ($this->table_entries === null) {
            $this->table_entries = new TableEntries($this->getPDO(), $user, $display_date);
        }
        return $this->table_entries;
    }
}
