<?php
/**
 * CredentialLoader class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Service;

use Model\Credential;

/**
 * Service to load credentials.
 */
class CredentialLoader
{
    protected $pdo;

    /**
     * Constructor.
     *
     * @param \PDO $pdo PDO connection.
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Get login credentials by username.
     *
     * @param string $username Username.
     *
     * @return Credential
     */
    public function getCredential($username)
    {
        return $this->getSingleCredential('username', $username);
    }

    /**
     * Get login credentials by id.
     *
     * @param int $cid Credential id.
     *
     * @return Credential
     */
    public function getCredentialById($cid)
    {
        return $this->getSingleCredential('cid', $cid);
    }

    /**
     * Get a single credential by requested filter key and value.
     *
     * @param string $key   Database table column name.
     * @param string $value Query parameter value.
     *
     * @return Credential
     */
    private function getSingleCredential($key, $value)
    {
        $credential = null;

        $cred_array = $this->querySingleCredential($key, $value);
        if (!empty($cred_array)) {
            $credential = $this->getCredentialFromArray($cred_array);
        }

        return $credential;
    }

    /**
     * Create Credential object from array.
     *
     * @param array $cred_array Query result.
     *
     * @return Credential
     */
    private function getCredentialFromArray(array $cred_array)
    {
        $credential = new Credential($cred_array['username']);
        $credential->setCid($cred_array['cid']);
        $credential->setPassword($cred_array['password']);
        $credential->setUserType($cred_array['user_type']);
        $credential->setRequirePassReset($cred_array['require_pass_reset']);
        return $credential;
    }

    /**
     * Query for single credential.
     *
     * @param string $filter_key Database table column name.
     * @param string $value      Query parameter value.
     *
     * @return array
     */
    private function querySingleCredential($filter_key, $value)
    {
        $query = "SELECT * FROM credentials WHERE $filter_key = :value";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':value', $value);
        $stmt->execute();
        $cred_array = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $cred_array;
    }
}
