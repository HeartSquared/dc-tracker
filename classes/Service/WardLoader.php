<?php
/**
 * WardLoader class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Service;

use Model\Ward;

/**
 * Service to load wards.
 */
class WardLoader
{
    protected $pdo;

    /**
     * Constructor.
     *
     * @param \PDO $pdo PDO connection.
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Get all wards.
     *
     * @return Ward[]
     */
    public function getAllWards()
    {
        $wards_array = $this->queryAllWards();
        $wards = [];

        foreach ($wards_array as $ward_array) {
            $ward = $this->getWardFromArray($ward_array);
            $wards[] = $ward;
        }

        return $wards;
    }

    /**
     * Get all wards for a particular site.
     *
     * @param int $sid Site id.
     *
     * @return Ward[].
     */
    public function getAllWardsBySid($sid)
    {
        $wards_array = $this->queryAllWards('sid', $sid);
        $ward = [];
        foreach ($wards_array as $ward_array) {
            $ward = $this->getWardFromArray($ward_array);
            $wards[] = $ward;
        }
        return $wards;
    }

    /**
     * Get a single ward by credential id.
     *
     * @param int $cid Credential id.
     *
     * @return Ward
     */
    public function getWardByCid($cid)
    {
        return $this->getSingleWard('cid', $cid);
    }

    /**
     * Get a single site by ward id.
     *
     * @param int $wid Ward id.
     *
     * @return Ward
     */
    public function getWardByWid($wid)
    {
        return $this->getSingleWard('wid', $wid);
    }

    /**
     * Get a single ward by requested filter key and value.
     *
     * @param string $value Query parameter value.
     * @param string $key   Database table column name.
     *
     * @return Ward
     */
    private function getSingleWard($key, $value)
    {
        $ward_array = $this->querySingleWard($key, $value);
        $ward = $this->getWardFromArray($ward_array);
        return $ward;
    }

    /**
     * Create Ward object from array.
     *
     * @param array $ward_array Query result.
     *
     * @return Ward
     */
    private function getWardFromArray(array $ward_array)
    {
        $ward = new Ward($ward_array['cid']);
        $ward->setWid($ward_array['wid']);
        $ward->setSid($ward_array['sid']);
        $ward->setWardCode($ward_array['ward_code']);
        $ward->setWardName($ward_array['ward_name']);
        return $ward;
    }

    /**
     * Query for all wards; optional filter.
     *
     * @param string|null $filter_key Database table column name.
     * @param string|null $value      Query parameter value.
     *
     * @return array
     */
    private function queryAllWards($filter_key = null, $value = null)
    {
        if ($filter_key && $value) {
            $query = "SELECT * FROM wards WHERE $filter_key = :value ORDER BY ward_name ASC";
        } else {
            $query = "SELECT * FROM wards LEFT JOIN sites ON wards.sid = sites.sid ORDER BY site_name ASC, ward_name ASC";
        }

        $stmt = $this->pdo->prepare($query);

        if ($filter_key && $value) {
            $stmt->bindParam(':value', $value);
        }

        $stmt->execute();
        $wards_array = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $wards_array;
    }

    /**
     * Query for single ward.
     *
     * @param string $filter_key Database table column name.
     * @param string $value      Query parameter value.
     *
     * @return array
     */
    private function querySingleWard($filter_key, $value)
    {
        $query = "SELECT * FROM wards WHERE $filter_key = :value";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':value', $value);
        $stmt->execute();
        $ward_array = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $ward_array;
    }
}
