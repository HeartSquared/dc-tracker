<?php
/**
 * Installer class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Service;

/**
 * Service to set up the database and configuration required to run.
 */
class InstallerPostgreSQL extends AbstractInstaller
{
    /**
     * {@inheritDoc}
     *
     * @return void
     */
    protected function createCredentialsTable()
    {
        $this->pdo->exec(
            "CREATE TABLE IF NOT EXISTS credentials (
            cid SERIAL PRIMARY KEY,
            username TEXT NOT NULL UNIQUE,
            password TEXT NOT NULL,
            user_type TEXT NOT NULL,
            require_pass_reset BOOLEAN NOT NULL)"
        );
    }

    /**
     * {@inheritDoc}
     *
     * @return void
     */
    protected function createSitesTable()
    {
        $this->pdo->exec(
            "CREATE TABLE IF NOT EXISTS sites (
            sid SERIAL PRIMARY KEY,
            cid INT NOT NULL UNIQUE,
            site_code TEXT NOT NULL UNIQUE,
            site_name TEXT NOT NULL,
            suburb TEXT NOT NULL,
            FOREIGN KEY (cid) REFERENCES credentials(cid))"
        );
    }

    /**
     * {@inheritDoc}
     *
     * @return void
     */
    protected function createUsersTable()
    {
        $this->pdo->exec(
            "CREATE TABLE IF NOT EXISTS users (
            uid SERIAL PRIMARY KEY,
            cid INT NOT NULL,
            sid INT NOT NULL,
            firstname TEXT NOT NULL,
            lastname TEXT NOT NULL,
            role TEXT NOT NULL,
            email TEXT NOT NULL UNIQUE,
            FOREIGN KEY (cid) REFERENCES credentials(cid),
            FOREIGN KEY (sid) REFERENCES sites(sid))"
        );
    }



    /**
     * {@inheritDoc}
     *
     * @return void
     */
    protected function createWardsTable()
    {
        $this->pdo->exec(
            "CREATE TABLE IF NOT EXISTS wards (
            wid SERIAL PRIMARY KEY,
            cid INT NOT NULL UNIQUE,
            sid INT NOT NULL,
            ward_code TEXT NOT NULL,
            ward_name TEXT NOT NULL,
            FOREIGN KEY (cid) REFERENCES credentials(cid),
            FOREIGN KEY (sid) REFERENCES sites(sid))"
        );
    }

    /**
     * {@inheritDoc}
     *
     * @return void
     */
    protected function createDischargeEntriesTable()
    {
        $this->pdo->exec(
            "CREATE TABLE IF NOT EXISTS dc_entries (
            eid SERIAL PRIMARY KEY,
            wid INT NOT NULL,
            firstname TEXT NOT NULL,
            lastname TEXT NOT NULL,
            ur_num TEXT,
            require_medprof BOOLEAN NOT NULL,
            comments TEXT,
            discharge_date BIGINT NOT NULL,
            time_received BIGINT NOT NULL,
            status TEXT NOT NULL,
            time_completed BIGINT,
            time_collected BIGINT,
            collected_by TEXT,
            collected_by_details TEXT,
            FOREIGN KEY (wid) REFERENCES wards(wid))"
        );
    }

    /**
     * {@inheritDoc}
     *
     * @return void
     */
    protected function createSettingsTable()
    {
        $this->pdo->exec(
            "CREATE TABLE IF NOT EXISTS field_settings (
            field TEXT NOT NULL,
            name TEXT NOT NULL,
            value TEXT,
            PRIMARY KEY (field, name))"
        );
    }
}
