<?php
/**
 * UserLoader class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Service;

use Model\User;

/**
 * Service to load user profiles.
 */
class UserLoader
{
    // User role options.
    const ROLE_SYSTEM_ADMIN = 'System Admin';
    const ROLE_ADMIN = 'Admin';
    const ROLE_MANAGER = 'Manager';
    const ROLE_PHARMACIST = 'Pharmacist';
    const ROLE_TECH = 'Technician';

    protected $pdo;

    /**
     * Constructor.
     *
     * @param \PDO $pdo PDO connection.
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Get list of user roles.
     *
     * @return array
     */
    public static function getUserRoles()
    {
        return [
            'sysadmin' => self::ROLE_SYSTEM_ADMIN,
            'admin' => self::ROLE_ADMIN,
            'manager' => self::ROLE_MANAGER,
            'pharmacist' => self::ROLE_PHARMACIST,
            'tech' => self::ROLE_TECH,
        ];
    }

    /**
     * Get user by credential id.
     *
     * @param int $cid Credential id.
     *
     * @return User
     */
    public function getUserByCid($cid)
    {
        return $this->getSingleUser('cid', $cid);
    }

    /**
     * Get user by id.
     *
     * @param int $uid User id.
     *
     * @return User
     */
    public function getUserByUid($uid)
    {
        return $this->getSingleUser('uid', $uid);
    }

    /**
     * Get a single user by requested filter key and value.
     *
     * @param string $key   Database table column name.
     * @param string $value Query parameter value.
     *
     * @return User
     */
    private function getSingleUser($key, $value)
    {
        $user = null;

        $user_array = $this->querySingleUser($key, $value);
        if (!empty($user_array)) {
            $user = $this->getUserFromArray($user_array);
        }

        return $user;
    }

    /**
     * Create User object from array.
     *
     * @param array $user_array Query result.
     *
     * @return User
     */
    private function getUserFromArray(array $user_array)
    {
        $user = new User($user_array['cid']);
        $user->setUid($user_array['uid']);
        $user->setFirstName($user_array['firstname']);
        $user->setLastName($user_array['lastname']);
        $user->setRole($user_array['role']);
        $user->setEmail($user_array['email']);
        $user->setSid($user_array['sid']);
        return $user;
    }

    /**
     * Query for single user.
     *
     * @param string $filter_key Database table column name.
     * @param string $value      Query parameter value.
     *
     * @return array
     */
    private function querySingleUser($filter_key, $value)
    {
        $query = "SELECT * FROM users WHERE $filter_key = :value";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':value', $value);
        $stmt->execute();
        $user_array = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $user_array;
    }
}
