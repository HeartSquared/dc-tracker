<?php
/**
 * TableManager class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Service;

use Model\AbstractTable;
use Model\TableEntries;

/**
 * Renders table data.
 */
class TableManager
{
    private $pdo;

    /**
     * Constructor.
     *
     * @param \PDO $pdo PDO connection.
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Renders headers for the table.
     *
     * @param AbstractTable $table       Table data.
     * @param string        $sort_option Column to sort by.
     * @param string        $sort_order  Sort direction for results.
     *
     * @return void
     */
    private function displayTableHeaders(AbstractTable $table, $sort_option, $sort_order = 'asc')
    {
        $columns = $table->getColumns();
        // Display each column heading.
        echo '<table class="table table-bordered"><thead><tr>';

        foreach ($columns as $col => $value) {
            if ('require_medprof' == $col) {
                echo '<th>Medprof</th>';
            } elseif ('status' == $col) {
                echo '<th>Status</th>';
            } else {
                $sort_order_reverse = $sort_order == 'asc' ? 'desc' : 'asc';
                echo '<th><form method="get" action="">
              <input type="hidden" name="sort_option" value="' . $col . '">
              <input type="hidden" name="sort_order" value="' . $sort_order_reverse . '">
              <button type="submit" class="btn btn-link sort-option';
                if ($sort_option == $col) {
                    echo ' sort-select';
                    if ($sort_order_reverse === 'asc') {
                        echo ' dropup';
                    }
                }
                echo '" id="sort-' . $col . '-' . $sort_order . '">
              <strong>' . $value . '</strong>
              <span id="' . $col . '-caret" class="caret"></span>
            </button></form></th>';
            }
        }

        if ($table->displayReadOnly()) {
            echo '</tr></thead>';
        } else {
            // Add extra column for edit button.
            echo '<th style="width:22px;"></th></tr></thead>';
        }
    }

    /**
     * Renders table data rows.
     *
     * @param AbstractTable $table           Table data.
     * @param string        $collection_type Data collection type.
     * @param string        $sort_option     Column to sort by.
     * @param string        $sort_order      Sort direction for results.
     *
     * @return void
     */
    public function displayTableData(AbstractTable $table, $collection_type, $sort_option, $sort_order)
    {
        $result = $table->getData($sort_option, $sort_order);

        // Span one extra column for edit button.
        $colspan = count($table->getColumns());
        if (!$table->displayReadOnly()) {
            $colspan++;
        }

        if (count($result)) {
            // Output data of each row.
            foreach ($result as $row) {
                // Remove row id from array (used for edit button value).
                $id = array_pop($row);

                // Add table row styling for TableEntries.
                if ('Model\TableEntries' == get_class($table)) {
                    $entry_cancelled = $table->checkEntryCancelled($id);
                    $entry_completed = $table->checkEntryCompleted($id);
                    $entry_collected = $table->checkEntryCollected($id);

                    // Unset Completed and Collected By columns if cancelled.
                    // Replaced with 'Discharge cancelled' further down.
                    if ($entry_cancelled) {
                        unset($row['time_collected']);
                        unset($row['time_completed']);
                    }

                    // Must be checked in this order to apply correct styles.
                    if ($entry_collected || $entry_cancelled) {
                        echo '<tr class="collected">';
                    } elseif ($entry_completed) {
                        echo '<tr style="background-color:#f0fcf0;">';
                    } else {
                        echo '<tr>';
                    }
                } else {
                    echo '<tr>';
                }

                foreach ($row as $col => $value) {
                    echo '<td>';
                    switch ($col) {
                        case 'site_name':
                        case 'ward_name':
                        case 'suburb':
                        case 'firstname':
                        case 'lastname':
                            echo ucwords($row[$col]);
                            break;

                        default:
                            echo $row[$col];
                    }
                    echo '</td>';
                }

                if (!$table->displayReadOnly()) {
                    if (isset($entry_cancelled) && $entry_cancelled) {
                        echo '<td colspan="2">Discharge cancelled</td>';
                    }
                    // Edit button for row.
                    echo '
                    <td style="padding-bottom:0;">
                      <form id="form_edit" method="post" action="includes/edit-forward.php">
                        <input type="image" src="img/edit.png" style="height:22px;" alt="Edit" title="Edit">
                        <input name="id" type="hidden" value="' . $id . '">
                        <input name="type" type="hidden" value="' . $collection_type . '">
                      </form>
                      <span id="old_sort_option" class="hidden">' . $sort_option . '</span>
                    </td>
                  </tr>';
                }
            }
            // Row count.
            echo '<tr class="text-center"><td colspan="' . $colspan . '">Total ' . $collection_type . ': ' .count($result) . '</td></tr>';
        } else {
            echo '<tr><td colspan="' . $colspan . '">There are no ' . $collection_type . ' registered!</td></tr>';
        }
    }

    /**
     * Render table.
     *
     * @param AbstractTable $table           Table data.
     * @param string        $collection_type Data collection type.
     * @param string        $sort_option     Column to sort by.
     * @param string        $sort_order      Sort direction for results.
     *
     * @return void
     */
    public function displayTable(AbstractTable $table, $collection_type, $sort_option, $sort_order = 'asc')
    {
        $this->displayTableHeaders($table, $sort_option, $sort_order);
        echo '<tbody id="table">';
        $this->displayTableData($table, $collection_type, $sort_option, $sort_order);
        echo '</tbody></table>';
    }

    /**
     * Display a custom entry table for Head Office users (excludes Sysadmins).
     *
     * @param TableEntries $table Table data.
     *
     * @return void
     */
    public function displayHeadOfficeEntriesTable(TableEntries $table)
    {
        $site_loader = new SiteLoader($this->pdo);
        $sites = $site_loader->getAllHospitalSites();

        if (!empty($sites)) {
            foreach ($sites as $site) {
                // Get array of row counts for site by criteria.
                $summary = $table->getSiteEntrySummary($site->getSid());
                $total_count = 0;

                echo '<div class="col-sm-6">';

                // Table head.
                echo '<table class="table table-bordered"><thead><tr><th colspan="2">';
                echo ucwords($site->getSiteName());
                echo '</th></tr></thead>';

                // Table body.
                echo '<tbody>';

                foreach ($summary as $criteria => $row_count) {
                    echo '<tr><td>';

                    if (EntryLoader::STATUS_INCOMPLETE == $criteria) {
                        echo 'In Progress';
                    } elseif ('medprof' == $criteria) {
                        echo 'Medication Profile Requested';
                    } else {
                        echo $criteria;
                    }

                    echo "</td><td>$row_count</td></tr>";

                    if ('medprof' != $criteria) {
                        $total_count += $row_count;
                    }
                }

                echo '<tr class="text-center"><td colspan="2">';
                echo "Total discharge entries: $total_count</td></tr>";
                echo '</tbody></table>';
                echo '</div>';
            }
        } else {
            echo '<div class="col-xs-12">';
            echo '<div class="alert alert-info">';
            echo 'No results as there are no sites registered!';
            echo '</div>';
            echo '</div>';
        }
    }
}
