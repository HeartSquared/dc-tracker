<?php
/**
 * CredentialEditor class.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

namespace Service;

/**
 * Service to edit credentials.
 */
class CredentialEditor extends CredentialLoader
{
    /**
     * Create new credential.
     *
     * @param string $username  Username.
     * @param string $user_type User type.
     * @param string $password  Password.
     *
     * @return void
     */
    public function createCredential($username, $user_type, $password, $require_pass_reset = 1)
    {
        $query = "INSERT INTO credentials (username, password, user_type, require_pass_reset) VALUES (:username, :password, :user_type, :require_pass_reset)";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':username', $username);
        $stmt->bindValue(':password', password_hash($password, PASSWORD_DEFAULT));
        $stmt->bindParam(':user_type', $user_type);
        $stmt->bindValue(':require_pass_reset', $require_pass_reset);
        $stmt->execute();
    }

    /**
     * Update username.
     *
     * @param int    $cid      Credential id.
     * @param string $username Username.
     *
     * @return void
     */
    public function updateUsername($cid, $username)
    {
        $query = "UPDATE credentials SET username = :username WHERE cid = :cid";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':username', $username);
        $stmt->bindParam(':cid', $cid);
        $stmt->execute();
    }

    /**
     * Update user password.
     *
     * @param int    $cid      Credential id.
     * @param string $password Password.
     *
     * @return void
     */
    public function updatePassword($cid, $password)
    {
        $query = "UPDATE credentials SET password = :password WHERE cid = :cid";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindValue(':password', password_hash($password, PASSWORD_DEFAULT));
        $stmt->bindParam(':cid', $cid);
        $stmt->execute();
    }

    /**
     * Update stored value of if user needs to reset their password.
     *
     * @param int  $cid                Credential id.
     * @param bool $require_pass_reset If user needs to reset their password.
     *
     * @return void
     */
    public function updatePasswordReset($cid, $require_pass_reset)
    {
        $query = "UPDATE credentials SET require_pass_reset = :require_pass_reset WHERE cid = :cid";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':require_pass_reset', $require_pass_reset);
        $stmt->bindParam(':cid', $cid);
        $stmt->execute();
    }

    /**
     * Delete login credentials.
     *
     * @param int $cid Credential id.
     *
     * @return void
     */
    public function deleteCred($cid)
    {
        $query = "DELETE FROM credentials WHERE cid = :cid";
        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':cid', $cid);
        $stmt->execute();
    }

    /**
     * Check if username is already in use.
     *
     * @param string   $username Username.
     * @param int|null $cid      Credential id.
     *
     * @return bool
     */
    public function checkUserExists($username, $cid = null)
    {
        $query = "SELECT username FROM credentials WHERE username = :username";

        if ($cid) {
            $query .= " AND cid != :cid";
        }

        $stmt = $this->pdo->prepare($query);
        $stmt->bindParam(':username', $username);

        if ($cid) {
            $stmt->bindParam(':cid', $cid);
        }

        $stmt->execute();
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);

        if ($result) {
            return true;
        } else {
            return false;
        }
    }
}
