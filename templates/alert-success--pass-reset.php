<!-- Reset password success -->
<div id="password_reset_success" class="row
<?php if (!$password_reset_success && empty($new_password)) : ?>
    hidden
<?php endif; ?>
">
    <section class="col-xs-12">
        <div class="alert alert-success">
            <p>
                <strong>Password has been reset.</strong>
            </p>
            <p>
                New password:
                <em>
                    <span id="new_password">
<?php if (!empty($new_password)) : ?>
                        <?php echo $new_password; ?>
<?php endif; ?>
                    </span>
                </em>
            </p>
        </div>
    </section>
</div>
