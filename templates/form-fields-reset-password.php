<?php
$has_access_to_edit = !isset($user_has_access_to_edit) || $user_has_access_to_edit;
?>
<?php if ($has_access_to_edit) : ?>
<!-- Button: Reset Password -->
<button type="submit" id="btn_reset_password" name="btn_reset_password" class="btn btn-primary">
    Reset Password
</button>
<?php endif; ?>
