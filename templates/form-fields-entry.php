<!-- Select: Ward -->
<div class="form-group">
    <label for="ward" class="control-label col-xs-12">Ward:</label>
    <div class="col-sm-9 col-xs-12">
        <select id="ward" name="ward" class="form-control">
            <option style="display:none;" value disabled selected> - - Select a ward - - </option>
<?php
$wards = get_wards_accessible_by_user($container, $user);
?>
<?php foreach ($wards as $ward): ?>
            <option value="<?php echo $ward->getWid(); ?>"
    <?php if ($ward->getWid() == $ward_value) : ?>
                selected
    <?php endif; ?>
            >

    <?php
    // Include site for Sysadmins.
    $is_sysadmin = check_access(get_roles_with_access('is sysadmin'), $user);
    if ($is_sysadmin) {
        $sid = $ward->getSid();
        $site_loader = $container->getSiteLoader();
                echo $site_loader->getSiteBySid($sid)->getSiteCode() . ' - ';
    }
    ?>
                <?php echo ucwords($ward->getWardName()); ?> (<?php echo strtoupper($ward->getWardCode()); ?>)
            </option>
<?php endforeach; ?>

        </select>
    </div>
</div>

<!-- Input: First Name -->
<div class="form-group">
    <label for="firstname" class="control-label col-xs-12">First Name:</label>
    <div class="col-xs-12">
        <input type="text" id="firstname" name="firstname" class="form-control" placeholder="required" value="<?php echo $firstname_value; ?>" required>
    </div>
</div>

<!-- Input: Last Name -->
<div class="form-group">
    <label for="lastname" class="control-label col-xs-12">Last Name:</label>
    <div class="col-xs-12">
        <input type="text" id="lastname" name="lastname" class="form-control" placeholder="required" value="<?php echo $lastname_value; ?>" required>
    </div>
</div>

<!-- Input: UR Number -->
<div class="form-group">
    <label for="ur_num" class="control-label col-xs-12">UR Number:</label>
    <div class="col-sm-4 col-xs-12">
        <input type="text" id="ur_num" name="ur_num" maxlength="<?php echo $ur_char_limit; ?>" class="form-control" value="<?php echo $ur_num_value; ?>" >
    </div>
</div>

<!-- Checkbox: Medprof Required -->
<div class="form-group">
    <label for="medprof_required" class="control-label col-xs-12">Medication Profile:</label>
    <div class="col-sm-6 col-xs-12">
        <input type="checkbox" id="medprof_required" name="medprof_required" data-toggle="toggle" data-on="Required" data-off="Not Required" data-onstyle="info"
<?php if ($medprof_required_checked) : ?>
            checked
<?php endif; ?>
        >
    </div>
</div>

<!-- Input: Discharge Date -->
<div class="form-group">
    <label for="discharge_date" class="control-label col-xs-12">Discharge Date:</label>
    <div class="col-sm-6 col-xs-12">
        <!-- Javascript disabled -->
        <div id="field_discharge_date_no_js">
            <input type="text" name="discharge_date" class="form-control" placeholder="required (dd/mm/yyyy)" value="<?php echo $discharge_date_value; ?>" required>
        </div>
        <!-- Javascript enabled -->
        <div id="field_discharge_date_js" class="input-group hidden">
            <div id="datepicker" class="input-group date">
                <input type="text" id="discharge_date" class="form-control" readonly>
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
        </div>
    </div>
</div>

<!-- Input: Comments -->
<div class="form-group">
    <label for="comments" class="control-label col-xs-12">Comments:</label>
    <div class="col-sm-12 col-xs-12">
        <textarea type="text" id="comments" name="comments" rows="3" class="form-control"><?php echo $comments_value; ?></textarea>
    </div>
</div>
