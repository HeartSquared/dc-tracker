<?php
use Service\SiteLoader;
?>

<!-- Input: Hospital -->
<?php
$head_office_site_code = SiteLoader::SITECODE_HEADOFFICE;
?>
<div class="form-group">
    <label for="site_code" class="control-label col-xs-12">Hospital:</label>
    <div class="col-xs-12">

<?php if ($head_office_site_code != $user_site_code) : ?>
        <!-- Logged in user is based at a hospital site -->
        <select id="site_code" name="site_code" class="form-control" ng-model="site_code" ng-value="site_code" disabled>
    <?php
    $site = $site_loader->getSiteBySid($user_sid);
    ?>
            <option value="<?php echo $site->getSiteCode(); ?>" selected>
                <?php echo ucwords($site->getSiteName()) . ' (' . strtoupper($site->getSiteCode()) . ')'; ?>
            </option>
        </select>

<?php elseif (!empty($has_entries)) : ?>
        <!-- Ward has entries associated -->
        <select id="site_code" name="site_code" class="form-control" ng-model="site_code" ng-value="site_code" disabled>
    <?php
    $site = $site_loader->getSiteBySid($sid);
    ?>
            <option value="<?php echo $site->getSiteCode(); ?>" selected>
                <?php echo ucwords($site->getSiteName()) . ' (' . strtoupper($site->getSiteCode()) . ')'; ?>
            </option>
        </select>

<?php else : ?>
        <!-- Logged in user is from Head Office and ward has no entries associated -->
        <select id="site_code" name="site_code" class="form-control" ng-model="site_code" ng-value="site_code">
            <option style="display:none;" value disabled selected> - - Select a site - - </option>
    <?php
    // Get all sites.
    $sites = $site_loader->getAllHospitalSites();
    ?>

    <?php foreach ($sites as $site) : ?>
            <option value="<?php echo $site->getSiteCode(); ?>"
        <?php if ($site->getSiteCode() == $site_code_value) : ?>
                selected
        <?php endif; ?>
            >
                <?php echo ucwords($site->getSiteName()); ?> (<?php echo strtoupper($site->getSiteCode()); ?>)
            </option>
    <?php endforeach; ?>
        </select>
<?php endif; ?>

    </div>
</div>

<!-- Input: Ward Name -->
<div class="form-group">
    <label for="ward_name" class="control-label col-xs-12">Ward Name:</label>
    <div class="col-xs-12">
        <input type="text" id="ward_name" name="ward_name" class="form-control" placeholder="eg. Intensive Care Unit" value="<?php echo $ward_name_value; ?>" required>
    </div>
</div>

<!-- Input: Ward Code -->
<div class="form-group">
    <label for="ward_code" class="control-label col-xs-12">Ward Code (max 6 characters, no spaces):</label>
    <div class="col-sm-4 col-xs-12">
        <input type="text" id="ward_code" name="ward_code" class="form-control" maxlength="6" placeholder="eg. ICU" ng-model="ward_code" ng-value="ward_code" value="<?php echo $ward_code_value; ?>" required
<?php if (!empty($has_entries)) : ?>
            disabled
<?php endif; ?>
        >
    </div>
</div>

<!-- Username (Angular auto-fill) -->
<div id="field_username" class="hidden">
    <div class="form-group">
        <div class="col-xs-12">
            <label for="username">Username:</label>
            <span>
                <span ng-show="site_code != null">{{site_code + "_" | lowercase}}</span><span ng-show="ward_code != null">{{ward_code | lowercase}}</span>
            </span>
        </div>
    </div>
</div>
