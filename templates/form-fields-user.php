<?php
use Service\SiteLoader;
$has_access_to_edit = !isset($user_has_access_to_edit) || $user_has_access_to_edit;
?>

<!-- Input: First Name -->
<div class="form-group">
    <label for="firstname" class="control-label col-xs-12">First Name:</label>
    <div class="col-xs-12">
        <input type="text" id="firstname" name="firstname" class="form-control" ng-model="firstname" ng-value="firstname" placeholder="required" value="<?php echo $firstname_value; ?>"
<?php if (!$has_access_to_edit) : ?>
            disabled
<?php endif; ?>
        required>
    </div>
</div>

<!-- Input: Last Name -->
<div class="form-group">
    <label for="lastname" class="control-label col-xs-12">Last Name:</label>
    <div class="col-xs-12">
        <input type="text" id="lastname" name="lastname" class="form-control" ng-model="lastname" ng-value="lastname" placeholder="required" value="<?php echo $lastname_value; ?>"
<?php if (!$has_access_to_edit) : ?>
            disabled
<?php endif; ?>
        required>
    </div>
</div>

<!-- Email -->
<?php
$pharmacy_domain_value = get_pharmacy_domain($container);
?>
<div class="form-group">
    <label for="email" class="control-label col-xs-12">Email:</label>
    <div class="col-xs-12">

<?php if (!empty($account) && $generated_email != $account->getEmail()) : ?>
        <input id="email" type="text" name="email" class="form-control" value="<?php echo $email_value; ?>" placeholder="required"
    <?php if (!$has_access_to_edit) : ?>
            disabled
    <?php endif; ?>
        required>

<?php else : ?>
        <div id="field_email_no_js">
            <input type="text" name="email" class="form-control" value="<?php echo $email_value; ?>" placeholder="required"
    <?php if (!$has_access_to_edit) : ?>
            disabled
    <?php endif; ?>
            required>
        </div>
        <div id="field_email_js" class="hidden">
    <?php if (empty($pharmacy_domain_value)) : ?>
        <input id="email" type="text" class="form-control empty-pharmacy-domain" value="<?php echo $email_value; ?>" placeholder="required" disabled required>
    <?php else : ?>
        <?php if ($has_access_to_edit) : ?>
            <div class="input-group">
        <?php endif; ?>
                <input id="email" type="text" class="form-control" value='{{firstname + "." + lastname + domain_name | lowercase}}' disabled required>
        <?php if ($has_access_to_edit) : ?>
                <span id="btn_edit_email" class="input-group-btn">
                    <button type="button" class="btn btn-default">Edit</button>
                </span>
            </div>
        <?php endif; ?>
    <?php endif; ?>
        </div>
<?php endif; ?>

    </div>
</div>

<!-- Input: User Role -->
<div class="form-group">
    <label for="role" class="control-label col-xs-12">User Role:</label>
    <div class="col-sm-6 col-xs-12">
<?php
// Get User Roles options.
$user_roles = $container->getUserLoader()->getUserRoles();
?>

<?php if (!empty($uid) && 1 == $uid) : ?>
        <!-- Editing User 1 -->
        <select id="role" name="role" class="form-control" disabled required>
            <option value="<?php echo $user_roles['sysadmin'] ?>" selected>
                <?php echo $user_roles['sysadmin'] ?>
            </option>
        </select>

<?php else : ?>
        <select id="role" name="role" class="form-control"
    <?php if (!$has_access_to_edit) : ?>
            disabled
    <?php endif; ?>
        required>

    <?php
    // Get admin roles.
    $admin_roles = [$user_roles['sysadmin'], $user_roles['admin']];

    if ($has_access_to_edit) {
        // Enable options for System Admin and Admin only for users with the
        // role or higher access.
        if ($user_roles['sysadmin'] != $user->getRole()) {
            unset($user_roles['sysadmin']);
            if ($user_roles['admin'] != $user->getRole()) {
                unset($user_roles['admin']);
            }
        }
    }
    ?>

    <?php foreach ($user_roles as $role): ?>
            <option value="<?php echo $role; ?>"
        <?php if ($role == $role_value) : ?>
                selected
        <?php endif; ?>
            ">
                <?php echo $role; ?>
            </option>
    <?php endforeach; ?>

        </select>
<?php endif; ?>

    </div>
</div>

<!-- Input: Site -->
<?php
$head_office_site_code = SiteLoader::SITECODE_HEADOFFICE;
?>
<div class="form-group">
    <label for="site_code" class="control-label col-xs-12">Site:</label>
    <div class="col-sm-8 col-xs-12">

<?php if ($head_office_site_code != $user_site_code) : ?>
        <!-- Logged in user is based at a hospital site -->
        <select id="site_code" name="site_code" class="form-control" disabled>
<?php
$site = $site_loader->getSiteBySid($user_sid);
?>
            <option value="<?php echo $site->getSiteCode(); ?>" selected>
                <?php echo ucwords($site->getSiteName()) . ' (' . strtoupper($site->getSiteCode()) . ')'; ?>
            </option>
        </select>

<?php elseif (!empty($uid) && 1 == $uid): ?>
        <!-- Editing User 1 -->
        <select id="site_code" name="site_code" class="form-control" disabled>
            <option value="<?php echo $head_office_site_code; ?>" selected>Head Office</option>
        </select>

<?php else : ?>
        <!-- Logged in user is from Head Office -->
        <select id="site_code" name="site_code" class="form-control"
    <?php if (!$has_access_to_edit) : ?>
            disabled
    <?php endif; ?>
        >
            <option style="display:none;" value disabled selected> - - Select a site - - </option>
            <option value="<?php echo $head_office_site_code; ?>"
    <?php if (isset($account_site_code) && $head_office_site_code == $account_site_code) : ?>
                selected
    <?php endif; ?>
              >
                Head Office
            </option>

    <?php
    // Get all sites.
    $sites = $site_loader->getAllHospitalSites();
    ?>

    <?php foreach ($sites as $site) : ?>
            <option value="<?php echo $site->getSiteCode(); ?>"
        <?php if ($site->getSiteCode() == $site_code_value) : ?>
                selected
        <?php endif; ?>
            >
                <?php echo ucwords($site->getSiteName()); ?> (<?php echo strtoupper($site->getSiteCode()); ?>)
            </option>
    <?php endforeach; ?>

        </select>
<?php endif; ?>

    </div>
</div>
