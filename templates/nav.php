<?php
/**
 * Navbar.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

use Model\AbstractUserType;

require_once 'includes/common.php';

$site_loader = $container->getSiteLoader();
$head_office = $site_loader->getHeadOffice();

$is_type_user = false;
$is_pharmacy = false;
$is_manager = false;
$is_admin = false;
$is_sysadmin = false;

// Check logged in user's access.
if ($logged_in) {
    $is_type_user = check_access(get_roles_with_access('is type user'), $user);
    $is_pharmacy = check_access(get_roles_with_access('is pharmacy'), $user);
    $is_manager = check_access(get_roles_with_access('is manager'), $user);
    $is_admin = check_access(get_roles_with_access('is admin'), $user);
    $is_sysadmin = check_access(get_roles_with_access('is sysadmin'), $user);
}
?>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">

<?php if ($logged_in) : ?>
            <!-- Mobile nav toggle -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar top-bar"></span>
                <span class="icon-bar middle-bar"></span>
                <span class="icon-bar bottom-bar"></span>
            </button>
<?php endif; ?>

            <a class="navbar-brand" href="index.php" style="color:#f59654;">DC-Tracker</a>
        </div><!-- /.navbar-header -->


<?php if ($logged_in) : ?>
        <div class="collapse navbar-collapse" id="navbar">
            <ul class="nav navbar-nav">

    <?php if ($is_sysadmin || ($is_type_user && $user->getSid() !== $head_office->getSid())) : ?>
                <!-- Pharmacy only access -->
                <li><a href="add-entry.php">Add discharge</a></li>
    <?php endif; ?>

    <?php if ($is_manager) : ?>
                <!-- Manager only access -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                      Admin
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">

                      <!-- * REGISTER NEW * -->
                      <li class="dropdown-header">Register New</li>
        <?php if ($is_admin) : ?>
                      <!-- Admin only access -->
                      <li><a href="register-site.php">Site</a></li>
        <?php endif; ?>
                      <li><a href="register-ward.php">Ward</a></li>
                      <li><a href="register-user.php">User</a></li>

                      <!-- * VIEW / EDIT * -->
                      <li role="separator" class="divider"></li>
                      <li class="dropdown-header">View / Edit</li>
        <?php if ($is_admin) : ?>
                      <!-- Admin only access -->
                      <li><a href="sites.php">Sites</a></li>
        <?php endif; ?>
                      <li><a href="wards.php">Wards</a></li>
                      <li><a href="users.php">Users</a></li>

        <?php if ($is_admin) : ?>
                      <!-- * ADMIN * -->
                      <!-- Admin only access -->
                      <li role="separator" class="divider"></li>
                      <li class="dropdown-header">Admin</li>
                      <li><a href="admin-settings.php">Settings</a></li>
        <?php endif; ?>

                    </ul>
                </li>
    <?php endif; ?>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php echo $user_credential->getUsername(); ?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
    <?php if ($is_type_user) : ?>
                        <li><a href="edit-profile.php">Profile</a></li>
    <?php endif; ?>
                        <li><a href="password-change.php">Change Password</a></li>
                        <li><a href="includes/logout.php">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /#navbar -->
<?php endif; ?>

    </div><!-- /.container -->
</nav>
