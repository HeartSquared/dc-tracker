<section id="error" class="col-sm-6 col-xs-12
<?php if ($error_hidden) : ?>
    hidden
<?php endif; ?>
">
    <div class="alert alert-danger col-xs-12">
        <p id="error_message">
<?php if (isset($error_message)) : ?>
            <?php echo $error_message; ?>
<?php endif; ?>
        </p>
    </div>
</section>
