<!-- DEFAULTS -->
<?php
$show_defaults = false;

if (empty($setting_loader)) {
    // Setting loader is not called - likely on installation step.
    // Define ids and show defaults fieldset.
    $pharmacy_domain_id = 'pharmacy_domain__default';
    $static_password_id = 'static_password__default';
    $ur_char_length_id = 'ur_num__char_length';
    $ur_leading_zero_id = 'ur_num__leading_zero';
    $ur_alphanumeric_id = 'ur_num__alphanumeric';
    $show_defaults = true;
} elseif (check_access(get_roles_with_access('is sysadmin'), $user)) {
    // Allow only Sysadmins to see defaults fieldset.
    $show_defaults = true;
}
?>
<?php if ($show_defaults) : ?>
<fieldset>
    <legend>Defaults</legend>

    <!-- Input: Pharmacy Website Domain -->
    <div class="form-group">
        <label for="<?php echo $pharmacy_domain_id; ?>" class="control-label col-xs-12">Pharmacy Website Domain:</label>
        <div class="col-xs-12">
            <input type="text" id="<?php echo $pharmacy_domain_id; ?>" name="<?php echo $pharmacy_domain_id; ?>" class="form-control" placeholder="eg. example.com" value="<?php echo $pharmacy_domain_value; ?>">
            <small class="form-text text-muted">
              This is used to assist automated email generating. Leave this blank if you do not wish to use this option.
            </small>
        </div>
    </div>

    <!-- Input: Static Password -->
    <div class="form-group">
        <label for="<?php echo $static_password_id; ?>" class="control-label col-xs-12">Static Password:</label>
        <div class="col-xs-12">
            <input type="text" id="<?php echo $static_password_id; ?>" name="<?php echo $static_password_id; ?>" class="form-control" value="<?php echo $static_password_value; ?>">
            <small class="form-text text-muted">
              A static default password for new accounts and when reseting passwords. Leave this blank to generate random passwords (recommended).
            </small>
        </div>
    </div>
</fieldset>
<?php endif; ?>

<!-- UR NUMBER -->
<fieldset>
    <legend>UR Number</legend>
    <!-- Input: Character Limit -->
    <div class="form-group">
        <label for="<?php echo $ur_char_length_id; ?>" class="control-label col-xs-12">Character Limit:</label>
        <div class="col-sm-4 col-xs-12">
            <select id="<?php echo $ur_char_length_id; ?>" name="<?php echo $ur_char_length_id; ?>" class="form-control">
<?php for ($n = 5; $n <= 10; $n++) : ?>
                <option value="<?php echo $n; ?>"
<?php if ($n == $ur_char_length_value) : ?>
                    selected
<?php endif; ?>
                >
                    <?php echo $n; ?>
                </option>
<?php endfor; ?>
            </select>
        </div>
    </div>

    <!-- Checkbox: Leading Zeroes -->
    <div class="form-group">
        <label for="<?php echo $ur_leading_zero_id; ?>" class="control-label col-xs-12">Leading Zeroes:</label>
        <div class="col-sm-6 col-xs-12">
            <input type="checkbox" id="<?php echo $ur_leading_zero_id; ?>" name="<?php echo $ur_leading_zero_id; ?>" data-toggle="toggle" data-on="Include" data-off="Exclude" data-onstyle="info"
<?php if ($ur_leading_zero_value) : ?>
                checked
<?php endif; ?>
            >
        </div>
    </div>

    <!-- Checkbox: Alphanumeric -->
    <div class="form-group">
        <label for="<?php echo $ur_alphanumeric_id; ?>" class="control-label col-xs-12">Alphanumeric:</label>
        <div class="col-sm-6 col-xs-12">
            <input type="checkbox" id="<?php echo $ur_alphanumeric_id; ?>" name="<?php echo $ur_alphanumeric_id; ?>" data-toggle="toggle" data-on="Alphanumeric" data-off="Numeric Only" data-onstyle="info"
<?php if ($ur_alphanumeric_value) : ?>
                checked
<?php endif; ?>
            >
        </div>
    </div>
</fieldset>
