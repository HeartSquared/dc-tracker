<!-- Input: Hospital Name -->
<div class="form-group">
    <label for="hosp_name" class="control-label col-xs-12">Hospital Name:</label>
    <div class="col-xs-12">
        <input type="text" id="hosp_name" name="hosp_name" class="form-control" placeholder="eg. Epworth Eastern" value="<?php echo $hosp_name_value; ?>" required>
    </div>
</div>

<!-- Input: Suburb -->
<div class="form-group">
    <label for="suburb" class="control-label col-xs-12">Suburb:</label>
    <div class="col-xs-12">
        <input type="text" id="suburb" name="suburb" class="form-control" placeholder="eg. Box Hill" value="<?php echo $suburb_value; ?>" required>
    </div>
</div>

<!-- Input: Site Code -->
<div class="form-group">
    <label for="site_code" class="control-label col-xs-12">Site Code (max 6 characters, no spaces):</label>
    <div class="col-sm-4 col-xs-12">
        <input type="text" id="site_code" name="site_code" class="form-control" maxlength="6" placeholder="eg. BOX" ng-value="site_code" ng-model="site_code" value="<?php echo $site_code_value; ?>"
<?php if (!empty($has_wards)) : ?>
            disabled
<?php endif; ?>
        required>
    </div>
</div>

<!-- Username (Angular auto-fill) -->
<div id="field_username" class="hidden">
    <div class="form-group">
        <div class="col-xs-12">
            <label for="username">General Pharmacy Username:</label>
            <span>
                {{site_code + "_pharmacy" | lowercase}}
            </span>
        </div>
    </div>
</div>
