<?php
/**
 * Page - Index.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

if (PHP_SESSION_NONE == session_status()) {
    session_start();
}

$no_redirect = true;
$base_path = basename(__DIR__);

if (!file_exists('includes/config/config.php')) {
    header('Location: install.php');
}

require_once 'includes/common.php';

if ($logged_in) {
    header('Location: index.php');
}

// Error setting for if Javascript is not enabled.
$error_hidden = true;
$error_message = '';
require_once 'includes/login.php';

?>
<!DOCTYPE html>
<html>

    <head>
        <?php require_once 'includes/incl-head.html'; ?>
        <title>DC-Tracker - Login</title>
    </head>

    <body class="container">
        <!-- Navigation Bar -->
        <?php require_once 'templates/nav.php'; ?>

        <!-- Main Content -->
        <div class="container">

            <!-- Page Title -->
            <div class="row">
                <section class="col-xs-12">
                    <h3>Login</h3>
                </section>
            </div>

            <!-- Content -->
            <div class="row">

                <!-- Login Form -->
                <form id="form_login" method="post" action="" class="form-horizontal">
                    <section class="col-sm-6 col-xs-12">

                        <!-- Input: Username -->
                        <div class="form-group">
                            <label for="username" class="control-label col-xs-12">Username/Email:</label>
                            <div class="col-xs-12">
                                <input type="text" id="username" name="username" class="form-control" required
                                <?php if (isset($_POST['username'])) : ?>
                                    value="<?php echo $_POST['username']; ?>"
                                <?php endif; ?>
                                >
                            </div>
                        </div>

                        <!-- Input: Password -->
                         <div class="form-group">
                            <label for="password" class="control-label col-xs-12">Password:</label>
                            <div class="col-xs-12">
                                <input type="password" id="password" name="password" class="form-control" required>
                            </div>
                        </div>

                        <!-- Submit Button: Login -->
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input type="submit" name="btn_login" class="btn btn-default pull-right col-sm-2 col-xs-12" value="Login">
                            </div>
                        </div>

                    </section>
                </form>

                <!-- Error -->
                <?php require 'templates/alert-error.php'; ?>

            </div><!-- /.row -->
        </div><!-- /.container -->

        <!-- Footer -->
        <?php require_once 'templates/footer.php'; ?>

        <!-- Scripts -->
        <?php require_once 'includes/incl-js.html'; ?>
        <script>
          $(document).ready(function(){
            $('#form_login').submit(function(e){
              //Stop the form from submitting itself to the server.
              e.preventDefault();
              // Assign input values to variables.
              var username = $('#username').val();
              var password = $('#password').val();

              var data = {
                username: username,
                password: password
              };
              // Pass data to ajax form.
              $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: 'includes/login.php',
                data: {
                  json_data: JSON.stringify(data)
                },
                success: function(result) {
                  // No errors.
                  if (true == result.success) {
                    // Password requires reset.
                    if (true == result.reset_required) {
                      $(location).attr('href', 'password-reset.php');
                    }
                    else {
                      $(location).attr('href', 'index.php');
                    }
                  }
                  // Error occurred (username/password didn't match).
                  else {
                    // Display messages.
                    $('#error').removeClass('hidden');
                    $('#error_message').html(result.error);
                  }
                }
              });
            });
          });
        </script>

    </body>
</html>
