<?php
/**
 * Page - PHP info.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

$base_path = basename(__DIR__);

require_once 'includes/common.php';
?>

<!DOCTYPE html>
<html>

    <head>
        <?php require_once 'includes/incl-head.html'; ?>
        <title>DC-Tracker - PHP Technical Info</title>
    </head>

    <body class="container">
        <!-- Navigation Bar -->
        <?php require_once 'templates/nav.php'; ?>

        <!-- Main Content -->
        <div class="container">

            <!-- Page Title -->
            <div class="row">
                <section class="col-xs-12">
                    <h3>PHP Info</h3>
                </section>
            </div>

            <!-- Content -->
            <div class="row">

                <!-- Table -->
                <section class="col-xs-12">
                    <?php phpinfo(); ?>
                </section>

            </div><!-- /.row -->
        </div><!-- /.container -->

        <!-- Footer -->
        <?php require_once 'templates/footer.php'; ?>

    </body>
</html>
