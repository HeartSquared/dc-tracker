<?php
/**
 * Page - Installation.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

$config_file = 'includes/config/config.php';
// Requires checking if post data exists, as config file may be created even if
// installation isn't complete.
$has_access_to_install = !file_exists($config_file) || isset($_POST['db_host']);

// Error setting for if Javascript is not enabled.
$success_hidden = true;
$error_hidden = true;
$error_message = '';
require_once 'includes/install.php';

// Value filling for if Javascript is not enabled.
$db_type_value = isset($_POST['db_type']) ? $_POST['db_type'] : null;
$db_name_value = isset($_POST['db_name']) ? $_POST['db_name'] : null;
$db_directory_value = isset($_POST['db_directory']) ? $_POST['db_directory'] : null;
$db_host_value = isset($_POST['db_host']) ? $_POST['db_host'] : null;
$db_username_value = isset($_POST['db_username']) ? $_POST['db_username'] : null;
$db_password_value = isset($_POST['db_password']) ? $_POST['db_password'] : null;
$sysadmin_firstname_value = isset($_POST['sysadmin_firstname']) ? $_POST['sysadmin_firstname'] : null;
$sysadmin_lastname_value = isset($_POST['sysadmin_lastname']) ? $_POST['sysadmin_lastname'] : null;
$sysadmin_email_value = isset($_POST['sysadmin_email']) ? $_POST['sysadmin_email'] : null;
$sysadmin_password_value = isset($_POST['sysadmin_password']) ? $_POST['sysadmin_password'] : null;
$pharmacy_domain_value = isset($_POST['pharmacy_domain__default']) ? $_POST['pharmacy_domain__default'] : null;
$static_password_value = isset($_POST['static_password__default']) ? $_POST['static_password__default'] : null;
$ur_char_length_value = isset($_POST['ur_num__char_length']) ? $_POST['ur_num__char_length'] : 7;
$ur_leading_zero_value = isset($_POST['ur_num__leading_zero']) ? $_POST['ur_num__leading_zero'] : 1;
$ur_alphanumeric_value = isset($_POST['ur_num__alphanumeric']) ? $_POST['ur_num__alphanumeric'] : 0;
?>

<!DOCTYPE html>
<html>

    <head>
        <?php
        require_once 'includes/incl-head.html';
        require 'includes/incl-form-style.html';
        ?>
        <title>DC-Tracker - Install</title>
    </head>

    <body>
        <!-- Main Content -->
        <div class="container">

            <!-- Page Title -->
            <div class="row">
                <section class="col-sm-8 col-sm-offset-2 col-xs-12">
                    <h3>Installation</h3>
                </section>
            </div>

<?php if (!$has_access_to_install) : ?>
            <!-- Content -->
            <div class="row">
                <!-- Warning -->
                <section id="error" class="col-sm-8 col-sm-offset-2 col-xs-12">
                    <div class="alert alert-warning col-xs-12">
                        <p>
                            The installation has already been run.
                        </p>
                        <p>
                            If you wish to re-install, delete the config file and return to this page.
                        </p>
                        <p><a href="index.php">Return to index</a></p>
                    </div>
                </section>
<?php else : ?>
            <!-- Content -->
            <div class="row">
    <?php if ($success_hidden) : ?>
        <?php if (!$error_hidden) : ?>
                <!-- Error -->
                <section id="error" class="col-sm-8 col-sm-offset-2 col-xs-12">
                    <div class="alert alert-danger col-xs-12">
                        <p id="error_message">
            <?php if (isset($error_message)) : ?>
                            <?php echo $error_message; ?>
            <?php endif; ?>
                        </p>
                    </div>
                </section>
        <?php endif; ?>

                <!-- Register Form -->
                <form id="form_install" method="post" action="" class="form-horizontal">

                    <section class="col-sm-8 col-sm-offset-2 col-xs-12">
                        <!-- Fields -->
                        <!-- DATABASE -->
                        <fieldset>
                            <legend>Database</legend>
                            <!-- Select: Database Type -->
                            <div class="form-group">
                                <label for="db_type" class="control-label col-xs-12">Type:</label>
                                <div class="col-sm-6 col-xs-12">
                                    <select id="db_type" name="db_type" class="form-control">
                                        <option style="display:none;" value disabled selected> - - Select a database type - - </option>
        <?php
        $db_types = [
            'mysql' => 'MySQL',
            'pgsql' => 'PostgreSQL',
            'sqlite' => 'SQLite',
        ];
        ?>
        <?php foreach ($db_types as $type => $label) : ?>
                                        <option value="<?php echo $type; ?>"
            <?php if ($type == $db_type_value) : ?>
                                            selected
            <?php endif; ?>
                                        >
                                            <?php echo $label; ?>
        <?php endforeach; ?>
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <!-- Input: Database Name -->
                            <div class="form-group">
                                <label for="db_name" class="control-label col-xs-12">Name:</label>
                                <div class="col-xs-12">
                                    <input type="text" id="db_name" name="db_name" class="form-control" placeholder="required" value="<?php echo $db_name_value; ?>" required>
                                </div>
                            </div>

                            <!-- Input: Database Host -->
                            <div class="form-group">
                                <label for="db_host" class="control-label col-xs-12">Host:</label>
                                <div class="col-xs-12">
                                    <input type="text" id="db_host" name="db_host" class="form-control" value="<?php echo $db_host_value; ?>">
                                    <small class="form-text text-muted">
                                      For SQLite, enter the absolute path to the directory where the database is stored (eg. /var/www). Ensure you have set the correct permissions for the folder.
                                    </small>
                                </div>
                            </div>

                            <!-- Input: Database Username -->
                            <div class="form-group">
                                <label for="db_username" class="control-label col-xs-12">Username:</label>
                                <div class="col-xs-12">
                                    <input type="text" id="db_username" name="db_username" class="form-control" value="<?php echo $db_username_value; ?>">
                                    <small class="form-text text-muted">
                                      Not required for SQLite.
                                    </small>
                                </div>
                            </div>

                            <!-- Input: Database Password -->
                            <div class="form-group">
                                <label for="db_password" class="control-label col-xs-12">Password:</label>
                                <div class="col-xs-12">
                                    <input type="password" id="db_password" name="db_password" class="form-control" value="<?php echo $db_password_value; ?>">
                                    <small class="form-text text-muted">
                                      Not required for SQLite.
                                    </small>
                                </div>
                            </div>
                        </fieldset>

                        <!-- SYSADMIN -->
                        <fieldset>
                            <legend>System Administrator</legend>
                            <!-- Input: First Name -->
                            <div class="form-group">
                                <label for="sysadmin_firstname" class="control-label col-xs-12">First Name:</label>
                                <div class="col-xs-12">
                                    <input type="text" id="sysadmin_firstname" name="sysadmin_firstname" class="form-control" value="<?php echo $sysadmin_firstname_value; ?>">
                                    <small class="form-text text-muted">
                                      Required when creating a new database.
                                    </small>
                                </div>
                            </div>

                            <!-- Input: Last Name -->
                            <div class="form-group">
                                <label for="sysadmin_lastname" class="control-label col-xs-12">Last Name:</label>
                                <div class="col-xs-12">
                                    <input type="text" id="sysadmin_lastname" name="sysadmin_lastname" class="form-control" value="<?php echo $sysadmin_lastname_value; ?>">
                                    <small class="form-text text-muted">
                                      Required when creating a new database.
                                    </small>
                                </div>
                            </div>

                            <!-- Input: Email -->
                            <div class="form-group">
                                <label for="sysadmin_email" class="control-label col-xs-12">Email:</label>
                                <div class="col-xs-12">
                                    <input type="text" id="sysadmin_email" name="sysadmin_email" class="form-control" value="<?php echo $sysadmin_email_value; ?>">
                                    <small class="form-text text-muted">
                                      Required when creating a new database.
                                    </small>
                                </div>
                            </div>

                            <!-- Input: Password -->
                            <div class="form-group">
                                <label for="sysadmin_password" class="control-label col-xs-12">Password:</label>
                                <div class="col-xs-12">
                                    <input type="password" id="sysadmin_password" name="sysadmin_password" class="form-control" value="<?php echo $sysadmin_password_value; ?>">
                                    <small class="form-text text-muted">
                                      Required when creating a new database.
                                    </small>
                                </div>
                            </div>
                        </fieldset>

                        <?php include 'templates/form-fields-settings.php'; ?>

                    </section>

                    <!-- Submit Button: Install -->
                    <section class="col-sm-8 col-sm-offset-2 col-xs-12">
                        <input type="submit" name="btn_install" class="btn btn-success col-xs-12" value="Install">
                    </section>
                </form>
    <?php else: ?>

                <!-- Success -->
                <section id="success" class="col-sm-8 col-sm-offset-2 col-xs-12">
                    <div class="alert alert-success col-xs-12">
                        <p>Install successful.</p>
                        <p><a href="index.php">Return to index</a></p>
                    </div>
                </section>
    <?php endif; ?>
<?php endif; ?>

            </div><!-- /.row -->

        </div><!-- /.container -->

        <!-- Scripts -->
        <?php
        require_once 'includes/incl-js.html';
        require_once 'includes/incl-form-js.html';
        ?>
    </body>
</html>
