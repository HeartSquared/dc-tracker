<?php
/**
 * Page - Edit registered user details.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

$base_path = basename(__DIR__);

require_once 'includes/common.php';

use Service\UserLoader;

// Check if there is an entry to edit.
if (empty($_SESSION['editing_uid'])) {
    header('Location: users.php');
}

$credential_editor = $container->getCredentialEditor();
$site_loader = $container->getSiteLoader();
$user_editor = $container->getUserEditor();

// Get the current logged in user's details.
$user_role = $user->getRole();
$user_has_sysadmin_access = check_access(get_roles_with_access('is sysadmin'), $user);
$user_has_admin_access = check_access(get_roles_with_access('is admin'), $user);
$user_has_manager_access = check_access(get_roles_with_access('is manager'), $user);
$user_sid = $user->getSid();
$user_site_code = $site_loader->getSiteBySid($user_sid)->getSiteCode();

// Get editing user details.
$uid = $_SESSION['editing_uid'];
$account = $user_editor->getUserByUid($uid);
$account_role = $account->getRole();
$account_is_sysadmin = UserLoader::ROLE_SYSTEM_ADMIN == $account_role;
$account_is_admin = UserLoader::ROLE_ADMIN == $account_role;
$account_is_manager = UserLoader::ROLE_MANAGER == $account_role;
$account_sid = $account->getSid();
$account_site_code = $site_loader->getSiteBySid($account_sid)->getSiteCode();

// Get automatically generated email based on user's name.
$pharmacy_domain_value = get_pharmacy_domain($container);
$generated_email = "{$account->getFirstName()}.{$account->getLastName()}@{$pharmacy_domain_value}";
$generated_email = strtolower($generated_email);

// If logged in user has a manager role, check user to edit is from their site.
$user_is_manager = UserLoader::ROLE_MANAGER == $user_role;
$user_manages_account = $user_is_manager && ($user_sid == $account_sid);

// Check if logged in user has access to edit the user.
// The logged in user must have higher access than the editing user.
$user_has_access_to_edit = false;
if ($user_has_sysadmin_access || ($user_has_admin_access && !$account_is_sysadmin) || ($user_has_manager_access && !($account_is_sysadmin || $account_is_admin) && $user_manages_account)) {
    $user_has_access_to_edit = true;
}

// Error setting for if Javascript is not enabled.
$password_reset_success = false;
$new_password = null;
require_once 'includes/pass-admin-reset.php';
$error_hidden = true;
$error_message = '';
require_once 'includes/edit-user.php';

// Value filling for if Javascript is not enabled.
$firstname_value = isset($_POST['firstname']) ? $_POST['firstname'] : $account->getFirstName();
$lastname_value = isset($_POST['lastname']) ? $_POST['lastname'] : $account->getLastName();
$email_value = isset($_POST['email']) ? $_POST['email'] : $account->getEmail();
$role_value = isset($_POST['role']) ? $_POST['role'] : $account_role;
$site_code_value = isset($_POST['site_code']) ? $_POST['site_code'] : $account_site_code;
?>

<!DOCTYPE html>
<html>

    <head>
        <?php require_once 'includes/incl-head.html'; ?>
        <title>DC-Tracker - Edit User</title>
        <script src="vendor/angular.min.js"></script>
        <script>
          var app = angular.module('formUser', []);

          var firstname = <?php echo json_encode($account->getFirstName()); ?>;
          var lastname = <?php echo json_encode($account->getLastName()); ?>;
          var domain_name = <?php echo json_encode($pharmacy_domain_value); ?>;

          app.controller('formController', function($scope) {
            $scope.firstname = firstname;
            $scope.lastname = lastname;
            $scope.domain_name = '@' + domain_name;
          });
        </script>
    </head>

    <body>
        <!-- Navigation Bar -->
        <?php require_once 'templates/nav.php'; ?>

        <!-- Main Content -->
        <div class="container">
<?php
// Check if the logged user does not have access, or if they do not
// manage the selected user.
if (!$user_has_manager_access || ($user_is_manager && !$user_manages_account)) : ?>
            <!-- Forbidden -->
            <?php include 'templates/forbidden.html'; ?>

<?php else : ?>
            <!-- Page Title -->
            <div class="row">
                <section class="col-xs-12">
                    <h3>Edit User</h3>
                </section>
            </div>

            <?php include 'templates/alert-success--pass-reset.php'; ?>

    <?php if (1 == $uid) : ?>
            <!-- Warning: User 1 -->
            <div class="row">
                <section class="col-xs-12">
                    <div class="alert alert-warning col-xs-12">
                        <p><strong>Note!</strong> This is User 1 (System Admin).</p>
                    </div>
                </section>
            </div>
    <?php endif; ?>

    <?php if (!$user_has_access_to_edit) : ?>
            <!-- Warning: No access to edit -->
            <div class="row">
                <section class="col-xs-12">
                    <div class="alert alert-info col-xs-12">
                        <p>You do not have access to edit this user.</p>
                    </div>
                </section>
            </div>
    <?php endif; ?>

            <!-- Content -->
            <div class="row">
                <!-- Edit Form -->
                <form id="form_edit" method="post" action="" class="form-horizontal">
                    <section class="col-sm-6 col-xs-12" ng-app="formUser" ng-controller="formController">
                        <!-- Fields -->
                        <?php include 'templates/form-fields-user.php'; ?>
                    </section>

                    <!-- Error -->
                    <?php include 'templates/alert-error.php'; ?>

                    <!-- Submit Buttons -->
                    <section id="buttons" class="col-sm-6 col-xs-12">
                        <div class="btn-group btn-group-justified" role="group">
    <?php if ($user_has_access_to_edit) : ?>
                            <div class="btn-group" role="group">
                                <button type="submit" id="btn_save" name="btn_save" class="btn btn-success">Save</button>
                            </div>
        <?php if (1 != $uid) : ?>
                            <div class="btn-group" role="group">
                                <button type="submit" id="btn_delete" name="btn_delete" class="btn btn-danger">Delete</button>
                            </div>
        <?php endif; ?>
                            <div class="btn-group" role="group">
                                <button type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-default">Cancel</button>
                            </div>
    <?php else : ?>
                            <div class="btn-group" role="group">
                                <button type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-primary">Return to user list</button>
                            </div>
    <?php endif; ?>
                        </div>
                    </section>
                </form>
            </div><!-- /.row -->

            <div class="row">
                <!-- Reset Password Form -->
                <form id="form_reset_password" method="post" action="" class="form-horizontal">
                    <section class="col-xs-12">
                        <!-- Fields -->
                        <?php include 'templates/form-fields-reset-password.php'; ?>
                    </section>
                </form>
            </div><!-- /.row -->
<?php endif; ?>
        </div><!-- /.container -->

        <!-- Footer -->
        <?php require_once 'templates/footer.php'; ?>

        <!-- Scripts -->
        <?php require_once 'includes/incl-js.html'; ?>
        <?php require_once 'js/pass-admin-reset.html'; ?>
        <script>
          $(document).ready(function() {

            // Change Email field if JS is enabled.
            $('#field_email_no_js').html('');
            $('#field_email_js').removeClass('hidden');

            // Enable manual editing of email.
            $('#btn_edit_email').click(function() {
              $('#email').removeAttr('disabled');
              $('#email').parent().removeClass('input-group');
              $('#btn_edit_email').addClass('hidden');
            });

            $('#btn_save, #btn_delete').click(function(e) {
              //Stop the form from submitting itself to the server.
              e.preventDefault();

              // Check which button was clicked and assign request type.
              if (this.id == 'btn_save') {
                var request_type = 'update';
              }
              else if (this.id == 'btn_delete') {
                var request_type = 'delete';
              }

              // Assign input values to variables.
              var uid = "<?php echo $uid; ?>";
              var firstname = $('#firstname').val();
              var lastname = $('#lastname').val();
              var email = $('#email').val();
              var role = $('#role').val();
              var site_code = $('#site_code').val();

              var data = {
                request_type: request_type,
                uid: uid,
                firstname: firstname,
                lastname: lastname,
                email: email,
                role: role,
                site_code: site_code
              };

              // Pass data to ajax form.
              $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: 'includes/edit-user.php',
                data: {
                  json_data: JSON.stringify(data)
                },
                success: function(result) {
                  // No errors.
                  if (true == result.success) {
                    $(location).attr('href', 'users.php');
                  }
                  // Error occurred.
                  else {
                    errors = result.errors;
                    messages = '';
                    // Add breaks between each message.
                    for (var key in errors.messages) {
                      messages += errors.messages[key] + '<br>';
                    }
                    // Adds/removes .has-error from fields.
                    for (var key in errors.fields) {
                      if (errors.fields[key]) {
                        $('#' + key).parent().addClass('has-error');
                      }
                      else {
                        $('#' + key).parent().removeClass('has-error');
                      }
                    }
                    // Display messages.
                    $('#error').removeClass('hidden');
                    $('#error_message').html(messages);
                  }
                }
              });
            });

            $('#btn_cancel').click(function(e) {
              // Return to Users without saving.
              $(location).attr('href', 'users.php');
            });
          });
        </script>

    </body>
</html>

