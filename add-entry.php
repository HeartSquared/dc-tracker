<?php
/**
 * Page - Add new discharge request entry.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

$base_path = basename(__DIR__);

require_once 'includes/common.php';

$site_loader = $container->getSiteLoader();
$head_office = $site_loader->getHeadOffice();

// Check if the logged in user has access to the page.
$is_type_user = check_access(get_roles_with_access('is type user'), $user);
$is_sysadmin = check_access(get_roles_with_access('is sysadmin'), $user);
$has_access = $is_sysadmin || ($is_type_user && $user->getSid() !== $head_office->getSid());
$wards = get_wards_accessible_by_user($container, $user);

// Get character limit for UR Number.
$setting_loader = $container->getSettingLoader();
$ur_char_length_setting = $setting_loader->getSetting('ur_num', 'char_length');
$ur_char_limit = $ur_char_length_setting->getValue();

// Error setting for if Javascript is not enabled.
$error_hidden = true;
$error_message = '';
require_once 'includes/add-entry.php';

$today = date('d/m/Y', time());

// Value filling for if Javascript is not enabled.
$ward_value = isset($_POST['ward']) ? $_POST['ward'] : null;
$firstname_value = isset($_POST['firstname']) ? $_POST['firstname'] : null;
$lastname_value = isset($_POST['lastname']) ? $_POST['lastname'] : null;
$ur_num_value = isset($_POST['ur_num']) ? $_POST['ur_num'] : null;
$medprof_required_checked = isset($_POST['medprof_required']);
$discharge_date_value = isset($_POST['discharge_date']) ? $_POST['discharge_date'] : $today;
$comments_value = isset($_POST['comments']) ? $_POST['comments'] : null;
?>

<!DOCTYPE html>
<html>

    <head>
        <?php
        require_once 'includes/incl-head.html';
        require_once 'includes/incl-form-style.html';
        ?>
        <title>DC-Tracker - Add Discharge Entry</title>
    </head>

    <body>
        <!-- Navigation Bar -->
        <?php require_once 'templates/nav.php'; ?>

        <!-- Main Content -->
        <div class="container">

<?php if (!$has_access) : ?>
            <!-- Forbidden -->
            <?php include 'templates/forbidden.html'; ?>
<?php elseif (empty($wards)) : ?>
            <!-- Page Title -->
            <div class="row">
              <section class="col-xs-12">
                <h3>No wards registered!</h3>
              </section>
            </div>

            <!-- Content -->
            <div class="alert alert-warning">
                <p>There are currently no wards registered, so you cannot add any entries.</p>
            </div>
<?php else : ?>
            <!-- Page Title -->
            <div class="row">
                <section class="col-xs-12">
                    <h3>Add Discharge Entry</h3>
                </section>
            </div>

            <!-- Content -->
            <div class="row">
                <!-- Register Form -->
                <form id="form_add" method="post" action="" class="form-horizontal">

                    <section class="col-sm-6 col-xs-12">
                        <!-- Fields -->
                        <?php include 'templates/form-fields-entry.php'; ?>
                    </section>

                    <!-- Error -->
                    <?php include 'templates/alert-error.php'; ?>

                    <!-- Submit Button: Add Entry -->
                    <section class="col-sm-6 col-xs-12">
                        <input type="submit" name="btn_add" class="btn btn-success col-xs-12" value="Add Entry">
                    </section>

                </form>
            </div><!-- /.row -->

<?php endif; ?>
        </div><!-- /.container -->

        <!-- Footer -->
        <?php require_once 'templates/footer.php'; ?>

        <!-- Scripts -->
        <?php
        require_once 'includes/incl-js.html';
        require_once 'includes/incl-form-js.html';
        ?>
        <script>
          $(document).ready(function(){

            // Change the shown email field if JS is enabled.
            $('#field_discharge_date_no_js').html('');
            $('#field_discharge_date_js').removeClass('hidden');

            // Datepicker settings.
            $('#datepicker').datepicker({
              format: 'dd/mm/yyyy',
              weekStart: 1,
              todayBtn: 'linked',
              todayHighlight: true
            });
            $('#datepicker').datepicker('setDate', 'now');

            // Submit form
            $('#form_add').submit(function(e){
              //Stop the form from submitting itself to the server.
              e.preventDefault();
              // Assign input values to variables.
              var ward = $('#ward').val();
              var firstname = $('#firstname').val();
              var lastname = $('#lastname').val();
              var ur_num = $('#ur_num').val();
              var medprof_required = $('#medprof_required').is(':checked');
              var comments = $('#comments').val();
              var discharge_date = $('#discharge_date').val();

              var data = {
                ward: ward,
                firstname: firstname,
                lastname: lastname,
                ur_num: ur_num,
                medprof_required: medprof_required,
                comments: comments,
                discharge_date: discharge_date
              };

              // Pass data to ajax form.
              $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: 'includes/add-entry.php',
                data: {
                  json_data: JSON.stringify(data)
                },
                success: function(result) {
                  // No errors.
                  if (true == result.success) {
                    $(location).attr('href', 'index.php');
                  }
                  // Error occurred.
                  else {
                    errors = result.errors;
                    messages = '';
                    // Add breaks between each message.
                    for (var key in errors.messages) {
                      messages += errors.messages[key] + '<br>';
                    }
                    // Adds/removes .has-error from fields.
                    for (var key in errors.fields) {
                      if (errors.fields[key]) {
                        $('#' + key).parent().addClass('has-error');
                      }
                      else {
                        $('#' + key).parent().removeClass('has-error');
                      }
                    }
                    // Display messages.
                    $('#error').removeClass('hidden');
                    $('#error_message').html(messages);
                  }
                }
              });
            });
          });
        </script>

    </body>
</html>
