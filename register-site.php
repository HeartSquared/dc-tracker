<?php
/**
 * Page - Register new hospital site.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

$base_path = basename(__DIR__);

require_once 'includes/common.php';

// Check if the logged in user has access to the page.
$has_access = check_access(get_roles_with_access('is admin'), $user);

// Error setting for if Javascript is not enabled.
$error_hidden = true;
$error_message = '';
require_once 'includes/register-site.php';

// Value filling for if Javascript is not enabled.
$hosp_name_value = isset($_POST['hosp_name']) ? $_POST['hosp_name'] : null;
$suburb_value = isset($_POST['suburb']) ? $_POST['suburb'] : null;
$site_code_value = isset($_POST['site_code']) ? $_POST['site_code'] : null;
?>

<!DOCTYPE html>
<html>

    <head>
        <?php require_once 'includes/incl-head.html'; ?>
        <title>DC-Tracker - Register Site</title>
        <script src="vendor/angular.min.js"></script>
        <script>
          var app = angular.module('formSite', []);

          app.controller('formController', function($scope) {});
        </script>
    </head>

    <body>
        <!-- Navigation Bar -->
        <?php require_once 'templates/nav.php'; ?>

        <!-- Main Content -->
        <div class="container">

<?php if (!$has_access) : ?>
            <!-- Forbidden -->
            <?php include 'templates/forbidden.html'; ?>

<?php else : ?>
            <!-- Page Title -->
            <div class="row">
                <section class="col-xs-12">
                    <h3>Register Site</h3>
                </section>
            </div>

            <!-- Content -->
            <div class="row">
                <!-- Register Form -->
                <form id="form_register" method="post" action="" class="form-horizontal">

                    <section class="col-sm-6 col-xs-12" ng-app="formSite" ng-controller="formController">
                        <!-- Fields -->
                        <?php include 'templates/form-fields-site.php'; ?>
                    </section>

                    <!-- Error -->
                    <?php include 'templates/alert-error.php'; ?>

                    <!-- Submit Button: Register -->
                    <section class="col-sm-6 col-xs-12">
                        <input type="submit" name="btn_register" class="btn btn-success col-xs-12" value="Register">
                    </section>

                </form>
            </div><!-- /.row -->
<?php endif; ?>
        </div><!-- /.container -->

        <!-- Footer -->
        <?php require_once 'templates/footer.php'; ?>

        <!-- Scripts -->
        <?php require_once 'includes/incl-js.html'; ?>
        <script>
          $(document).ready(function() {

            // Show Username field only if JS is enabled.
            $('#field_username').removeClass('hidden');

            $('#form_register').submit(function(e) {
              //Stop the form from submitting itself to the server.
              e.preventDefault();

              // Assign input values to variables.
              var hosp_name = $('#hosp_name').val();
              var suburb = $('#suburb').val();
              var site_code = $('#site_code').val();

              var data = {
                hosp_name: hosp_name,
                suburb: suburb,
                site_code: site_code
              };

              // Pass data to ajax form.
              $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: 'includes/register-site.php',
                data: {
                  json_data: JSON.stringify(data)
                },
                success: function(result) {
                  // No errors.
                  if (true == result.success) {
                    $(location).attr('href', 'sites.php');
                  }
                  // Error occurred.
                  else {
                    errors = result.errors;
                    messages = '';
                    // Add breaks between each message.
                    for (var key in errors.messages) {
                      messages += errors.messages[key] + '<br>';
                    }
                    // Adds/removes .has-error from fields.
                    for (var key in errors.fields) {
                      if (errors.fields[key]) {
                        $('#' + key).parent().addClass('has-error');
                      }
                      else {
                        $('#' + key).parent().removeClass('has-error');
                      }
                    }
                    // Display messages.
                    $('#error').removeClass('hidden');
                    $('#error_message').html(messages);
                  }
                }
              });
            });
          });
        </script>

    </body>
</html>
