<?php
/**
 * Page - Reset password.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

$no_redirect = true;
$base_path = basename(__DIR__);

require_once 'includes/common.php';

if (!$logged_in) {
    if (true != $_SESSION['pass_reset_required']) {
        header('Location: index.php');
    }
} else {
    // If the user is logged in and does not require their password to be reset,
    // redirect to password-change.php.
    if (!$user_credential->getRequirePassReset()) {
        header('Location: password-change.php');
    }
}

// Error setting for if Javascript is not enabled.
$error_hidden = true;
$error_message = '';
require_once 'includes/pass-reset.php';
?>

<!DOCTYPE html>
<html>

    <head>
        <?php require_once 'includes/incl-head.html'; ?>
        <title>DC-Tracker - Password Reset</title>
    </head>

    <body>
        <!-- Navigation Bar -->
        <?php require_once 'templates/nav.php'; ?>

        <!-- Main Content -->
        <div class="container">

            <!-- Page Title -->
            <div class="row">
                <section class="col-xs-12">
                    <h3>Password Reset</h3>
                </section>
            </div>

            <!-- Message -->
            <div class="row">
                <section class="col-xs-12">
                    <p>Please change your password.</p>
                </section>
            </div>

            <!-- Content -->
            <div class="row">
                <!-- Password Reset Form -->
                <form id="form_pass_reset" method="post" action="" class="form-horizontal">

                    <section class="col-sm-6 col-xs-12">
                        <!-- Input: New Password -->
                        <div class="form-group">
                            <label for="new_pass" class="control-label col-xs-12">New Password:</label>
                            <div class="col-xs-12">
                                <input type="password" id="new_pass" name="new_pass" class="form-control" required>
                            </div>
                        </div>

                        <!-- Input: Confirm New Password -->
                        <div class="form-group">
                            <label for="new_pass_confirm" class="control-label col-xs-12">Confirm New Password:</label>
                            <div class="col-xs-12">
                                <input type="password" id="new_pass_confirm" name="new_pass_confirm" class="form-control" required>
                            </div>
                        </div>
                    </section>

                    <!-- Error -->
                    <?php require 'templates/alert-error.php'; ?>

                    <!-- Submit Button: Save -->
                    <section id="buttons" class="col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input type="submit" name="btn_save" class="btn btn-success col-xs-12" value="Save">
                            </div>
                        </div>
                    </section>

                </form>
            </div><!-- /.row -->
        </div><!-- /.container -->

        <!-- Footer -->
        <?php require_once 'templates/footer.php'; ?>

        <!-- Scripts -->
        <?php require_once 'includes/incl-js.html'; ?>
        <script>
          $(document).ready(function(){
            $('#form_pass_reset').submit(function(e){
              //Stop the form from submitting itself to the server.
              e.preventDefault();
              // Assign input values to variables.
              var new_pass = $('#new_pass').val();
              var new_pass_confirm = $('#new_pass_confirm').val();

              var data = {
                new_pass: new_pass,
                new_pass_confirm: new_pass_confirm
              };

              // Pass data to ajax form.
              $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: 'includes/pass-reset.php',
                data: {
                  json_data: JSON.stringify(data)
                },
                success: function(result) {
                  // No errors.
                  if (true == result.success) {
                    $(location).attr('href', 'index.php');
                  }
                  // Error occurred.
                  else {
                    errors = result.errors;
                    messages = '';
                    // Add breaks between each message.
                    for (var key in errors.messages) {
                      messages += errors.messages[key] + '<br>';
                    }
                    // Adds/removes .has-error from fields.
                    for (var key in errors.fields) {
                      if (errors.fields[key]) {
                        $('#' + key).parent().addClass('has-error');
                      }
                      else {
                        $('#' + key).parent().removeClass('has-error');
                      }
                    }
                    // Display messages.
                    $('#error').removeClass('hidden');
                    $('#error_message').html(messages);
                  }
                }
              });
            });
          });
        </script>

    </body>
</html>
