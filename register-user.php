<?php
/**
 * Page - Register new user.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

$base_path = basename(__DIR__);

require_once 'includes/common.php';

use Service\UserLoader;

// Check if the logged in user has access to the page.
$has_access = check_access(get_roles_with_access('is manager'), $user);

$site_loader = $container->getSiteLoader();

// Get the current user's site id and code.
$user_sid = $user->getSid();
$user_site_code = $site_loader->getSiteBySid($user_sid)->getSiteCode();

// Error setting for if Javascript is not enabled.
$error_hidden = true;
$error_message = '';
require_once 'includes/register-user.php';

// Value filling for if Javascript is not enabled.
$firstname_value = isset($_POST['firstname']) ? $_POST['firstname'] : null;
$lastname_value = isset($_POST['lastname']) ? $_POST['lastname'] : null;
$email_value = isset($_POST['email']) ? $_POST['email'] : null;
$role_value = isset($_POST['role']) ? $_POST['role'] : null;
$site_code_value = isset($_POST['site_code']) ? $_POST['site_code'] : null;
?>

<!DOCTYPE html>
<html>

    <head>
        <?php require_once 'includes/incl-head.html'; ?>
        <title>DC-Tracker - Register User</title>
        <script src="vendor/angular.min.js"></script>
        <script>
          var app = angular.module('formUser', []);

          var domain_name = <?php echo json_encode(get_pharmacy_domain($container)); ?>;

          app.controller('formController', function($scope) {
            $scope.domain_name = '@' + domain_name;
          });
        </script>
    </head>

    <body>
        <!-- Navigation Bar -->
        <?php require_once 'templates/nav.php'; ?>

        <!-- Main Content -->
        <div class="container">

<?php if (!$has_access) : ?>
            <!-- Forbidden -->
            <?php include 'templates/forbidden.html'; ?>

<?php else : ?>
            <!-- Page Title -->
            <div class="row">
                <section class="col-xs-12">
                    <h3>Register User</h3>
                </section>
            </div>

            <!-- Content -->
            <div class="row">
                <!-- Register Form -->
                <form id="form_register" method="post" action="" class="form-horizontal">

                    <section class="col-sm-6 col-xs-12" ng-app="formUser" ng-controller="formController">
                        <!-- Fields -->
                        <?php include 'templates/form-fields-user.php'; ?>
                    </section>

                    <!-- Error -->
                    <?php include 'templates/alert-error.php'; ?>

                    <!-- Submit Button: Register -->
                    <section class="col-sm-6 col-xs-12">
                        <input type="submit" name="btn_register" class="btn btn-success col-xs-12" value="Register">
                    </section>

                </form>
            </div><!-- /.row -->
<?php endif; ?>
        </div><!-- /.container -->

        <!-- Footer -->
        <?php require_once 'templates/footer.php'; ?>

        <!-- Scripts -->
        <?php require_once 'includes/incl-js.html'; ?>
        <script>
          $(document).ready(function(){

            // Change the shown email field if JS is enabled.
            $('#field_email_no_js').html('');
            $('#field_email_js').removeClass('hidden');

            if (<?php echo $has_access_to_edit ?> && $('#email').hasClass('empty-pharmacy-domain')) {
              $('#email').removeAttr('disabled');
            }

            // Enable manual editing of email
            $('#btn_edit_email').click(function() {
              $('#email').removeAttr('disabled');
              $('#email').parent().removeClass('input-group');
              $('#btn_edit_email').addClass('hidden');
            });

            $('#form_register').submit(function(e){
              //Stop the form from submitting itself to the server.
              e.preventDefault();
              // Assign input values to variables.
              var firstname = $('#firstname').val();
              var lastname = $('#lastname').val();
              var email = $('#email').val();
              var role = $('#role').val();
              var site_code = $('#site_code').val();

              var data = {
                firstname: firstname,
                lastname: lastname,
                email: email,
                role: role,
                site_code: site_code
              };

              // Pass data to ajax form.
              $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: 'includes/register-user.php',
                data: {
                  json_data: JSON.stringify(data)
                },
                success: function(result) {
                  // No errors.
                  if (true == result.success) {
                    $(location).attr('href', 'users.php');
                  }
                  // Error occurred.
                  else {
                    errors = result.errors;
                    messages = '';
                    // Add breaks between each message.
                    for (var key in errors.messages) {
                      messages += errors.messages[key] + '<br>';
                    }
                    // Adds/removes .has-error from fields.
                    for (var key in errors.fields) {
                      if (errors.fields[key]) {
                        $('#' + key).parent().addClass('has-error');
                      }
                      else {
                        $('#' + key).parent().removeClass('has-error');
                      }
                    }
                    // Display messages.
                    $('#error').removeClass('hidden');
                    $('#error_message').html(messages);
                  }
                }
              });
            });
          });
        </script>

    </body>
</html>
