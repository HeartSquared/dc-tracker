<?php
/**
 * Form actions to system required password change.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

// Decode POST data.
if (isset($_POST)) {
    // Javascript is enabled.
    if (isset($_POST['json_data'])) {
        $no_redirect = true;
        $base_path = basename(__DIR__);

        include_once 'common.php';

        $js_enabled = true;
        $post_data = json_decode($_POST['json_data'], true);
    } else {
        // Fallback - Javascript is disabled.
        $js_enabled = false;
        $post_data = $_POST;
    }
}

// Process actions when submit button is clicked.
// Will use AJAX when Javascript is enabled.
if (isset($_POST['btn_save']) || $js_enabled) {
    $new_pass = $post_data['new_pass'];
    $new_pass_confirm = $post_data['new_pass_confirm'];

    $errors = [];

    $errors['fields']['new_pass'] = false;
    $errors['fields']['new_pass_confirm'] = false;

    // Validate new password.
    alter_errors__validate_new_password($errors, $new_pass, $new_pass_confirm);

    // Check if there are errors.
    if (!empty($errors['messages'])) {
        $result = array_merge(['success' => false], ['errors' => $errors]);
    } else {
        // Get current user id from session.
        $cid = $_SESSION['current_cid'];

        // Update password and require_pass_reset.
        $credential_editor = $container->getCredentialEditor();
        $credential_editor->updatePassword($cid, $new_pass);
        $credential_editor->updatePasswordReset($cid, 0);

        $_SESSION['logged_in'] = true;

        $result = ['success' => true];
    }

    // Javascript is enabled.
    if ($js_enabled) {
        // Return JSON encoded result.
        echo json_encode($result);
    } else {
        // Javascript is disabled.
        // Successful.
        if (true == $result['success']) {
            header('Location: index.php');
        } else {
            // Failed. Display errors.
            $error_hidden = false;
            $error_message = concat_result_error_messages($result);
        }
    }
}
