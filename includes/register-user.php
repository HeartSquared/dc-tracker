<?php
/**
 * Form actions to add a new user.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

use Model\AbstractUserType;

// Decode POST data.
if (isset($_POST)) {
    // Javascript is enabled.
    if (isset($_POST['json_data'])) {
        $base_path = basename(__DIR__);

        include_once 'common.php';

        $js_enabled = true;
        // Decode POST data and trim the values.
        $post_data = trim_array_values(json_decode($_POST['json_data'], true));
    } else {
        // Fallback - Javascript is disabled.
        $js_enabled = false;
        $post_data = trim_array_values($_POST);

        // Default values for a field if it is not set.
        // Variables from ../register-user.php.
        $defaults = [
            'firstname' => null,
            'lastname' => null,
            'email' => null,
            'role' => null,
            'site_code' => $user_site_code,
        ];

        // If the $post_data array doesn't currently have the field, set default.
        set_post_data_defaults($post_data, $defaults);
    }
}

// Process actions when submit button is clicked.
// Will use AJAX when Javascript is enabled.
if (isset($_POST['btn_register']) || $js_enabled) {
    $credential_editor = $container->getCredentialEditor();
    $site_loader = $container->getSiteLoader();
    $user_editor = $container->getUserEditor();

    $post_data['email'] = strtolower($post_data['email']);
    $sid = $site_loader->getSiteBySiteCode($post_data['site_code'])->getSid();

    $entity_type = 'user';

    // Fields to perform error checking on.
    // Key: Field id.
    // Value: Field label name for error message.
    $fields_to_validate = [
        'firstname' => 'First Name',
        'lastname' => 'Last Name',
        'email' => 'Email',
        'role' => 'User Role',
        'site_code' => 'Site',
    ];

    $errors = validate_fields($container, $entity_type, $fields_to_validate, $post_data);

    // Check if there are errors.
    if (!empty($errors['messages'])) {
        $result = array_merge(['success' => false], ['errors' => $errors]);
    } else {
        // Create new login credentials.
        $password = generate_password($container);
        $credential_editor->createCredential($post_data['email'], AbstractUserType::USERTYPE_USER, $password);
        $cid = $credential_editor->getCredential($post_data['email'])->getCid();

        // Register user profile.
        $user_editor->createUser($cid, $post_data['firstname'], $post_data['lastname'], $post_data['role'], $post_data['email'], $sid);

        // Store values for display message.
        $_SESSION['new_user_username'] = $post_data['email'];
        $_SESSION['new_user_pass'] = $password;

        $result = ['success' => true];
    }

    // Javascript is enabled.
    if ($js_enabled) {
        // Return JSON encoded result.
        echo json_encode($result);
    } else {
        // Javascript is disabled.
        // Successful.
        if (true == $result['success']) {
            header('Location: users.php');
        } else {
            // Failed. Display errors.
            $error_hidden = false;
            $error_message = concat_result_error_messages($result);
        }
    }
}
