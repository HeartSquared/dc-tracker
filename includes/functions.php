<?php
/**
 * Common functions.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

use Model\AbstractUserType;
use Service\Container;

/**
 * Check if the user has access to the restricted areas.
 *
 * @param array                 $roles Roles with access.
 * @param AbstractUserType|null $user  User object.
 *
 * @return bool
 */
function check_access(array $roles, AbstractUserType $user = null)
{
    if (isset($user) && in_array($user->getRole(), $roles)) {
        return true;
    } else {
        return false;
    }
}

/**
 * Unset session items which should be confined to their corresponding pages.
 *
 * @return void
 */
function unset_session_items()
{
    // Get the current page file name.
    $filename = basename($_SERVER['PHP_SELF']);

    if ('edit-site.php' != $filename) {
        unset($_SESSION['editing_sid']);
    }
    if ('edit-ward.php' != $filename) {
        unset($_SESSION['editing_wid']);
    }
    if ('edit-user.php' != $filename) {
        unset($_SESSION['editing_uid']);
    }
    if ('edit-entry.php' != $filename) {
        unset($_SESSION['editing_eid']);
    }
}

/**
 * Generate a random password or get the default static password.
 *
 * @param Container $container Container service.
 *
 * @return string
 */
function generate_password(Container $container)
{
    $setting_loader = $container->getSettingLoader();
    $static_password_setting = $setting_loader->getSetting('static_password', 'default');
    $static_password_value = $static_password_setting->getValue();

    // Generate a random string for the password.
    $factory = new RandomLib\Factory;
    $generator = $factory->getMediumStrengthGenerator();
    $randomString = $generator->generateString(12, 512);
    $password = !is_null($static_password_value) ? $static_password_value : $randomString;
    return $password;
}

/**
 * Get the default pharmacy domain setting value.
 *
 * @param Container $container Container service.
 *
 * @return string
 */
function get_pharmacy_domain(Container $container)
{
    $setting_loader = $container->getSettingLoader();
    $pharmacy_domain_setting = $setting_loader->getSetting('pharmacy_domain', 'default');
    return $pharmacy_domain_setting->getValue();
}

/**
 * Set post data default values when JS is disabled.
 *
 * @param array $post_data Form post data.
 * @param array $defaults  Field ids and their default values.
 *
 * @return void
 */
function set_post_data_defaults(array &$post_data, array $defaults)
{
    // If the $post_data array doesn't currently have the field, set to default.
    foreach ($defaults as $id => $value) {
        if (!isset($post_data[$id])) {
            $post_data[$id] = $value;
        }
    }
}

/**
 * Trim values and return the new array.
 *
 * @param array $array Array
 *
 * @return array
 */
function trim_array_values(array $array)
{
    $new_array = [];

    foreach ($array as $key => $value) {
        // If there's a value, return it trimmed.
        if (isset($value) && !is_null($value)) {
            $new_array[$key] = trim($value);
        } else {
            // No value, set it to null.
            $new_array[$key] = null;
        }
    }

    return $new_array;
}

function alter_errors__validate_new_password(&$errors, $new_pass, $new_pass_confirm)
{
    // Validate password.
    if (empty(trim($new_pass))) {
        // New password is blank.
        $errors['messages'][] = 'New password cannot be blank.';
        $errors['fields']['new_pass'] = true;
    }
    if (strlen($new_pass) < 6) {
        // New password is too short.
        $errors['messages'][] = 'Password cannot be shorter than 6 characters.';
        $errors['fields']['new_pass'] = true;
    }
    if ($new_pass != $new_pass_confirm) {
        // New password entries do not match.
        $errors['messages'][] = 'The passwords do not match!';
        $errors['fields']['new_pass'] = true;
        $errors['fields']['new_pass_confirm'] = true;
    }
}

/**
 * Validate field submissions.
 *
 * @param Container $container          Container service.
 * @param string    $entity_type        Entity type we are validating.
 * @param array     $fields_to_validate Fields to validate.
 * @param array     $data               Field submission data.
 *
 * @return array $errors Fields with errors and error messages.
 */
function validate_fields(Container $container, $entity_type, array $fields_to_validate, array $data)
{
    $setting_loader = $container->getSettingLoader();
    $credential_editor = $container->getCredentialEditor();
    $site_editor = $container->getSiteEditor();
    $ward_loader = $container->getWardLoader();
    $user_loader = $container->getUserLoader();

    // Get settings.
    $ur_char_length_setting = $setting_loader->getSetting('ur_num', 'char_length');
    $ur_char_length_id = $ur_char_length_setting->getId();
    $ur_alphanumeric_setting = $setting_loader->getSetting('ur_num', 'alphanumeric');
    $ur_allow_alphanumeric = $ur_alphanumeric_setting->getValue();

    foreach ($fields_to_validate as $field_id => $label) {
        // Reset each field to not have error class.
        $errors['fields'][$field_id] = false;

        $has_error = false;

        // Input is blank.
        if (empty($data[$field_id])) {
            $errors['messages'][] = $label . ' cannot be blank.';
            $has_error = true;
        } elseif ('ur_num' == $field_id) {
            // Check for invalid characters for UR Number.
            if ($ur_allow_alphanumeric) {
                // Allow alphanumeric characters.
                $allow_spaces = false;
                if (check_invalid_characters($data[$field_id], $allow_spaces)) {
                    $errors['messages'][] = $label . ' must only contain letters or numbers.';
                    $has_error = true;
                }
            } else {
                // Allow only numeric characters.
                if (!is_numeric($data[$field_id])) {
                    $errors['messages'][] = $label . ' must be numeric.';
                    $has_error = true;
                }
            }
        } elseif ('discharge_date' == $field_id) {
            if ('invalid' == $data[$field_id]) {
                $errors['messages'][] = $label . ' format is invalid (use dd/mm/yy or dd/mm/yyyy).';
                $has_error = true;
            }
        } elseif ('email' == $field_id) {
            // Check email format.
            $valid_email = filter_var($data[$field_id], FILTER_VALIDATE_EMAIL);
            if (!$valid_email) {
                $errors['messages'][] = $label . ' is not in the correct format.';
                $has_error = true;
            }

            // Check if credential with matching username (email) exists.
            if (!empty($data['uid'])) {
                $cid = $user_loader->getUserByUid($data['uid'])->getCid();
            }
            if (!empty($cid)) {
                $user_exists = $credential_editor->checkUserExists($data[$field_id], $cid);
            } else {
                $user_exists = $credential_editor->checkUserExists($data[$field_id]);
            }

            // Username (email) in use.
            if ($user_exists) {
                $errors['messages'][] = $label . ' is already in use.';
                $has_error = true;
            }
        } elseif ($field_id == $ur_char_length_id) {
            $options = [
                'min' => 5,
                'max' => 10,
            ];

            if (check_invalid_list_value($data[$field_id], 'numeric', $options)) {
                // Input is an invalid option.
                $errors['messages'][] = "Now now, don't try to be too sneaky, that $label wasn't an option ;)";
                $has_error = true;
            }
        } elseif ('site_code' == $field_id) {
            if ('site' == $entity_type) {
                // Check if the entered site code is already in use.
                if (!empty($data['sid'])) {
                    $site_code_in_use = $site_editor->checkSiteCodeUsed($data[$field_id], $data['sid']);
                } else {
                    $site_code_in_use = $site_editor->checkSiteCodeUsed($data[$field_id]);
                }

                // Site code in use.
                if ($site_code_in_use) {
                    $errors['messages'][] = "The $label you have entered is already in use.";
                    $has_error = true;
                }

                // Do not allow spaces for site code.
                $allow_spaces = false;
                $has_invalid_char = check_invalid_characters($data[$field_id], $allow_spaces);
            }
        } elseif ('ward_code' == $field_id) {
            // Check ward exists using username (stored in credentials).
            if (!empty($data['wid'])) {
                $cid = $ward_loader->getWardByWid($data['wid'])->getCid();
                $ward_exists = $credential_editor->checkUserExists($data['username'], $cid);
            } else {
                $ward_exists = $credential_editor->checkUserExists($data['username']);
            }

            // Ward code used for selected site.
            if ($ward_exists) {
                $errors['messages'][] = 'This ward has already been registered for the selected site.';
                $has_error = true;
            }

            // Do not allow spaces for ward code.
            $allow_spaces = false;
            $has_invalid_char = check_invalid_characters($data[$field_id], $allow_spaces);
        }

        // Invalid characters in field.
        if (!empty($has_invalid_char)) {
            $errors['messages'][] = $label . ' contains invalid characters.';
            $has_error = true;
        }

        // Mark the field as having an error.
        if ($has_error) {
            $errors['fields'][$field_id] = true;
        }
    }

    return $errors;
}

/**
 * Concatenate error messages from validation results.
 *
 * @param array $result Validation results.
 *
 * @return string
 */
function concat_result_error_messages(array $result)
{
    $error_message = '';

    foreach ($result['errors']['messages'] as $error) {
        $error_message .= "$error<br>";
    }

    return $error_message;
}

/**
 * Check for special characters.
 *
 * @param string $input        String to check.
 * @param bool   $allow_spaces Allow spaces in the string.
 *
 * @return bool
 */
function check_invalid_characters($input, $allow_spaces = true)
{
    if ($allow_spaces) {
        return (preg_match("/[^A-Za-z0-9 ]/", $input));
    } else {
        return (preg_match("/[^A-Za-z0-9]/", $input));
    }
}

/**
 * Check for invalid value options (in case of user manipulation).
 *
 * @param mixed  $value   The value passed in for form submission.
 * @param string $type    The list type we are checking.
 * @param array  $options Options for extra checks (eg. min/max values).
 *
 * @return bool
 */
function check_invalid_list_value($value, $type, array $options = [])
{
    $invalid = false;

    switch ($type) {
        case 'numeric':
            if ($value < $options['min'] || $value > $options['max']) {
                $invalid = true;
            }
            break;

        case 'site':
            break;

        case 'ward':
            break;

        default:
            break;
    }

    return $invalid;
}

/**
 * Get all wards the user has access to.
 *
 * @param Container        $container Container service.
 * @param AbstractUserType $user      User object.
 *
 * @return Ward[]
 */
function get_wards_accessible_by_user(Container $container, AbstractUserType $user)
{
    $is_sysadmin = check_access(get_roles_with_access('is sysadmin'), $user);

    // Get wards.
    $ward_loader = $container->getWardLoader();

    // Sysadmins can see all wards from all sites.
    if ($is_sysadmin) {
        $wards = $ward_loader->getAllWards();
    } else {
        $sid = $user->getSid();
        $wards = $ward_loader->getAllWardsBySid($sid);
    }

    return $wards;
}
