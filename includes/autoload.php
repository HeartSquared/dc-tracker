<?php
/**
 * Class autoloader.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

// Autoload classes.
spl_autoload_register(
    function ($class) {
        $path = PROJECT_ROOT . 'classes/'. str_replace('\\', '/', $class) . '.php';

        if (file_exists($path)) {
            include $path;
        }
    }
);
