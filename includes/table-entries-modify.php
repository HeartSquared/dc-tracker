<?php
/**
 * Modify discharge request entries table.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

// Decode REQUEST data.
if (isset($_REQUEST)) {
    // Javascript is enabled.
    if (isset($_REQUEST['json_data'])) {
        $base_path = basename(__DIR__);

        include_once 'common.php';

        $js_enabled = true;
        // Decode REQUEST data and trim the values.
        $request_data = trim_array_values(json_decode($_REQUEST['json_data'], true));
    } else {
        // Fallback - Javascript is disabled.
        $js_enabled = false;
        $request_data = trim_array_values($_REQUEST);

        if (isset($_REQUEST['btn_complete'])) {
            $request_data['request_type'] = 'complete';
        } elseif (isset($_REQUEST['btn_collect'])) {
            $request_data['request_type'] = 'collect';
        }
    }
}

// Process actions when submit button is clicked.
// Will use AJAX when Javascript is enabled.
if (isset($_REQUEST['btn_complete']) || isset($_REQUEST['btn_collect']) || $js_enabled) {
    $entry_editor = $container->getEntryEditor();

    // Complete/collect a discharge entry.
    switch ($request_data['request_type']) {
        case 'complete':
            $entry_editor->completeEntry($request_data['eid']);
            break;

        case 'collect':
            if (isset($request_data['collected_by_details'])) {
                $entry_editor->collectEntry($request_data['eid'], $request_data['collected_by'], $request_data['collected_by_details']);
            } else {
                $entry_editor->collectEntry($request_data['eid'], $request_data['collected_by']);
            }
            break;
    }

    // Javascript is enabled.
    if ($js_enabled) {
        $table_manager = $container->getTableManager();

        // Create date object from string. Ensure it is set to midnight.
        $select_date = date_create_from_format('d/m/Y H:i:s', "{$request_data['select_date']} 00:00:00");

        // If not a valid date entered, use today's date instead.
        if (empty($select_date)) {
            $today = strtotime('today midnight');
            $select_date = date_create_from_format('U', $today);
        }

        // Get new table data (head office display).
        if ('refresh_head_office_display' == $request_data['request_type']) {
            $table = $container->getTableEntries($user, $select_date);
            $table_manager->displayHeadOfficeEntriesTable($table);
            return;
        }

        // Get new table data.
        $table = $container->getTableEntries($user, $select_date);
        $table_manager->displayTableData($table, $request_data['collection_type'], $request_data['sort_option'], $request_data['sort_order']);
    }
}
