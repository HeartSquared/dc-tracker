<?php
/**
 * Sort discharge request entries table.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

$base_path = basename(__DIR__);

require_once 'common.php';

$table_manager = $container->getTableManager();

// Decode GET data.
$json_decoded = json_decode($_GET['json_data'], true);

switch ($json_decoded['collection_type']) {
    case 'users':
        $table = $container->getTableUsers($user);
        break;

    case 'sites':
        $table = $container->getTableSites($user);
        break;

    case 'wards':
        $table = $container->getTableWards($user);
        break;

    case 'entries':
        // Create date object from string. Ensure it is set to midnight.
        $select_date = date_create_from_format('d/m/Y H:i:s', "{$json_decoded['select_date']} 00:00:00");

        // If not a valid date entered, use today's date instead.
        if (empty($select_date)) {
            $today = strtotime('today midnight');
            $select_date = date_create_from_format('U', $today);
        }

        $table = $container->getTableEntries($user, $select_date);
        break;
}

$table_manager->displayTableData($table, $json_decoded['collection_type'], $json_decoded['sort_option'], $json_decoded['sort_order']);
