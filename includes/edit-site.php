<?php
/**
 * Form actions to edit a site.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

// Decode POST data.
if (isset($_POST)) {
    // Javascript is enabled.
    if (isset($_POST['json_data'])) {
        $base_path = basename(__DIR__);

        include_once 'common.php';

        $js_enabled = true;
        // Decode POST data and trim the values.
        $post_data = trim_array_values(json_decode($_POST['json_data'], true));
    } else {
        // Fallback - Javascript is disabled.
        $js_enabled = false;
        $post_data = trim_array_values($_POST);

        // Default values for a field if it is not set.
        // Variables from ../edit-site.php.
        $defaults = [
            'sid' => $sid,
            'hosp_name' => null,
            'suburb' => null,
            'site_code' => $site->getSiteCode(),
        ];

        // If the $post_data array doesn't currently have the field, set default.
        set_post_data_defaults($post_data, $defaults);

        // Set request type depending on which button is clicked.
        if (isset($_POST['btn_save'])) {
            $post_data['request_type'] = 'update';
        } elseif (isset($_POST['btn_delete'])) {
            $post_data['request_type'] = 'delete';
        } elseif (isset($_POST['btn_cancel'])) {
            // Return to sites if edit is cancelled.
            header('Location: sites.php');
        }
    }
}

// Process actions when submit button is clicked.
// Will use AJAX when Javascript is enabled.
if (isset($_POST['btn_save']) || isset($_POST['btn_delete']) || $js_enabled) {
    $credential_editor = $container->getCredentialEditor();
    $site_editor = $container->getSiteEditor();

    // Get site's credential id.
    $site_cid = $site_editor->getSiteBySid($post_data['sid'])->getCid();

    switch ($post_data['request_type']) {
        case 'update':
            $has_wards = $site_editor->checkHasWards($post_data['sid']);

            // If the site has wards, the Site Code should not be able to be changed.
            // This is to prevent people trying to be sneaky.
            if ($has_wards) {
                $post_data['site_code'] = $site_editor->getSiteBySid($post_data['sid'])->getSiteCode();
            } else {
                $post_data['site_code'] = strtoupper($post_data['site_code']);
            }

            $post_data['username'] = strtolower($post_data['site_code']) . '_pharmacy';

            $entity_type = 'site';

            // Fields to perform error checking on.
            // Key: Field id.
            // Value: Field label name for error message.
            $fields_to_validate = [
                'hosp_name' => 'Hospital Name',
                'suburb' => 'Suburb',
                'site_code' => 'Site Code',
            ];

            $errors = validate_fields($container, $entity_type, $fields_to_validate, $post_data);

            // Check if there are errors.
            if (!empty($errors['messages'])) {
                $result = array_merge(['success' => false], ['errors' => $errors]);
            } else {
                // Update Site and Credential details.
                $site_editor->updateSite($post_data['sid'], $post_data['hosp_name'], $post_data['suburb'], $post_data['site_code']);
                $credential_editor->updateUsername($site_cid, $post_data['username']);
                $result = ['success' => true];
            }
            break;

        case 'delete':
            // Delete Site and Credential.
            $site_editor->deleteSite($post_data['sid']);
            $credential_editor->deleteCred($site_cid);
            $result = ['success' => true];
            break;
    }

    // Javascript is enabled.
    if ($js_enabled) {
        // Return JSON encoded result.
        echo json_encode($result);
    } else {
        // Javascript is disabled.
        // Successful.
        if (true == $result['success']) {
            header('Location: sites.php');
        } else {
            // Failed. Display errors.
            $error_hidden = false;
            $error_message = concat_result_error_messages($result);
        }
    }
}
