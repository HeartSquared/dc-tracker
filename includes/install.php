<?php
/**
 * Database installation.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

use Service\Container;

// Process actions when submit button is clicked.
// Will use AJAX when Javascript is enabled.
if (isset($_POST['btn_install'])) {
    include_once 'functions.php';

    $post_data = trim_array_values($_POST);

    // Default values for a field if it is not set.
    $defaults = [
        'db_type' => null,
        'db_name' => null,
        'db_host' => null,
        'db_username' => null,
        'db_password' => null,
        'sysadmin_firstname' => null,
        'sysadmin_lastname' => null,
        'sysadmin_email' => null,
        'sysadmin_password' => null,
        'pharmacy_domain__default' => null,
        'static_password__default' => null,
        'ur_num__char_length' => 7,
        'ur_num__leading_zero' => 0,
        'ur_num__alphanumeric' => 0,
    ];

    // If the $post_data array doesn't currently have the field, set default.
    set_post_data_defaults($post_data, $defaults);

    $config_contents = "<?php
    define('PROJECT_ROOT', dirname(dirname(__DIR__)) . '/');
    \$config = [];";

    // Database details.
    switch ($post_data['db_type']) {
        case 'sqlite':
            $config_contents .= "
    \$config['database'] = [
        'type' => 'sqlite',
        // Path to database.
        'dsn' => 'sqlite:{$post_data['db_host']}/{$post_data['db_name']}.sq3',
        'user' => null,
        'pass' => null,
    ];";
            break;

        case 'pgsql':
        case 'mysql':
            $config_contents .= "
    \$config['database'] = [
        'type' => '{$post_data['db_type']}',
        'dsn' => '{$post_data['db_type']}:host={$post_data['db_host']};dbname={$post_data['db_name']}',
        'user' => '{$post_data['db_username']}',
        'pass' => '{$post_data['db_password']}',
    ];";
            break;

        default:
            $config_contents = null;
            break;
    }

    $errors = [];

    if (empty($config_contents)) {
        $errors['messages'][] = 'Error creating config file.';
    } else {
        // Create config file.
        $config_file = 'includes/config/config.php';
        file_put_contents($config_file, $config_contents);

        include_once $config_file;
        // Autoload classes.
        include_once 'includes/autoload.php';

        try {
            $container = new Container($config['database']);
            $container->getPDO();
        } catch (PDOException $e) {
            $errors['messages'][] = 'Error connecting to database. Please check that your database details are correct.';
        }

        // Fields to perform error checking on.
        // Key: Field id.
        // Value: Field label name for error message.
        $fields_to_validate = [
            'db_type' => 'Database Type',
            'db_name' => 'Database Name',
            'db_host' => 'Database Host',
        ];

        if ('sqlite' != $post_data['db_type']) {
            $fields_to_validate = array_merge(
                $fields_to_validate,
                [
                    'db_username' => 'Database Username',
                    'db_password' => 'Database Password',
                ]
            );
        }

        if (!empty($post_data['static_password__default'])) {
            $fields_to_validate = array_merge(
                $fields_to_validate,
                [
                    'static_password__default' => 'Static Password',
                ]
            );
        }

        $fields_to_validate = array_merge(
            $fields_to_validate,
            [
                'ur_num__char_length' => 'Character Limit',
            ]
        );

        foreach ($fields_to_validate as $field_id => $label) {
            // Reset each field to not have error class.
            $errors['fields'][$field_id] = false;

            $has_error = false;

            // Input is blank.
            if (empty($post_data[$field_id])) {
                $errors['messages'][] = $label . ' cannot be blank.';
                $has_error = true;
            } elseif ('static_password__default' == $field_id) {
                if (strlen($post_data[$field_id]) < 6) {
                    // Static password is too short.
                    $errors['messages'][] = $label . ' cannot be shorter than 6 characters.';
                    $has_error = true;
                }
            } elseif ('ur_num__char_length' == $field_id) {
                $options = [
                    'min' => 5,
                    'max' => 10,
                ];

                if (check_invalid_list_value($post_data[$field_id], 'numeric', $options)) {
                    // Input is an invalid option.
                    $errors['messages'][] = "Now now, don't try to be too sneaky, that $label wasn't an option ;)";
                    $has_error = true;
                }
            }

            // Mark the field as having an error.
            if ($has_error) {
                $errors['fields'][$field_id] = true;
            }
        }
    }

    // Check if there are errors.
    if (empty($errors['messages'])) {
        // Create the database.
        $installer = $container->getInstaller($post_data['db_type']);
        $installer->createDatabase();

        // Check and create user 1 if they do not exist yet.
        $credential_loader = $container->getCredentialLoader();
        $user1_exists = !empty($credential_loader->getCredentialById(1));
        if (!$user1_exists) {
            $sysadmin_fields_to_validate = [
                'sysadmin_firstname' => 'Sysadmin First Name',
                'sysadmin_lastname' => 'Sysadmin Last Name',
                'sysadmin_email' => 'Sysadmin Email',
                'sysadmin_password' => 'Sysadmin Password',
            ];

            foreach ($sysadmin_fields_to_validate as $field_id => $label) {
                // Reset each field to not have error class.
                $errors['fields'][$field_id] = false;

                $has_error = false;

                // Input is blank.
                if (empty($post_data[$field_id])) {
                    $errors['messages'][] = $label . ' cannot be blank.';
                    $has_error = true;
                } elseif ('sysadmin_email' == $field_id) {
                    // Check email format.
                    $valid_email = filter_var($post_data[$field_id], FILTER_VALIDATE_EMAIL);
                    if (!$valid_email) {
                        $errors['messages'][] = $label . ' is not in the correct format.';
                        $has_error = true;
                    }
                } elseif ('sysadmin_password' == $field_id) {
                    if (strlen($post_data[$field_id]) < 6) {
                        // New password is too short.
                        $errors['messages'][] = $label . ' cannot be shorter than 6 characters.';
                        $has_error = true;
                    }
                }

                // Mark the field as having an error.
                if ($has_error) {
                    $errors['fields'][$field_id] = true;
                }
            }

            if (empty($errors['messages'])) {
                // Create Head Office site.
                $installer->createHeadOffice();
                // Create user 1 - System admin based at head office.
                $installer->createUser1($post_data['sysadmin_email'], $post_data['sysadmin_password'], $post_data['sysadmin_firstname'], $post_data['sysadmin_lastname']);
            }
        }

        if (empty($errors['messages'])) {
            $setting_editor = $container->getSettingEditor();
            $settings = [
                'ur_num' => [
                    'char_length' => $post_data['ur_num__char_length'],
                    'leading_zero' => $post_data['ur_num__leading_zero'],
                    'alphanumeric' => $post_data['ur_num__alphanumeric'],
                ],
                'pharmacy_domain' => [
                    'default' => $post_data['pharmacy_domain__default'],
                ],
                'static_password' => [
                    'default' => $post_data['static_password__default'],
                ],
            ];

            // Add field settings.
            foreach ($settings as $field_name => $field_settings) {
                foreach ($field_settings as $name => $value) {
                    if (empty($setting_editor->getSetting($field_name, $name))) {
                        $setting_editor->createSetting($field_name, $name, $value);
                    } else {
                        $setting_editor->updateSetting($field_name, $name, $value);
                    }
                }
            }

            $result = ['success' => true];
        }
    }

    // Not successful. Has errors.
    if (empty($result['success'])) {
        $result = array_merge(['success' => false], ['errors' => $errors]);
    }

    // Javascript is disabled.
    // Successful.
    if (true === $result['success']) {
        $success_hidden = false;
    } else {
        // Failed. Display errors.
        $error_hidden = false;
        $error_message = concat_result_error_messages($result);
    }
}
