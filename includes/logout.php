<?php
/**
 * Logout functionality.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

// Create a session if it does not already exist.
// Pulls existing session data.
if (!session_start()) {
    session_start();
}

// Unset all the session data that has been stored.
session_unset();
session_destroy();
// Redirect to index.
header('Location: ../index.php');
