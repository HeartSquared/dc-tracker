<?php
/**
 * Form actions to edit settings.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

// Decode POST data.
if (isset($_POST)) {
    // Include common.php for ajax calls.
    if (isset($_POST['json_data'])) {
        $base_path = basename(__DIR__);
        include_once 'common.php';
    }

    $user_is_sysadmin = check_access(get_roles_with_access('is sysadmin'), $user);

    // Get array of Setting objects.
    $setting_editor = $container->getSettingEditor();
    $settings = [
        'ur_char_length_setting' => $setting_editor->getSetting('ur_num', 'char_length'),
        'ur_leading_zero_setting' => $setting_editor->getSetting('ur_num', 'leading_zero'),
        'ur_alphanumeric_setting' => $setting_editor->getSetting('ur_num', 'alphanumeric'),
    ];
    $ur_char_length_id = $settings['ur_char_length_setting']->getId();
    $ur_leading_zero_id = $settings['ur_leading_zero_setting']->getId();
    $ur_alphanumeric_id = $settings['ur_alphanumeric_setting']->getId();

    // Default values for a field if it is not set.
    $defaults = [
        $ur_char_length_id => 7,
        $ur_leading_zero_id => 0,
        $ur_alphanumeric_id => 0,
    ];

    if ($user_is_sysadmin) {
        $sysadmin_settings = [
            'pharmacy_domain_setting' => $setting_editor->getSetting('pharmacy_domain', 'default'),
            'static_password_setting' => $setting_editor->getSetting('static_password', 'default'),
        ];
        $settings = array_merge($sysadmin_settings, $settings);
        $pharmacy_domain_id = $settings['pharmacy_domain_setting']->getId();
        $static_password_id = $settings['static_password_setting']->getId();

        // Default values for a sysadmin only field if it is not set.
        $sysadmin_defaults = [
            $pharmacy_domain_id => null,
            $static_password_id => null,
        ];
        $defaults = array_merge($sysadmin_defaults, $defaults);
    }

    // Javascript is enabled.
    if (isset($_POST['json_data'])) {
        $js_enabled = true;
        // Decode POST data and trim the values.
        $post_data = trim_array_values(json_decode($_POST['json_data'], true));
    } else {
        // Fallback - Javascript is disabled.
        $js_enabled = false;

        if (isset($_POST['btn_save'])) {
            // If the $post_data array doesn't currently have the field, set default.
            set_post_data_defaults($_POST, $defaults);
            $post_data = trim_array_values($_POST);
        }
    }
}

// Process actions when submit button is clicked.
// Will use AJAX when Javascript is enabled.
if (isset($_POST['btn_save']) || $js_enabled) {
    $entity_type = 'settings';

    // Fields to perform error checking on.
    // Key: Field id.
    // Value: Field label name for error message.
    $fields_to_validate = [
        $ur_char_length_id => 'Character Limit',
    ];

    $errors = validate_fields($container, $entity_type, $fields_to_validate, $post_data);

    if ($user_is_sysadmin) {
        if (!empty($post_data[$static_password_id]) && strlen($post_data[$static_password_id]) < 6) {
            // Static password is too short.
            $errors['messages'][] = 'Static Password cannot be shorter than 6 characters.';
            $errors['fields'][$static_password_id] = true;
        }
    }

    // Check if there are errors.
    if (!empty($errors['messages'])) {
        $result = array_merge(['success' => false], ['errors' => $errors]);
    } else {
        // Update settings.
        foreach ($settings as $setting) {
            $id = $setting->getId();
            if (empty($post_data[$id])) {
                $post_data[$id] = $defaults[$id];
            } else {
                if ($id == $ur_leading_zero_id || $id == $ur_alphanumeric_id) {
                    $post_data[$id] = 1;
                }
            }
            $setting_editor->updateSettingById($id, $post_data[$id]);
        }
        $result = ['success' => true];
    }

    // Javascript is enabled.
    if ($js_enabled) {
        // Return JSON encoded result.
        echo json_encode($result);
    } else {
        // Javascript is disabled.
        // Successful.
        if (true == $result['success']) {
            $success_hidden = false;
        } else {
            // Failed. Display errors.
            $error_hidden = false;
            $error_message = concat_result_error_messages($result);
        }
    }
}
