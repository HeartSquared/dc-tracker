<?php
/**
 * Common requirements.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

// Start session.
if (PHP_SESSION_NONE == session_status()) {
    session_start();
}

// Get logged in status.
$logged_in = false;

if (isset($_SESSION['logged_in']) && true == $_SESSION['logged_in']) {
    $logged_in = true;
}

// Return to index if not logged in.
if ((!$logged_in) && (true != $no_redirect)) {
    header('Location: index.php');
    return;
}

$base_common = basename(__DIR__);
if ($base_common == $base_path) {
    include_once 'config/config.php';
} else {
    include_once 'includes/config/config.php';
}

require_once PROJECT_ROOT . 'includes/functions.php';
require_once PROJECT_ROOT . 'includes/permissions.php';

// Autoload classes.
require_once PROJECT_ROOT . 'includes/autoload.php';

// Unset session items which should only be set on their corresponding pages.
unset_session_items();

use Model\AbstractUserType;
use Service\Container;

$container = new Container($config['database']);

if ($logged_in) {
    $current_cid = $_SESSION['current_cid'];
    $user_credential = $container->getCredentialLoader()->getCredentialById($current_cid);
    if (AbstractUserType::USERTYPE_USER == $user_credential->getUserType()) {
        $user = $container->getUserLoader()->getUserByCid($current_cid);
    } elseif (AbstractUserType::USERTYPE_WARD == $user_credential->getUserType()) {
        $user = $container->getWardLoader()->getWardByCid($current_cid);
    } elseif (AbstractUserType::USERTYPE_SITE == $user_credential->getUserType()) {
        $user = $container->getSiteLoader()->getSiteByCid($current_cid);
    }
}
