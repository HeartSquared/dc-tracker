<?php
/**
 * Permission checks.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

use Model\AbstractUserType;
use Service\UserLoader;

/**
 * Returns array of roles required to access the page.
 *
 * @param string $requirement Permission name.
 *
 * @return array
 */
function get_roles_with_access($requirement)
{
    if ('is sysadmin' == $requirement) {
        $roles = [
        UserLoader::ROLE_SYSTEM_ADMIN,
        ];
    } elseif ('is admin' == $requirement) {
        $roles = [
        UserLoader::ROLE_SYSTEM_ADMIN,
        UserLoader::ROLE_ADMIN,
        ];
    } elseif ('is manager' == $requirement) {
        $roles = [
        UserLoader::ROLE_SYSTEM_ADMIN,
        UserLoader::ROLE_ADMIN,
        UserLoader::ROLE_MANAGER,
        ];
    } elseif ('is pharmacy' == $requirement) {
        $roles = [
        UserLoader::ROLE_SYSTEM_ADMIN,
        UserLoader::ROLE_MANAGER,
        UserLoader::ROLE_PHARMACIST,
        UserLoader::ROLE_TECH,
        AbstractUserType::USERTYPE_SITE,
        ];
    } elseif ('is type user' == $requirement) {
        $roles = UserLoader::getUserRoles();
    }

    return $roles;
}
