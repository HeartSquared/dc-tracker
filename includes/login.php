<?php
/**
 * Form actions to login.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

use Model\AbstractUserType;

// Decode POST data.
if (isset($_POST)) {
    // Javascript is enabled.
    if (isset($_POST['json_data'])) {
        $no_redirect = true;
        $base_path = basename(__DIR__);

        include_once 'common.php';

        $js_enabled = true;
        $post_data = json_decode($_POST['json_data'], true);
    } else {
        // Fallback - Javascript is disabled.
        $js_enabled = false;
        $post_data = $_POST;
    }
}

// Process actions when submit button is clicked.
// Will use AJAX when Javascript is enabled.
if (isset($_POST['btn_login']) || $js_enabled) {
    $login_user = $post_data['username'];
    $login_pass = $post_data['password'];

    $usernames_to_check = [$login_user];

    // Allow not needing to enter config domain.
    // Check entered username does not contain the pharmacy domain.
    $pharmacy_domain_value = get_pharmacy_domain($container);
    if (!preg_match("/{$pharmacy_domain_value}$/", $login_user)) {
        // Appends config domain to username and adds to usernames to check.
        $usernames_to_check[] = $login_user . '@' . $pharmacy_domain_value;
    }

    // Loop through usernames.
    foreach ($usernames_to_check as $username) {
        $credential = $container->getCredentialLoader()->getCredential($username);
        // Stop checking through array if we find a match.
        if (!empty($credential)) {
            break;
        }
    }

    $invalid_login = false;

    // Username not registered.
    if (empty($credential)) {
        $invalid_login = true;
    } else {
        $cid = $credential->getCid();
        $password = $credential->getPassword();
        $reset_required = $credential->getRequirePassReset();
        $invalid_usertype = AbstractUserType::USERTYPE_INVALID == $credential->getUserType();

        // Password does not match or not a valid account.
        if (!password_verify($login_pass, $password) || $invalid_usertype) {
            $invalid_login = true;
        }
    }

    // Invalid login.
    if ($invalid_login) {
        $result = [
            'success' => false,
            'error' => 'Invalid username or password.',
        ];
    } else {
        // Assign session variables.
        $_SESSION['current_cid'] = $cid;
        if ($reset_required) {
            $_SESSION['pass_reset_required'] = true;
        } else {
            $_SESSION['logged_in'] = true;
        }

        $result = [
            'success' => true,
            'reset_required' => $reset_required,
        ];
    }

    // Javascript is enabled.
    if ($js_enabled) {
        // Return JSON encoded result.
        echo json_encode($result);
    } else {
        // Javascript is disabled.
        // Successful.
        if (true == $result['success']) {
            if (true == $result['reset_required']) {
                header('Location: password-reset.php');
            } else {
                header('Location: index.php');
            }
        } else {
            // Failed. Display errors.
            $error_hidden = false;
            $error_message = $result['error'];
        }
    }
}
