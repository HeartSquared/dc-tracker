<?php
/**
 * Form actions to user triggered password change.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

// Decode POST data.
if (isset($_POST)) {
    // Javascript is enabled.
    if (isset($_POST['json_data'])) {
        $no_redirect = true;
        $base_path = basename(__DIR__);

        include_once 'common.php';

        $js_enabled = true;
        $post_data = json_decode($_POST['json_data'], true);
    } else {
        // Fallback - Javascript is disabled.
        $js_enabled = false;
        $post_data = $_POST;
    }
}

// Process actions when submit button is clicked.
// Will use AJAX when Javascript is enabled.
if (isset($_POST['btn_update']) || $js_enabled) {
    $old_pass = $post_data['old_pass'];
    $new_pass = $post_data['new_pass'];
    $new_pass_confirm = $post_data['new_pass_confirm'];

    // Get existing credentials.
    $cid = $user->getCid();
    $credential_editor = $container->getCredentialEditor();
    $credential = $credential_editor->getCredentialById($cid);
    $existing_pass = $credential->getPassword();

    $errors = [];

    $errors['fields']['old_pass'] = false;
    $errors['fields']['new_pass'] = false;
    $errors['fields']['new_pass_confirm'] = false;

    // Password does not match.
    if (!password_verify($old_pass, $existing_pass)) {
        $errors['messages'][] = 'The old password you entered is incorrect!';
        $errors['fields']['old_pass'] = true;
    }

    // Validate new password.
    alter_errors__validate_new_password($errors, $new_pass, $new_pass_confirm);

    // Check if there are errors.
    if (!empty($errors['messages'])) {
        $result = array_merge(['success' => false], ['errors' => $errors]);
    } else {
        // Update password.
        $credential_editor->updatePassword($cid, $new_pass);
        $result = ['success' => true];
    }

    // Javascript is enabled.
    if ($js_enabled) {
        // Return JSON encoded result.
        echo json_encode($result);
    } else {
        // Javascript is disabled.
        // Successful.
        if (true == $result['success']) {
            $success_hidden = false;
            $error_hidden = true;
        } else {
            // Failed. Display errors.
            $error_hidden = false;
            $error_message = concat_result_error_messages($result);
        }
    }
}
