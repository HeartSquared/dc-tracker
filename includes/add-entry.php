<?php
/**
 * Form actions to add new discharge request entry.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

// Decode POST data.
if (isset($_POST)) {
    // Javascript is enabled.
    if (isset($_POST['json_data'])) {
        $base_path = basename(__DIR__);

        include_once 'common.php';

        $js_enabled = true;
        // Decode POST data and trim the values.
        $post_data = trim_array_values(json_decode($_POST['json_data'], true));
    } else {
        // Fallback - Javascript is disabled.
        $js_enabled = false;
        $post_data = trim_array_values($_POST);

        // Default values for a field if it is not set.
        $defaults = [
            'ward' => null,
            'firstname' => null,
            'lastname' => null,
            'ur_num' => null,
            'medprof_required' => false,
            'discharge_date' => null,
            'comments' => null,
        ];

        // If the $post_data array doesn't currently have the field, set default.
        set_post_data_defaults($post_data, $defaults);
    }
}

// Process actions when submit button is clicked.
// Will use AJAX when Javascript is enabled.
if (isset($_POST['btn_add']) || $js_enabled) {
    $entry_editor = $container->getEntryEditor();

    // Format discharge date.
    if (!empty($post_data['discharge_date'])) {
        // Create date object from string.
        $discharge_date = date_create_from_format('d/m/Y', $post_data['discharge_date']);
        // Change discharge date format to unix timestamp if valid, otherwise
        // change to 'invalid'.
        $post_data['discharge_date'] = !empty($discharge_date) ? date_format($discharge_date, 'U') : 'invalid';
    }

    $entity_type = 'entry';

    // Fields to perform error checking on.
    // Key: Field id.
    // Value: Field label name for error message.
    $fields_to_validate = [
        'ward' => 'Ward',
        'firstname' => 'First Name',
        'lastname' => 'Last Name',
        'ur_num' => 'UR Number',
        'discharge_date' => 'Discharge Date',
    ];

    $errors = validate_fields($container, $entity_type, $fields_to_validate, $post_data);

    // Check if there are errors.
    if (!empty($errors['messages'])) {
        $result = array_merge(['success' => false], ['errors' => $errors]);
    } else {
        // Set received time as current time.
        $time_received = time();

        // Create new discharge entry.
        $entry_editor->createEntry($post_data['ward'], $post_data['firstname'], $post_data['lastname'], $post_data['ur_num'], $post_data['medprof_required'], $post_data['comments'], $post_data['discharge_date'], $time_received);

        $result = ['success' => true];
    }

    // Javascript is enabled.
    if ($js_enabled) {
        // Return JSON encoded result.
        echo json_encode($result);
    } else {
        // Javascript is disabled.
        // Successful.
        if (true == $result['success']) {
            header('Location: index.php');
        } else {
            // Failed. Display errors.
            $error_hidden = false;
            $error_message = concat_result_error_messages($result);
        }
    }
}
