<?php
/**
 * Form actions to edit a user.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

// Decode POST data.
if (isset($_POST)) {
    // Javascript is enabled.
    if (isset($_POST['json_data'])) {
        $base_path = basename(__DIR__);

        include_once 'common.php';

        $js_enabled = true;
        // Decode POST data and trim the values.
        $post_data = trim_array_values(json_decode($_POST['json_data'], true));
    } else {
        // Fallback - Javascript is disabled.
        $js_enabled = false;
        $post_data = trim_array_values($_POST);

        // Default values for a field if it is not set.
        // Variables from ../edit-user.php.
        $defaults = [
            'uid' => $uid,
            'firstname' => null,
            'lastname' => null,
            'email' => null,
            'role' => $account->getRole(),
            'site_code' => $account_site_code,
        ];

        // If the $post_data array doesn't currently have the field, set default.
        set_post_data_defaults($post_data, $defaults);

        // Set request type depending on which button is clicked.
        if (isset($_POST['btn_save'])) {
            $post_data['request_type'] = 'update';
        } elseif (isset($_POST['btn_delete'])) {
            $post_data['request_type'] = 'delete';
        } elseif (isset($_POST['btn_cancel'])) {
            // Return to sites if edit is cancelled.
            header('Location: users.php');
        }
    }
}

// Process actions when submit button is clicked.
// Will use AJAX when Javascript is enabled.
if (isset($_POST['btn_save']) || isset($_POST['btn_delete']) || $js_enabled) {
    $credential_editor = $container->getCredentialEditor();
    $site_loader = $container->getSiteLoader();
    $user_editor = $container->getUserEditor();
    $account = $user_editor->getUserByUid($post_data['uid']);

    $account_cid = $account->getCid();
    $post_data['email'] = strtolower($post_data['email']);
    $sid = $site_loader->getSiteBySiteCode($post_data['site_code'])->getSid();

    $entity_type = 'user';

    // Fields to perform error checking on.
    // Key: Field id.
    // Value: Field label name for error message.
    $fields_to_validate = [
      'firstname' => 'First Name',
      'lastname' => 'Last Name',
      'email' => 'Email',
      'role' => 'User Role',
      'site_code' => 'Site',
    ];

    switch ($post_data['request_type']) {
        case 'update':
            $errors = validate_fields($container, $entity_type, $fields_to_validate, $post_data);

            // Check if there are errors.
            if (!empty($errors['messages'])) {
                $result = array_merge(['success' => false], ['errors' => $errors]);
            } else {
                // Update User and Credential details.
                $user_editor->updateUser($post_data['uid'], $post_data['firstname'], $post_data['lastname'], $post_data['role'], $post_data['email'], $sid);
                $credential_editor->updateUsername($account_cid, $post_data['email']);
                $result = ['success' => true];
            }
            break;

        case 'delete':
            // Delete User and Credential.
            $user_editor->deleteUser($post_data['uid']);
            $credential_editor->deleteCred($account_cid);
            $result = ['success' => true];
            break;
    }

    // Javascript is enabled.
    if ($js_enabled) {
        // Return JSON encoded result.
        echo json_encode($result);
    } else {
        // Javascript is disabled.
        // Successful.
        if (true == $result['success']) {
            header('Location: users.php');
        } else {
            // Failed. Display errors.
            $error_hidden = false;
            $error_message = concat_result_error_messages($result);
        }
    }
}
