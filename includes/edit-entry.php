<?php
/**
 * Form actions to edit a discharge request entry.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

use Service\EntryLoader;

// Decode POST data.
if (isset($_POST)) {
    // Javascript is enabled.
    if (isset($_POST['json_data'])) {
        $base_path = basename(__DIR__);

        include_once 'common.php';

        $js_enabled = true;
        // Decode POST data and trim the values.
        $post_data = trim_array_values(json_decode($_POST['json_data'], true));
    } else {
        // Fallback - Javascript is disabled.
        $js_enabled = false;

        if (isset($_POST['btn_save']) || isset($_POST['btn_delete'])) {
            // Fallback - Javascript is disabled.
            $js_enabled = false;

            // Default values for a field if it is not set.
            // Variables from ../edit-entry.php.
            $defaults = [
                'eid' => $eid,
                'ward' => null,
                'firstname' => null,
                'lastname' => null,
                'ur_num' => null,
                'medprof_required' => false,
                'discharge_date' => null,
                'comments' => null,
                'time_completed' => $time_completed,
                'time_collected' => $time_collected,
            ];

            // If the $post_data array doesn't currently have the field, set default.
            set_post_data_defaults($_POST, $defaults);
            $post_data = trim_array_values($_POST);

            // Set request type depending on which button is clicked.
            if (isset($_POST['btn_save'])) {
                $post_data['request_type'] = 'update';
            } elseif (isset($_POST['btn_delete'])) {
                $post_data['request_type'] = 'delete';
            }
        } elseif (isset($_POST['btn_cancel'])) {
            // Return to sites if edit is cancelled.
            header('Location: index.php');
        }
    }
}

// Process actions when submit button is clicked.
// Will use AJAX when Javascript is enabled.
if (isset($_POST['btn_save']) || isset($_POST['btn_delete']) || $js_enabled) {
    $entry_editor = $container->getEntryEditor();

    switch ($post_data['request_type']) {
        case 'update':
            // Create date object from string.
            $discharge_date = date_create_from_format('d/m/Y', $post_data['discharge_date']);
            // Change discharge date format to unix timestamp if valid, otherwise change
            // to 'invalid'.
            $post_data['discharge_date'] = !empty($discharge_date) ? date_format($discharge_date, 'U') : 'invalid';

            $completed = EntryLoader::STATUS_COMPLETED == $post_data['status'];
            $collected = EntryLoader::STATUS_COLLECTED == $post_data['status'];
            $cancelled = EntryLoader::STATUS_CANCELLED == $post_data['status'];

            // If not completed or collected, all below variables will be reset to NULL.
            // Cancelled entries will not remove details.
            $time_completed = ($completed || $collected || $cancelled) && isset($post_data['time_completed']) ? $post_data['time_completed'] : null;

            // If not collected, all below variables will be reset to NULL.
            // Cancelled entries will not remove details.
            $time_collected = ($collected || $cancelled) && isset($post_data['time_collected']) ? $post_data['time_collected'] : null;
            $collected_by = ($collected || $cancelled) && isset($post_data['collected_by']) ? $post_data['collected_by'] : null;
            $collected_by_details = ($collected || $cancelled) && isset($post_data['collected_by_details']) ? $post_data['collected_by_details'] : null;

            $entity_type = 'entry';

            // Fields to perform error checking on.
            // Key: Field id.
            // Value: Field label name for error message.
            $fields_to_validate = [
                'ward' => 'Ward',
                'firstname' => 'First Name',
                'lastname' => 'Last Name',
                'ur_num' => 'UR Number',
                'discharge_date' => 'Discharge Day',
            ];

            $errors = validate_fields($container, $entity_type, $fields_to_validate, $post_data);

            // Check if there are errors.
            if (!empty($errors['messages'])) {
                $result = array_merge(['success' => false], ['errors' => $errors]);
            } else {
                // Update Credential and Site details.
                $entry_editor->updateEntry($post_data['eid'], $post_data['ward'], $post_data['firstname'], $post_data['lastname'], $post_data['ur_num'], $post_data['medprof_required'], $post_data['comments'], $post_data['discharge_date'], $post_data['status'], $time_completed, $time_collected, $collected_by, $collected_by_details);

                $result = ['success' => true];
            }
            break;

        case 'delete':
            // Delete Entry.
            $entry_editor->deleteEntry($post_data['eid']);
            $result = ['success' => true];
            break;
    }

    // Javascript is enabled.
    if ($js_enabled) {
        // Return JSON encoded result.
        echo json_encode($result);
    } else {
        // Javascript is disabled.
        // Successful.
        if (true == $result['success']) {
            header('Location: index.php');
        } else {
            // Failed. Display errors.
            $error_hidden = false;
            $error_message = concat_result_error_messages($result);
        }
    }
}
