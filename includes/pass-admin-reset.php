<?php
/**
 * Reset password for an entity.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

// Decode POST data.
if (isset($_POST)) {
    // Javascript is enabled.
    if (isset($_POST['json_data'])) {
        $no_redirect = true;
        $base_path = basename(__DIR__);

        include_once 'common.php';

        $js_enabled = true;
        $post_data = json_decode($_POST['json_data'], true);

        $cid = $post_data['cid'];
    } else {
        // Fallback - Javascript is disabled.
        $js_enabled = false;

        if (!empty($_SESSION['editing_sid'])) {
            $site = $site_editor->getSiteBySid($_SESSION['editing_sid']);
            $cid = $site->getCid();
        } elseif (!empty($_SESSION['editing_wid'])) {
            $ward = $ward_editor->getWardByWid($_SESSION['editing_wid']);
            $cid = $ward->getCid();
        } elseif (!empty($_SESSION['editing_uid'])) {
            $account = $user_editor->getUserByUid($_SESSION['editing_uid']);
            $cid = $account->getCid();
        } else {
            $cid = null;
        }
    }
}

// Process actions when submit button is clicked.
// Will use AJAX when Javascript is enabled.
if (isset($_POST['btn_reset_password']) || $js_enabled) {
    $credential_editor = $container->getCredentialEditor();

    if (!empty($credential_editor->getCredentialById($cid))) {
        // Get the default static password or generate a random one if not set.
        $new_password = generate_password($container);

        // Update password and require_pass_reset.
        $credential_editor->updatePassword($cid, $new_password);
        $credential_editor->updatePasswordReset($cid, 1);

        $result = ['success' => true, 'new_password' => $new_password];
    } else {
        $result = [
            'success' => false,
            'errors' => [
                'messages' => [
                    'Error resetting password.',
                ],
            ],
        ];
    }

    // Javascript is enabled.
    if ($js_enabled) {
        // Return JSON encoded result.
        echo json_encode($result);
    } else {
        // Javascript is disabled.
        // Successful.
        if (true == $result['success']) {
            $password_reset_success = true;
        } else {
            // Failed. Display errors.
            $error_hidden = false;
            $error_message = concat_result_error_messages($result);
        }
    }
}
