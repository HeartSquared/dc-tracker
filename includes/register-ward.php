<?php
/**
 * Form actions to add a new ward.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

use Model\AbstractUserType;

// Decode POST data.
if (isset($_POST)) {
    // Javascript is enabled.
    if (isset($_POST['json_data'])) {
        $base_path = basename(__DIR__);

        include_once 'common.php';

        $js_enabled = true;
        // Decode POST data and trim the values.
        $post_data = trim_array_values(json_decode($_POST['json_data'], true));
    } else {
        // Fallback - Javascript is disabled.
        $js_enabled = false;
        $post_data = trim_array_values($_POST);

        // Default values for a field if it is not set.
        // Variables from ../register-ward.php.
        $defaults = [
            'site_code' => $user_site_code,
            'ward_name' => null,
            'ward_code' => null,
        ];

        // If the $post_data array doesn't currently have the field, set default.
        set_post_data_defaults($post_data, $defaults);
    }
}

// Process actions when submit button is clicked.
// Will use AJAX when Javascript is enabled.
if (isset($_POST['btn_register']) || $js_enabled) {
    $credential_editor = $container->getCredentialEditor();
    $site_loader = $container->getSiteLoader();
    $ward_editor = $container->getWardEditor();

    $post_data['ward_code'] = strtoupper($post_data['ward_code']);
    $post_data['username'] = strtolower("{$post_data['site_code']}_{$post_data['ward_code']}");

    $sid = $site_loader->getSiteBySiteCode($post_data['site_code'])->getSid();

    $entity_type = 'ward';

    // Fields to perform error checking on.
    // Key: Field id.
    // Value: Field label name for error message.
    $fields_to_validate = [
        'site_code' => 'Hospital',
        'ward_name' => 'Ward Name',
        'ward_code' => 'Ward Code',
    ];

    $errors = validate_fields($container, $entity_type, $fields_to_validate, $post_data);

    // Check if there are errors.
    if (!empty($errors['messages'])) {
        $result = array_merge(['success' => false], ['errors' => $errors]);
    } else {
        // Create new login credentials.
        $password = generate_password($container);
        $credential_editor->createCredential($post_data['username'], AbstractUserType::USERTYPE_WARD, $password);
        $cid = $credential_editor->getCredential($post_data['username'])->getCid();

        // Register Ward profile.
        $ward_editor->createWard($cid, $sid, $post_data['ward_code'], $post_data['ward_name']);

        // Store values for display message.
        $_SESSION['new_ward_username'] = $post_data['username'];
        $_SESSION['new_ward_pass'] = $password;

        $result = ['success' => true];
    }

    // Javascript is enabled.
    if ($js_enabled) {
        // Return JSON encoded result.
        echo json_encode($result);
    } else {
        // Javascript is disabled.
        // Successful.
        if (true == $result['success']) {
            header('Location: wards.php');
        } else {
            // Failed. Display errors.
            $error_hidden = false;
            $error_message = concat_result_error_messages($result);
        }
    }
}
