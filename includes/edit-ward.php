<?php
/**
 * Form actions to edit a ward.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

// Decode POST data.
if (isset($_POST)) {
    // Javascript is enabled.
    if (isset($_POST['json_data'])) {
        $base_path = basename(__DIR__);

        include_once 'common.php';

        $js_enabled = true;
        // Decode POST data and trim the values.
        $post_data = trim_array_values(json_decode($_POST['json_data'], true));
    } else {
        // Fallback - Javascript is disabled.
        $js_enabled = false;
        $post_data = trim_array_values($_POST);

        // Default values for a field if it is not set.
        // Variables from ../edit-ward.php.
        $defaults = [
            'wid' => $wid,
            'site_code' => $user_site_code,
            'ward_name' => null,
            'ward_code' => null,
        ];

        // If the $post_data array doesn't currently have the field, set default.
        set_post_data_defaults($post_data, $defaults);

        // Set request type depending on which button is clicked.
        if (isset($_POST['btn_save'])) {
            $post_data['request_type'] = 'update';
        } elseif (isset($_POST['btn_delete'])) {
            $post_data['request_type'] = 'delete';
        } elseif (isset($_POST['btn_cancel'])) {
            // Return to sites if edit is cancelled.
            header('Location: wards.php');
        }
    }
}

// Process actions when submit button is clicked.
// Will use AJAX when Javascript is enabled.
if (isset($_POST['btn_save']) || isset($_POST['btn_delete']) || $js_enabled) {
    $credential_editor = $container->getCredentialEditor();
    $ward_editor = $container->getWardEditor();
    $cid = $ward_editor->getWardByWid($post_data['wid'])->getCid();

    switch ($post_data['request_type']) {
        case 'update':
            $has_entries = $ward_editor->checkHasEntries($post_data['wid']);

            $ward = $ward_editor->getWardByWid($post_data['wid']);
            $site_loader = $container->getSiteLoader();

            // The ward has entries associated.
            if ($has_entries) {
                $post_data['ward_code'] = $ward->getWardCode();
                $sid = $ward->getSid();
                $post_data['site_code'] = $site_loader->getSiteBySid($sid)->getSiteCode();
            } else {
                $post_data['ward_code'] = strtoupper($post_data['ward_code']);
                $sid = $site_loader->getSiteBySiteCode($post_data['site_code'])->getSid();
            }

            $post_data['username'] = strtolower("{$post_data['site_code']}_{$post_data['ward_code']}");

            // Check ward exists using username (stored in Credentials).
            $ward_exists = $credential_editor->checkUserExists($post_data['username'], $cid);

            $entity_type = 'ward';

            // Fields to perform error checking on.
            // Key: Field id.
            // Value: Field label name for error message.
            $fields_to_validate = [
                'site_code' => 'Hospital',
                'ward_name' => 'Ward Name',
                'ward_code' => 'Ward Code',
            ];

            $errors = validate_fields($container, $entity_type, $fields_to_validate, $post_data);

            // Check if there are errors.
            if (!empty($errors['messages'])) {
                $result = array_merge(['success' => false], ['errors' => $errors]);
            } else {
                // Update Ward and Credential details.
                $ward_editor->updateWard($post_data['wid'], $sid, $post_data['ward_code'], $post_data['ward_name']);

                if (!$has_entries) {
                    // Only update the username if the ward has no associated entries.
                    $credential_editor->updateUsername($cid, $post_data['username']);
                }

                $result = ['success' => true];
            }
            break;

        case 'delete':
            // Delete Ward and Credential.
            $ward_editor->deleteWard($post_data['wid']);
            $credential_editor->deleteCred($cid);
            $result = ['success' => true];
            break;
    }

    // Javascript is enabled.
    if ($js_enabled) {
        // Return JSON encoded result.
        echo json_encode($result);
    } else {
        // Javascript is disabled.
        // Successful.
        if (true == $result['success']) {
            header('Location: wards.php');
        } else {
            // Failed. Display errors.
            $error_hidden = false;
            $error_message = concat_result_error_messages($result);
        }
    }
}
