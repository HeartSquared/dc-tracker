<?php
/**
 * Forwards to editing page.
 *
 * @author Cassandra Tam <cassandra.wx.tam@gmail.com>
 */

if (!session_start()) {
    session_start();
}

$id = $_POST['id'];
$type = $_POST['type'];

// Assign id to session variable and forward to editing page.
switch ($type) {
    case 'sites':
        $_SESSION['editing_sid'] = $id;
        header('Location: ../edit-site.php');
        break;

    case 'wards':
        $_SESSION['editing_wid'] = $id;
        header('Location: ../edit-ward.php');
        break;

    case 'users':
        $_SESSION['editing_uid'] = $id;
        header('Location: ../edit-user.php');
        break;

    case 'entries':
        $_SESSION['editing_eid'] = $id;
        header('Location: ../edit-entry.php');
        break;

    default:
        header('Location: ../index.php');
}
